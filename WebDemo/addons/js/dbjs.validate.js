﻿/*
dbjs.validate js验证扩展
author:DeepBlue
email:xshai@163.com
QQ:20480355
---------------------
updates:
---------------------
2014.07.18
-------------
增加radio验证(jvValidateRadio )
增加checkbox验证(jvValidateCheckBox )
-------------
2014-06-24
fisrt version
*/
var MSG_Required = "此项是必填项!";
var MSG_Radio = "此项为必选项!";
var MSG_Checkbox = "此项为必选项!";
var MSG_Number = "此项为数字!";
var MSG_Email = "此项为邮件地址!";
$(function () {
    jvBlurInput();
});

function jvBlurInput() {
    jvBlurRequired();
}

function jvSetRequiredMsg(obj) {
    var id = "required_" + $(obj).attr("id");
    if ($("#" + id).length == 0)
        $(obj).after("<label name='dbjs_validate_msg' id=\"" + id + "\" style=\"color:red\"> " + MSG_Required + "</label>");
}
function jvSetMsg(sender, msg) {
    var id = "required_" + $(sender).attr("id");
    if ($("#" + id).length == 0)
        $(sender).after("<label name='dbjs_validate_msg' id=\"" + id + "\" style=\"color:red\">" + msg + "</label>");
    else {
        var oldMsg = $("#" + id).html();
        if (oldMsg.indexOf(msg) == -1)
            $("#" + id).html(oldMsg + msg);
    }
}

function jvCheckInput() {
    //清除当取消验证时但先前已经出现的验证提示
    $("label[name='dbjs_validate_msg']").each(function () {
        var id = $(this).attr("id");
        if (id != null && id != "" && id != undefined) {
            var conID = id.replace("required_", "");
            if ($("#" + conID).length > 0) {
                var conClass = $("#" + conID).attr("class");
                if (conClass == null || conClass == "" || conClass == undefined || conClass.indexOf("required") == -1) {
                    $(this).remove();
                }
            }
            else
                $(this).remove();
        }
    });

    var len = arguments.length;
    if (len <= 1)
        return jvValidateText1(arguments[0]);
    else if (len == 2)
        return jvValidateText2(arguments[0], arguments[1]);
}

/*==============验证页面控件=======================*/
/*
验证radio
调用:<label class="radio_required" for="groupName"></label>
*/
function jvValidateRadio() {
    var flg = true;
    $(".radio_required").each(function () {
        var groupName = $(this).attr("for");
        if (groupName != "" && groupName != undefined) {
            if ($("input:radio[name='" + groupName + "']:checked").size() == 0) {
                $(this).css("color", "red");
                $(this).html(MSG_Radio);
            }
            else
                $(this).html("");
        }
    });
    return flg;
}
/*
验证checkbox
调用: class="checkbox_required" 
*/
function jvValidateCheckBox() {
    var flg = true;
    $(".checkbox_required").each(function () {
        var zoneID = $(this).attr("id");
        if (zoneID == undefined || zoneID == "undefined")
            zoneID = "guid_" + newGuid();
        $(this).attr("id", zoneID);
        var obj = "#" + zoneID;
        var isSelect = $(obj + " input[type='checkbox']:checked").size() > 0;
        if (isSelect == false) {
            flg = false;
            jvSetRequiredMsg(obj);
        }
        else
            $("#required_" + zoneID).remove();
    });
    return flg;
}

/*
验证 textbox select

*/
function jvValidateText1(sender) {
    var flg = true;
    $(".required").each(function () {
        if ($(this).val() == "") {
            jvSetRequiredMsg(this);
            flg = false;
        }
        else {
            $("#required_" + $(this).attr("id")).remove();
        }
    });

    var radioCheck = jvValidateRadio();
    var checkBoxValidate = jvValidateCheckBox();
    var numberValidate = jvValidateNumber();
    flg = flg && radioCheck && checkBoxValidate && numberValidate;
    if (flg == false) {
        if (sender != null && sender != undefined && $("#msg_check_input").length == 0)
            $(sender).after("<label id=\"msg_check_input\" style=\"color:red\"> 请检查必填项或输入值是否正确!</label>");
    }
    else {
        $("#msg_check_input").remove();
    }
    return flg;
}

/*
验证 textbox select
*/
function jvValidateText2(sender, selector) {
    var flg = true;
    $(selector + " .required").each(function () {
        if ($(this).val() == "") {
            jvSetRequiredMsg(this);
            flg = false;
        }
        else {
            $("#required_" + $(this).attr("id")).remove();
        }
    });

    var radioCheck = jvValidateRadio();
    var checkBoxValidate = jvValidateCheckBox();
    flg = flg && radioCheck && checkBoxValidate;
    if (flg == false) {
        if (sender != null && sender != undefined && $("#msg_check_input").length == 0)
            $(sender).after("<label id=\"msg_check_input\" style=\"color:red\"> 请检查必填项</label>");
    }
    else {
        // if ($("#msg_check_input").length > 0)
        $("#msg_check_input").remove();
    }
    return flg;
}

function jvBlurRequired() {
    $(".required").each(function () {
        $(this).blur(function () {
            if ($(this).val() == "") {
                jvSetRequiredMsg(this);
            }
            else {
                //if ($(this).hasClass("number")) {
                //    var val = $(this).val();
                //    if (isNaN(val)) {
                //        jvSetMsg(this, MSG_Number);
                //    }
                //}
                var obj = $("#required_" + $(this).attr("id"));
                $(obj).remove();
            }
        });
    });
}

/*==============验证控件的值=======================*/
function jvValidateNumber() {
    var flg = true;
    $(".number").each(function () {
        var val = $(this).val();
        if (val != "") {
            if (isNaN(val)) {
                jvSetMsg(this, MSG_Number);
                flg = false;
            }
        }
    });

    return flg;
}