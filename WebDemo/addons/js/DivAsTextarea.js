﻿function divClicked(divID, hiddenID) {
    var divHtml = $("#" + divID).html();
    var editableText = $("<textarea />");
    editableText.val($.trim(divHtml));
    $(this).replaceWith(editableText);
    editableText.focus();
    // setup the blur event for this new textarea
    editableText.blur(function () { editableTextBlurred(divID, hiddenID) });
}

function editableTextBlurred(divID, hiddenID) {
    var html = $(this).val();
    var viewableText = $(divID);
    viewableText.html(html);
    $("#" + hiddenID).val(html);
    $(this).replaceWith(viewableText);
    // setup the click event for this new div
    viewableText.click(function () { divClicked(divID, hiddenID) });
}

function divAsTextarea(divID, hiddenID) {
    $("#" + divID).click(function () { divClicked(divID, hiddenID); });
}

/* 原来滴...
function divClicked() {
    var divHtml = $(this).html();
    var editableText = $("<textarea />");
    editableText.val($.trim(divHtml));
    $(this).replaceWith(editableText);
    editableText.focus();
    // setup the blur event for this new textarea
    editableText.blur(editableTextBlurred);
}

function editableTextBlurred() {
    var html = $(this).val();
    var viewableText = $("<div>");
    viewableText.html(html);
    $(this).replaceWith(viewableText);
    // setup the click event for this new div
    viewableText.click(divClicked);
}

$(document).ready(function () {
    $("div").click(divClicked);
});
*/