﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinFormDemo
{
    public partial class FormSplit : Form
    {
        public FormSplit()
        {
            InitializeComponent();
            var v = ";w3r##$$!@@^#232";
            var arr = v.Split("#",StringSplitOptions.RemoveEmptyEntries );
            Text = "123123$$$".Split("$")[0];
        }
    }
}
