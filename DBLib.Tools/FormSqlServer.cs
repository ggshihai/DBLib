﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DBLib.Tools
{
    public partial class FormSqlServer : Form
    {
        public FormSqlServer()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            var connString = "server=.;database=master;integrated security=true";
            DBLib.Data.DbHelperSQL.connectionString = connString;
            var dir = textBox1.Text.Trim();
            var files = new System.IO.DirectoryInfo(dir).GetFiles();
            var sql = "CREATE DATABASE [{0}] ON ( FILENAME = N'{1}' )  FOR ATTACH";

            var ok = 0;
            var err = 0;
            foreach (var item in files)
            {
                if (item.FullName.ToLower().EndsWith(".mdf"))
                {
                    var s = sql.FormatWith(item.Name.Replace(".mdf", ""), item.FullName);
                    try
                    {
                        DBLib.Data.DbHelperSQL.ExecuteSql(s);
                        ok++;
                    }
                    catch (Exception ex)
                    {
                        err++;
                    }
                }
            }
            MessageBox.Show($"成功附加{ok}个,失败{err}个.");
        }
    }
}
