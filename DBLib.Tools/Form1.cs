﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;

namespace DBLib.Tools
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            this.StartPosition = FormStartPosition.CenterScreen;

        }

        private void btnConvertToBase64_Click(object sender, EventArgs e)
        {
            ConvertToBase64();
        }

        OpenFileDialog dlg;
        private void btnSelect_Click(object sender, EventArgs e)
        {
            dlg = new OpenFileDialog();
            dlg.Multiselect = true;
            dlg.Title = "选择要转换的图片";
            dlg.Filter = "Image files (*.jpg;*.bmp;*.gif;*.png)|*.jpg*.jpeg;*.gif;*.bmp|AllFiles (*.*)|*.*";
            if (DialogResult.OK == dlg.ShowDialog())
            {
                if (string.IsNullOrEmpty(dlg.FileName)) return;
                //for (int i = 0; i < dlg.FileNames.Length; i++)
                //{
                //    ImgToBase64String(dlg.FileNames[i].ToString());
                //}

                Bitmap bmp = new Bitmap(dlg.FileName);
                this.pictureBox1.Image = bmp;

                ConvertToBase64();

            }
        }

        private void ConvertToBase64()
        {
            var ext = Path.GetExtension(dlg.FileName);
            var base64 = ImgToBase64String(dlg.FileName);
            if (cbStyle.Checked)
                richTextBox2.Text = string.Format("background-image: url(\"data:image/{0};base64,{1}\")", ext, base64);
            else if (cbDataHeader.Checked)
                richTextBox2.Text = string.Format("data:image/{0};base64,{1}", ext, base64);
            else
                richTextBox2.Text = base64;
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            if (richTextBox2.Text.Trim() != "")
            {
                Clipboard.SetDataObject(richTextBox2.Text);
                //MessageBox.Show("已复制到剪贴板！");
            }
        }

        //图片 转为    base64编码的文本
        private string ImgToBase64String(string Imagefilename)
        {
            try
            {
                Bitmap bmp = new Bitmap(Imagefilename);
                //this.pictureBox1.Image = bmp;
                //FileStream fs = new FileStream(Imagefilename + ".txt", FileMode.Create);
                //StreamWriter sw = new StreamWriter(fs);

                MemoryStream ms = new MemoryStream();
                //bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                bmp.Save(ms, getImageFormat(Imagefilename));
                byte[] arr = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(arr, 0, (int)ms.Length);
                ms.Close();
                String strbaser64 = Convert.ToBase64String(arr);
                //sw.Write(strbaser64);

                //sw.Close();
                //fs.Close(); 
                return strbaser64;
            }
            catch (Exception ex)
            {
                MessageBox.Show("ImgToBase64String 转换失败\nException:" + ex.Message);
                return null;
            }
        }

        private ImageFormat getImageFormat(string filename)
        {
            var ext = Path.GetExtension(filename).ToLower();
            ImageFormat format = ImageFormat.Png;
            switch (ext)
            {
                case ".gif": format = ImageFormat.Gif; break;
                case ".jpg":
                case ".jpeg": format = ImageFormat.Jpeg; break;
                case ".png": format = ImageFormat.Png; break;
                case ".bmp": format = ImageFormat.Bmp; break;
                case ".icon": format = ImageFormat.Icon; break;
            }
            return format;
        }

        //base64编码的文本 转为    图片
        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Multiselect = true;
            dlg.Title = "选择要转换的base64编码的文本";
            dlg.Filter = "txt files|*.txt";
            if (DialogResult.OK == dlg.ShowDialog())
            {
                for (int i = 0; i < dlg.FileNames.Length; i++)
                {
                    Base64StringToImage(dlg.FileNames[i].ToString());
                }

            }
        }
        //base64编码的文本 转为    图片
        private void Base64StringToImage(string txtFileName)
        {
            try
            {
                FileStream ifs = new FileStream(txtFileName, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(ifs);

                String inputStr = sr.ReadToEnd();
                byte[] arr = Convert.FromBase64String(inputStr);
                MemoryStream ms = new MemoryStream(arr);
                Bitmap bmp = new Bitmap(ms);

                //bmp.Save(txtFileName + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                //bmp.Save(txtFileName + ".bmp", ImageFormat.Bmp);
                //bmp.Save(txtFileName + ".gif", ImageFormat.Gif);
                //bmp.Save(txtFileName + ".png", ImageFormat.Png);
                ms.Close();
                sr.Close();
                ifs.Close();
                this.pictureBox1.Image = bmp;
                if (File.Exists(txtFileName))
                {
                    File.Delete(txtFileName);
                }
                //MessageBox.Show("转换成功！");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Base64StringToImage 转换失败\nException：" + ex.Message);
            }
        }
    }
}
