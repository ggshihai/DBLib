﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DBLib.Tools
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            new Form1().Show();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            new FormSqlServer().Show();
        }
    }
}
