﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
    public class Logs
    {
        public Logs()
        {
            //var logCfg = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "log4net.config");
            //XmlConfigurator.ConfigureAndWatch(logCfg);
        }
        private static string _logDir;
        private static string LogDir
        {
            get
            {
                if (string.IsNullOrEmpty(_logDir))
                {
                    _logDir = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\";
                    if (System.IO.Directory.Exists(_logDir) == false)
                        System.IO.Directory.CreateDirectory(_logDir);
                }
                return _logDir;
            }
        }

        private static string LogPath
        {
            get
            {
                return LogDir + "\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".log"; ;
            }
        }
        private static string LogPathError
        {
            get
            {
                return LogDir + "\\" + DateTime.Now.ToString("yyyy-MM-dd") + "_Error.log"; ;
            }
        }

        public static bool Write(string msg, DateTime datetime)
        {
            try
            {
                using (var sw = new System.IO.StreamWriter(LogPath, true, UTF8Encoding.UTF8))
                {
                    sw.Write(string.Format("{0} => {1}", datetime, msg));
                }
            }
            catch { return false; }
            return true;
        }

        public static bool Write(string msg, DateTime datetime, string dateTimeFormat)
        {
            try
            {
                using (var sw = new System.IO.StreamWriter(LogPath, true, UTF8Encoding.UTF8))
                {
                    sw.Write(string.Format("{0} => {1}", datetime.ToString(dateTimeFormat), msg));
                }
            }
            catch { return false; }
            return true;
        }

        public static bool WriteLine(string msg)
        {
            return WriteLine(msg, DateTime.Now, "HH:mm");
        }

        public static bool WriteLine(string msg, DateTime datetime)
        {
            return WriteLine(msg, datetime, "HH:mm");
        }

        public static bool WriteLine(string msg, DateTime dateTime, string dateTimeFormat)
        {
            try
            {
                using (var sw = new System.IO.StreamWriter(LogPath, true, UTF8Encoding.UTF8))
                {
                    sw.WriteLine(string.Format("{0} => {1}", dateTime.ToString(dateTimeFormat), msg));
                }
            }
            catch { return false; }
            return true;
        }

        public static bool WriteError(string format, params object[] args)
        {
            return WriteError(string.Format(format, args), DateTime.Now, "HH:mm");
        }
        private static object objLock = new object();
        public static bool WriteError(string msg, DateTime dateTime, string dateTimeFormat)
        {
            try
            {
                lock (objLock)
                {
                    using (var sw = new System.IO.StreamWriter(LogPathError, true, UTF8Encoding.UTF8))
                    {
                        sw.WriteLine(string.Format("{0} => {1}", dateTime.ToString(dateTimeFormat), msg));
                    }
                }
            }
            catch { return false; }
            return true;
        }


        //public static async Threading.Tasks.Task<bool> WriteLine(string msg, DateTime dateTime, string dateTimeFormat, string mmm)
        //{
        //    try
        //    {
        //        using (var sw = new System.IO.StreamWriter(LogPath, true, UTF8Encoding.UTF8))
        //        {
        //            sw.WriteLine(string.Format("{0} => {1}", dateTime.ToString(dateTimeFormat), msg));
        //        }
        //    }
        //    catch { return false; }
        //    return true;
        //}
    }
}
