﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace DBLib.Data
{
    public class DBHelper
    {
        /// <summary>
        /// 将DataTable 数据类型转换成 MS SQL 支持的类型
        /// </summary>
        /// <param name="type">DataTable 列类型</param>
        /// <returns></returns>
        public static string GetTableColumnType(System.Type type)
        {
            string result = "varchar(8000)";
            string sDbType = type.ToString();
            switch (sDbType)
            {
                case "System.String":
                    break;
                case "System.Int16":
                    result = "int";
                    break;
                case "System.Int32":
                    result = "int";
                    break;
                case "System.Int64":
                    result = "bigint";
                    break;
                case "System.Decimal":
                    result = "decimal(18,6)";
                    break;
                case "System.Double":
                    result = "decimal(18,6)";
                    break;
                case "System.DateTime":
                    result = "datetime";
                    break;
                default:
                    break;
            }
            return result;
        }

        //https://docs.microsoft.com/en-us/sql/relational-databases/replication/non-sql/data-type-mapping-for-oracle-publishers?view=sql-server-2017
        public static string GetMsSqlDataTypeFromOracleDataType(string oracleDataType)
        {
            string dataType = "nvarchar(max)";
            oracleDataType = oracleDataType.ToUpper();
            if (oracleDataType == "BFILE") dataType = "VARBINARY(MAX)";//是否完全对应:1
            else if (oracleDataType == "BLOB") dataType = "VARBINARY(MAX)";//是否完全对应:1
            else if (oracleDataType.StartsWith("CHAR")) dataType = oracleDataType;//是否完全对应:1
            else if (oracleDataType == "CLOB") dataType = "VARCHAR(MAX)";//是否完全对应:1
            else if (oracleDataType == "DATE") dataType = "DATETIME";//是否完全对应:1

            //FLOAT	FLOAT	No
            //FLOAT([1-53])	FLOAT([1-53])	No
            //FLOAT([54-126])	FLOAT	No
            else if (oracleDataType.StartsWith("FLOAT")) dataType = "FLOAT";//是否完全对应:0

            else if (oracleDataType == "INT") dataType = "NUMERIC(38)";//是否完全对应:0
            else if (oracleDataType == "INTERVAL") dataType = "DATETIME";//是否完全对应:1
            else if (oracleDataType == "LONG") dataType = "VARCHAR(MAX)";//是否完全对应:1
            else if (oracleDataType == "LONG RAW") dataType = "IMAGE";//是否完全对应:1
            else if (oracleDataType.StartsWith("NCHAR")) dataType = oracleDataType;//是否完全对应:0
            else if (oracleDataType == "NCLOB") dataType = "NVARCHAR(MAX)";//是否完全对应:1

            //NUMBER	FLOAT	Yes
            //NUMBER([1-38])	NUMERIC([1-38])	No
            //NUMBER([0-38],[1-38])	NUMERIC([0-38],[1-38])
            else if (oracleDataType == "NUMBER") dataType = "FLOAT";//是否完全对应:
            else if (oracleDataType.StartsWith("NUMBER")) dataType = oracleDataType.Replace("NUMBER", "NUMERIC");//是否完全对应:

            else if (oracleDataType.StartsWith("NVARCHAR2")) dataType = oracleDataType.Replace("NVARCHAR2", "NVARCHAR");//是否完全对应:

            //RAW([1-2000])	VARBINARY([1-2000])	No
            else if (oracleDataType.StartsWith("RAW")) dataType = oracleDataType.Replace("RAW", "VARBINARY");//是否完全对应:

            else if (oracleDataType == "REAL") dataType = "FLOAT";//是否完全对应:0

            else if (oracleDataType == "ROWID") dataType = "CHAR(18)";//是否完全对应:0

            else if (oracleDataType == "TIMESTAMP") dataType = "DATETIME";//是否完全对应:1

            //TIMESTAMP(0-7)	DATETIME	Yes
            //TIMESTAMP(8-9)	DATETIME	Yes
            //TIMESTAMP(0-7) WITH TIME ZONE	VARCHAR(37)	Yes
            //TIMESTAMP(8-9) WITH TIME ZONE	VARCHAR(37)	No
            //TIMESTAMP(0-7) WITH LOCAL TIME ZONE	VARCHAR(37)	Yes
            //TIMESTAMP(8-9) WITH LOCAL TIME ZONE	VARCHAR(37)	No
            else if (oracleDataType.StartsWith("TIMESTAMP"))
            {
                var reg = new Regex(@"^TIMESTAMP\(\d\)$");
                if (reg.IsMatch(oracleDataType)) dataType = "DATETIME";
                else dataType = "VARCHAR(37)";
            }
            else if (oracleDataType == "UROWID") dataType = "CHAR(18)";

            else if (oracleDataType.StartsWith("VARCHAR2")) dataType = oracleDataType.Replace("VARCHAR2", "VARCHAR");

            return dataType;
        }


        public static bool Exists(SqlConnection conn, SqlTransaction trans, string tableName, string keyID, object keyValue)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from " + tableName);
            strSql.AppendFormat(" where {0}=@keyID ", keyID);
            SqlParameter[] parameters = { new SqlParameter("@keyID", keyValue) };

            var obj = DbHelperSQL.GetSingle(conn, trans, strSql.ToString(), parameters);
            return Convert.ToInt32(obj) > 0;
        }

        public static bool Exists(SqlConnection conn, SqlTransaction trans, string sql)
        {
            var obj = DbHelperSQL.GetSingle(conn, trans, sql);
            return Convert.ToInt32(obj) > 0;
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public static bool Add<TModel>(SqlConnection conn, SqlTransaction trans, TModel model, string keyID = "id")
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder str1 = new StringBuilder();//数据字段
            StringBuilder str2 = new StringBuilder();//数据参数
            //利用反射获得属性的所有公共属性
            PropertyInfo[] pros = model.GetType().GetProperties();
            List<SqlParameter> paras = new List<SqlParameter>();
            strSql.AppendFormat("insert into {0}(", model.GetType().Name);
            foreach (PropertyInfo pi in pros)
            {
                ////如果不是主键则追加sql字符串
                if (!pi.Name.Equals(keyID, StringComparison.OrdinalIgnoreCase))
                {
                    //判断属性值是否为空
                    if (pi.GetValue(model, null) != null)
                    {
                        str1.Append(pi.Name + ",");//拼接字段
                        str2.Append("@" + pi.Name + ",");//声明参数
                        paras.Add(new SqlParameter("@" + pi.Name, pi.GetValue(model, null)));//对参数赋值
                    }
                }
            }
            strSql.Append(str1.ToString().Trim(','));
            strSql.Append(") values (");
            strSql.Append(str2.ToString().Trim(','));
            strSql.Append(") ");
            var obj = DbHelperSQL.ExecuteSql(conn, trans, strSql.ToString(), paras.ToArray());
            return obj > 0;
        }


        public static int Add(SqlConnection conn, SqlTransaction trans, string tableName, string keyID, DataTable dtStructure, DataTable dtDatasoure)
        {
            if (dtDatasoure.Rows.Count == 0) return 0;

            StringBuilder strSql = new StringBuilder();
            StringBuilder str1 = new StringBuilder();//数据字段
            StringBuilder str2 = new StringBuilder();//数据参数

            strSql.AppendFormat("insert into {0}(", tableName);
            var cols = dtStructure.Columns;
            foreach (DataColumn item in cols)
            {
                str1.Append(item.ColumnName + ",");//拼接字段
                str2.Append("@" + item.ColumnName + ",");//声明参数                
            }
            strSql.Append(str1.ToString().Trim(','));
            strSql.Append(") values (");
            strSql.Append(str2.ToString().Trim(','));
            strSql.Append(") ");

            //这里是由Oracle转mssql写数据,oracle没有自增ID,所以此处不需要以下
            //DBUtility.DbHelperSQL.ExecuteSql(conn, trans, "set identity_insert {0} ON".FormatWith(tableName));

            int i = 0;
            foreach (DataRow row in dtDatasoure.Rows)
            {
                var paras = new List<SqlParameter>();
                foreach (DataColumn col in cols)
                {
                    paras.Add(new SqlParameter("@" + col.ColumnName, row[col.ColumnName]));//对参数赋值
                }
                i += DbHelperSQL.ExecuteSql(conn, trans, strSql.ToString(), paras.ToArray());
            }

            //这里是由Oracle转mssql写数据,oracle没有自增ID,所以此处不需要以下
            //DBUtility.DbHelperSQL.ExecuteSql(conn, trans, "set identity_insert {0} OFF".FormatWith(tableName));
            return i;
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public static int Update(SqlConnection conn, SqlTransaction trans, string tableName, string keyID, DataTable dtStructure, DataTable dtDatasoure)
        {
            if (dtDatasoure.Rows.Count == 0) return 0;

            StringBuilder strSql = new StringBuilder();
            StringBuilder str1 = new StringBuilder();

            strSql.AppendFormat("update {0} set ", tableName);
            var cols = dtStructure.Columns;
            foreach (DataColumn item in cols)
            {
                str1.Append(item.ColumnName + "=@" + item.ColumnName + ",");//声明参数
            }
            strSql.Append(str1.ToString().Trim(','));
            strSql.AppendFormat(" where {0}=@{0} ", keyID);

            int i = 0;
            foreach (DataRow row in dtDatasoure.Rows)
            {
                //如果不是主键则追加sql字符串
                var paras = new List<SqlParameter>();
                foreach (DataColumn col in cols)
                {
                    if (!col.ColumnName.Equals(keyID))
                    {
                        paras.Add(new SqlParameter("@" + col.ColumnName, row[col.ColumnName]));//对参数赋值
                    }
                }
                paras.Add(new SqlParameter("@" + keyID, row[keyID]));
                i += DbHelperSQL.ExecuteSql(conn, trans, strSql.ToString(), paras.ToArray());
            }
            return i;
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public static bool Update<TModel>(SqlConnection conn, SqlTransaction trans, TModel model, string keyID, string keyValue)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder str1 = new StringBuilder();
            //利用反射获得属性的所有公共属性
            PropertyInfo[] pros = model.GetType().GetProperties();
            List<SqlParameter> paras = new List<SqlParameter>();
            strSql.AppendFormat("update {0} set ", model.GetType().Name);
            foreach (PropertyInfo pi in pros)
            {
                //如果不是主键则追加sql字符串
                if (!pi.Name.Equals(keyID))
                {
                    //判断属性值是否为空
                    if (pi.GetValue(model, null) != null)
                    {
                        str1.Append(pi.Name + "=@" + pi.Name + ",");//声明参数
                        paras.Add(new SqlParameter("@" + pi.Name, pi.GetValue(model, null)));//对参数赋值
                    }
                }
            }
            strSql.Append(str1.ToString().Trim(','));
            strSql.AppendFormat(" where {0}=@{0} ", keyID);
            paras.Add(new SqlParameter("@" + keyID, keyValue));
            return DbHelperSQL.ExecuteSql(conn, trans, strSql.ToString(), paras.ToArray()) > 0;
        }
    }
}