﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace System.Linq
{
    public struct OrderBy
    {
        public const bool Asc = true;
        public const bool Desc = false;
    }
    public static class PredicateExtensions
    {
        /// <summary>
        /// 分页查询 + 条件查询 + 排序
        /// </summary>
        /// <typeparam name="Tkey">泛型</typeparam>
        /// <param name="pageSize">每页大小</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="total">总数量</param>
        /// <param name="whereLambda">查询条件</param>
        /// <param name="orderbyLambda">排序条件</param>
        /// <param name="isAsc">是否升序</param>
        /// <returns>IQueryable 泛型集合</returns>
        //public IQueryable<T> LoadPageItems<Tkey>(int pageSize, int pageIndex, out int total, Expression<Func<T, bool>> whereLambda, Func<T, Tkey> orderbyLambda, bool isAsc)
        //{
        //    total = DB.ArticleContext .Set<T>().Where(whereLambda).Count();
        //    if (isAsc)
        //    {
        //        var temp = MyBaseDbContext.Set<T>().Where(whereLambda)
        //                     .OrderBy<T, Tkey>(orderbyLambda)
        //                     .Skip(pageSize * (pageIndex - 1))
        //                     .Take(pageSize);
        //        return temp.AsQueryable();
        //    }
        //    else
        //    {
        //        var temp = MyBaseDbContext.Set<T>().Where(whereLambda)
        //                   .OrderByDescending<T, Tkey>(orderbyLambda)
        //                   .Skip(pageSize * (pageIndex - 1))
        //                   .Take(pageSize);
        //        return temp.AsQueryable();
        //    }
        //}

        //public IQueryable<T> GetWithPager(Expression<Func<T, bool>> match, int page_index, int page_size, Dictionary<string, bool> order_keys, out int record_count)
        //{
        //    var result = _dbs.Where(match);
        //    var par = Expression.Parameter(typeof(T));
        //    bool first = true;
        //    foreach (KeyValuePair<string, bool> key in order_keys)
        //    {
        //        var pro = typeof(T).GetProperty(key.Key);
        //        var acc = Expression.MakeMemberAccess(par, pro);
        //        var exp = Expression.Lambda(acc, par);
        //        var name = (first ? "OrderBy" : "ThenBy") + (key.Value ? string.Empty : "Descending");
        //        MethodCallExpression call = Expression.Call(typeof(Queryable), name, new Type[] { typeof(T), pro.PropertyType }, result.Expression, Expression.Quote(exp));
        //        result = result.Provider.CreateQuery<T>(call);
        //        first = false;
        //    }
        //    record_count = result.Count();
        //    return result.Skip(page_size * (page_index - 1)).Take(page_size);
        //}

        public static IQueryable<T> QueryWithPager<T>(this IQueryable<T> source, Expression<Func<T, bool>> match, int page_index, int page_size
            , Dictionary<string, bool> orderFields, out int record_count)
        {
            IQueryable<T> result = source.Where(match);
            //if (match != null)
            //    result = source.Where(match);
            //else result = source;

            record_count = result.Count();
            if (record_count == 0) return result;

            var par = Expression.Parameter(typeof(T));
            bool first = true;
            foreach (KeyValuePair<string, bool> order in orderFields)
            {
                var pro = typeof(T).GetProperty(order.Key);
                var acc = Expression.MakeMemberAccess(par, pro);
                var exp = Expression.Lambda(acc, par);
                var name = (first ? "OrderBy" : "ThenBy") + (order.Value ? string.Empty : "Descending");
                //var name = (first ? "OrderBy" : "ThenBy") + (order.Value == OrderBy.Ascending ? string.Empty : "Descending");
                MethodCallExpression call = Expression.Call(typeof(Queryable), name, new Type[] { typeof(T), pro.PropertyType }, result.Expression, Expression.Quote(exp));
                result = result.Provider.CreateQuery<T>(call);
                first = false;
            }

            return result.Skip(page_size * (page_index - 1)).Take(page_size);
        }

        ///<summary>  
        /// Creates a predicate that evaluates to true.  
        /// </summary>  
        public static Expression<Func<T, bool>> True<T>() { return param => true; }

        /// <summary>  
        /// Creates a predicate that evaluates to false.  
        /// </summary>  
        public static Expression<Func<T, bool>> False<T>() { return param => false; }

        /// <summary>  
        /// Creates a predicate expression from the specified lambda expression.  
        /// </summary>  
        public static Expression<Func<T, bool>> Create<T>(Expression<Func<T, bool>> predicate) { return predicate; }

        /// <summary>  
        /// Combines the first predicate with the second using the logical "and".  
        /// </summary>  
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
        {
            return first.Compose(second, Expression.AndAlso);
        }

        /// <summary>  
        /// Combines the first predicate with the second using the logical "or".  
        /// </summary>  
        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
        {
            return first.Compose(second, Expression.OrElse);
        }

        /// <summary>  
        /// Negates the predicate.  
        /// </summary>  
        public static Expression<Func<T, bool>> Not<T>(this Expression<Func<T, bool>> expression)
        {
            var negated = Expression.Not(expression.Body);
            return Expression.Lambda<Func<T, bool>>(negated, expression.Parameters);
        }

        /// <summary>  
        /// Combines the first expression with the second using the specified merge function.  
        /// </summary>  
        static Expression<T> Compose<T>(this Expression<T> first, Expression<T> second, Func<Expression, Expression, Expression> merge)
        {
            // zip parameters (map from parameters of second to parameters of first)  
            var map = first.Parameters
                .Select((f, i) => new { f, s = second.Parameters[i] })
                .ToDictionary(p => p.s, p => p.f);

            // replace parameters in the second lambda expression with the parameters in the first  
            var secondBody = ParameterRebinder.ReplaceParameters(map, second.Body);

            // create a merged lambda expression with parameters from the first expression  
            return Expression.Lambda<T>(merge(first.Body, secondBody), first.Parameters);
        }

        class ParameterRebinder : ExpressionVisitor
        {
            readonly Dictionary<ParameterExpression, ParameterExpression> map;

            ParameterRebinder(Dictionary<ParameterExpression, ParameterExpression> map)
            {
                this.map = map ?? new Dictionary<ParameterExpression, ParameterExpression>();
            }

            public static Expression ReplaceParameters(Dictionary<ParameterExpression, ParameterExpression> map, Expression exp)
            {
                return new ParameterRebinder(map).Visit(exp);
            }

            protected override Expression VisitParameter(ParameterExpression p)
            {
                ParameterExpression replacement;

                if (map.TryGetValue(p, out replacement))
                {
                    p = replacement;
                }

                return base.VisitParameter(p);
            }
        }
        public static IQueryable<TElement> IsDateBetween<TElement>(this IQueryable<TElement> queryable,
                                                           Expression<Func<TElement, DateTime>> fromDate,
                                                           Expression<Func<TElement, DateTime>> toDate,
                                                           DateTime date)
        {
            var p = fromDate.Parameters.Single();
            Expression member = p;

            Expression fromExpression = Expression.Property(member, (fromDate.Body as MemberExpression).Member.Name);
            Expression toExpression = Expression.Property(member, (toDate.Body as MemberExpression).Member.Name);

            var after = Expression.LessThanOrEqual(fromExpression,
                 Expression.Constant(date, typeof(DateTime)));

            var before = Expression.GreaterThanOrEqual(
                toExpression, Expression.Constant(date, typeof(DateTime)));

            Expression body = Expression.And(after, before);

            var predicate = Expression.Lambda<Func<TElement, bool>>(body, p);
            return queryable.Where(predicate);
        }
    }
}