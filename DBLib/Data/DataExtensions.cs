﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace System
{
    public static class DataExtensions
    {
        /// <summary> 
        /// 将DataTable转换为List<T>对象
        /// </summary> 
        /// <param name="dt">DataTable 对象</param> 
        /// <returns>List<T>集合</returns> 
        public static List<T> DataTableToList<T>(this DataTable dt, string colName = null)
        {
            List<T> list = new List<T>();
            if (colName.IsNullOrEmpty())
            {
                foreach (DataRow dr in dt.Rows)
                {
                    //object obj = dr[0].ToString();
                    object obj = Convert.ChangeType(dr[0], typeof(T));
                    list.Add((T)obj);
                }
            }
            else
            {
                foreach (DataRow dr in dt.Rows)
                {
                    //object obj = dr[colName].ToString(); 
                    object obj = Convert.ChangeType(dr[colName], typeof(T));
                    list.Add((T)obj);
                    //list.Add((T)dr[colName]);
                }
            }
            return list;
        }
    }
}
