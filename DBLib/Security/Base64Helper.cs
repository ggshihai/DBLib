﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBLib.Security
{
    public class Base64Helper
    {
        /// <summary>
        /// Base64加密，默认采用utf8编码方式加密
        /// </summary>
        /// <param name="encodeType">加密采用的编码方式</param>
        /// <param name="source">待加密的明文</param>
        /// <returns></returns>
        public static string Encode(string source, Encoding encoding = null)
        {
            encoding = encoding ?? Encoding.UTF8;
            string encode = string.Empty;
            byte[] bytes = encoding.GetBytes(source);
            try
            {
                encode = Convert.ToBase64String(bytes);
            }
            catch
            {
                encode = source;
            }
            return encode;
        }

        /// <summary>
        /// Base64解密，默认采用utf8编码方式解密
        /// </summary>
        /// <param name="encodeType">解密采用的编码方式，注意和加密时采用的方式一致</param>
        /// <param name="result">待解密的密文</param>
        /// <returns>解密后的字符串</returns>
        public static string Decode(string base64String, Encoding encoding = null)
        {
            encoding = encoding ?? Encoding.UTF8;
            string decode = string.Empty;
            byte[] bytes = Convert.FromBase64String(base64String);
            try
            {
                decode = encoding.GetString(bytes);
            }
            catch
            {
                decode = base64String;
            }
            return decode;
        }
    }
}
