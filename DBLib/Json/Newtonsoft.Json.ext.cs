﻿/*
 * 项目地址:http://git.oschina.net/ggshihai/DBLib
 * Author:DeepBlue
 * QQ群:257018781
 * Email:xshai@163.com
 * 说明:一些常用的操作类库.
 * 额外说明:东拼西凑的东西,没什么技术含量,爱用不用,用了你不吃亏,用了你不上当,不用你也取不了媳妇...
 * -------------------------------------------------- 
 * -----------我是长长的美丽的善良的分割线-----------
 * -------------------------------------------------- 
 * 我曾以为无惧时光荏苒 如今明白谁都逃不过似水流年
 * --------------------------------------------------  
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;
using System.Collections;

namespace DBLib.Json
{
    public class JsonHelper
    {
        /// <summary>
        /// 将json数据反序列化为Dictionary
        /// </summary>
        /// <param name="jsonData">json数据</param>
        /// <returns></returns>
        public static Dictionary<string, object> ToDictionary(string jsonData)
        {
            try
            {
                //实例化JavaScriptSerializer类的新实例
                //JavaScriptSerializer jss = new JavaScriptSerializer();
                //将指定的 JSON 字符串转换为 Dictionary<string, object> 类型的对象
                //return jss.Deserialize<Dictionary<string, object>>(jsonData);
                return JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonData);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static string GetKeyValue(string key, string jsonSource)
        {
            var dict = ToDictionary(jsonSource);
            Dictionary<string, object> dataSet = (Dictionary<string, object>)dict["dataSet"];
            var sb = new System.Text.StringBuilder();
            //使用KeyValuePair遍历数据
            foreach (KeyValuePair<string, object> item in dataSet)
            {
                if (item.Key == key)
                {
                    var subItem = (Dictionary<string, object>)item.Value;
                    foreach (var kv in subItem)
                    {
                        //textBox1.AppendText(str.Key + ":" + str.Value + "\r\n");//显示到界面
                        sb.Append(kv.Key);
                    }
                    return sb.ToString();
                    break;
                }
            }
            return null;
        }

        public static string SerializeObject(object value)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(value);
        }

        /// <summary>
        /// 序列化成json,数据包含Datetime类型字段
        /// </summary>
        /// <param name="value"></param>
        /// <param name="dateTimeFormat">Datetime转换成的格式</param>
        /// <returns></returns>
        public static string SerializeObjectDate(object value, string dateTimeFormat = "yyyy-MM-dd HH:mm:ss", Newtonsoft.Json.Formatting format = Newtonsoft.Json.Formatting.None)
        {
            var timeConverter = new IsoDateTimeConverter { DateTimeFormat = dateTimeFormat };
            return JsonConvert.SerializeObject(value, format, timeConverter);
        }

        public static object DeserializeObject(string value)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject(value);
        }

        public static T DeserializeObject<T>(string value)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(value);
        }
        public static T DeserializeObject<T>(object value)
        {
            var json = SerializeObject(value);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }

        public static T DeserializeObject<T>(string value, JsonSerializerSettings settings)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(value, settings);
        }

        public static List<TResult> ToListModels<TResult>(object source)
        {
            var list = new List<TResult>();
            if (source == null) return list;
            var json = SerializeObject(source);
            list = DeserializeObject<List<TResult>>(json);
            return list;
        }
    }

    /// <summary>
    /// Newtonsoft.Json 属性小写类
    /// </summary>
    public class LowercaseJsonSerializer
    {
        private static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            ContractResolver = new LowercaseContractResolver()
        };

        /// <summary>
        /// 属性全小写
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string SerializeObject(object o)
        {
            return JsonConvert.SerializeObject(o, Settings);
        }

        public class LowercaseContractResolver : DefaultContractResolver
        {
            protected override string ResolvePropertyName(string propertyName)
            {
                return propertyName.ToLower();
            }
        }

        /// <summary>
        /// 属性首字母小写
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string FirstLetterLowercase(object obj)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj, Formatting.Indented,
            new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
        }
    }
}
