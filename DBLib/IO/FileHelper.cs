﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DBLib.IO
{
    public class FileHelper
    {

        private string path { get; set; }

        public FileHelper() { }

        public FileHelper(string path)
        {
            this.path = path;
        }

        /// <summary>
        /// 删除文件到回收站
        /// </summary>
        /// <param name="file"></param>
        public static void DeleteToRecycleBin(string file)
        {
            try { FileSystem.DeleteFile(file, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin); }
            catch { }
        }

        /// <summary>
        /// 取文件的扩展名
        /// </summary>
        /// <param name="FileName">文件名称</param>
        /// <returns>string</returns>
        public static string GetExtFileTypeName(string FileName)
        {
            string sFile = FileName;// myFile.PostedFile.FileName;
            sFile = sFile.Substring(sFile.LastIndexOf("\\") + 1);
            sFile = sFile.Substring(sFile.LastIndexOf(".")).ToLower();
            return sFile;
        }

        /// <summary>
        /// 返回结果:包含指定路径的扩展名（包括“.”）
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetExtensionName(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName)) return string.Empty;
            return System.IO.Path.GetExtension(fileName);
        }

        public bool SaveConfig<T>(T model)
        {
            try
            {
                using (var fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(T));
                    xs.Serialize(fs, model);
                    return true;
                }
            }
            catch { return false; }
        }

        public T ReadConfig<T>()
        {
            try
            {
                using (var sr = new StreamReader(path))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(T));
                    return (T)xs.Deserialize(sr);
                }
            }
            catch
            {
                return default(T);
            }
        }


        public static bool SaveConfig<T>(T model, string path)
        {
            try
            {
                using (var fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(T));
                    xs.Serialize(fs, model);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static T ReadConfig<T>(string path)
        {
            try
            {
                using (var sr = new StreamReader(path))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(T));
                    return (T)xs.Deserialize(sr);
                }
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }

        /// <summary>
        /// 将文件大小(字节)转换为最适合的显示方式
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static string ConvertFileSize(long? size)
        {
            if (size == null) return "0";
            string result = "0KB";
            int filelength = size.ToString().Length;
            if (filelength < 4)
                result = size + "byte";
            else if (filelength < 7)
                result = Math.Round(Convert.ToDouble(size / 1024d), 2) + "KB";
            else if (filelength < 10)
                result = Math.Round(Convert.ToDouble(size / 1024d / 1024), 2) + "MB";
            else if (filelength < 13)
                result = Math.Round(Convert.ToDouble(size / 1024d / 1024 / 1024), 2) + "GB";
            else
                result = Math.Round(Convert.ToDouble(size / 1024d / 1024 / 1024 / 1024), 2) + "TB";
            return result;
        }
        public static string ReadAllText(string fileName)
        {
            return File.ReadAllText(fileName, GetEncoding(fileName));
        }

        public static Encoding GetEncoding(string filename)
        {
            try
            {
                return Encoding.GetEncoding(GetCharset(filename));
            }
            catch (Exception ex)
            {
                return Encoding.Default;
            }
        }

        public static string GetCharset(string filename)
        {
            using (var fs = File.OpenRead(filename))
            {
                Ude.CharsetDetector cdet = new Ude.CharsetDetector();
                cdet.Feed(fs);
                cdet.DataEnd();
                return cdet.Charset;
                //if (cdet.Charset != null)
                //{
                //    Console.WriteLine("Charset: {0}, confidence: {1}",
                //         cdet.Charset, cdet.Confidence);
                //}
                //else
                //{
                //    Console.WriteLine("Detection failed.");
                //}
            }
        }
    }
}
