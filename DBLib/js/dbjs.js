﻿/// <reference path="../jquery/jquery-3.6.0.min.js" />

if (typeof Object.assign != 'function') {
    Object.assign = function (target) {
        'use strict';
        if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object');
        }
        target = Object(target);
        for (var index = 1; index < arguments.length; index++) {
            var source = arguments[index];
            if (source != null) {
                for (var key in source) {
                    if (Object.prototype.hasOwnProperty.call(source, key)) {
                        target[key] = source[key];
                    }
                }
            }
        }
        return target;
    };
}

//const var
var PX100 = "100px"; var PX150 = "150px";
var PX200 = "200px"; var PX250 = "250px";
var PX300 = "300px"; var PX350 = "350px";
var PX400 = "400px"; var PX450 = "450px";
var PX500 = "500px"; var PX550 = "550px";
var PX600 = "600px"; var PX650 = "650px";
var PX700 = "700px"; var PX750 = "750px";
var PX800 = "800px"; var PX750 = "850px";
var PX800 = "900px"; var PX750 = "950px";
var PX800 = "1000px";

var PER10 = "10%"; var PER20 = "20%";
var PER30 = "30%"; var PER40 = "40%";
var PER50 = "50%"; var PER60 = "60%";
var PER70 = "70%"; var PER80 = "80%";
var PER90 = "90%"; var PER100 = "100%";

var $nn = (function () {
    var prototype = {
        refresh: function () { window.location.href = window.location.href; },
        refreshAll: function () { window.parent.location.href = window.parent.location.href; },
        setTitle: function () { window.document.title = title; },
        setParentTitle: function () { window.parent.document.title = title; },
        isSuccess: function (str) { return str.indexOf("成功") > -1; }
    };

    return prototype;
})();

/*
创建数据共享接口——简化框架之间相互传值
*/
var share = {
    /**
    * 跨框架数据共享接口
    * @param	{String}	存储的数据名
    * @param	{Any}		将要存储的任意数据(无此项则返回被查询的数据)
    */
    data: function (name, value) {
        var top = window.top,
            cache = top['_CACHE'] || {};
        top['_CACHE'] = cache;

        return value !== undefined ? cache[name] = value : cache[name];
    },

    /**
    * 数据共享删除接口
    * @param	{String}	删除的数据名
    */
    removeData: function (name) {
        var cache = window.top['_CACHE'];
        if (cache && cache[name]) delete cache[name];
    }

};

function newGuid() {
    function guid_S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    // then to call it, plus stitch in '4' in the third group
    return (guid_S4() + guid_S4() + "-" + guid_S4() + "-4" + guid_S4().substr(0, 3) + "-" + guid_S4() + "-" + guid_S4() + guid_S4() + guid_S4()).toLowerCase();
}


//======================Date 扩展================================
// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
// 例子： 
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
//调用：  
//var time1 = new Date().Format("yyyy-MM-dd");
//var time2 = new Date().Format("yyyy-MM-dd HH:mm:ss");

Date.prototype.Format = function (fmt) { //author: meizz 
    if (fmt == null || fmt == '' || fmt == undefined) fmt = "yyyy-MM-dd";
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日    
        "h+": this.getHours() == 0 ? 12 : this.getHours(), //小时       
        "H+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

Date.prototype.toStr = function (fmt) {
    return this.Format(fmt);
}

Date.prototype.addHours = function (val) {
    return new Date(this.setHours(this.getHours() + val));
}

Date.prototype.addDays = function (days) {
    return new Date(this.setDate(this.getDate() + days));
}
Date.prototype.addMonths = function (v) {
    return new Date(this.setMonth(this.getMonth() + v));
}
Date.prototype.addYears = function (v) {
    return new Date(this.setFullYear(this.getFullYear() + v));
}

//计算时间相差天数
function TotalDays(date2, date1) {
    return DiffDays(date2, date1);
}
//计算时间相差天数
function DiffDays(date2, date1) {
    var d1 = new Date(date1);
    var d2 = new Date(date2);
    var date3 = d2.getTime() - d1.getTime();  //时间差的毫秒数 

    //计算出相差天数
    var days = Math.floor(date3 / (24 * 3600 * 1000));
    return days;
}

/*
是否为int
*/
String.prototype.isInt = function () {
    return !isNaN(parseInt(this));
}
String.prototype.isFloat = function () {
    return !isNaN(parseFloat(this));
}
String.prototype.isNullOrEmpty = function () {

}
/*
 json helper
 */

var dbjs = {};
dbjs.objIsExist = function (obj) {
    if (!obj) return false;
    for (var key in obj) {
        return true;
    }
    return false;
}
dbjs.setValueByJson = function (json, arrIgnore) {
    if (this.objIsExist(json) == false) return false;
    arrIgnore = arrIgnore || [];
    for (var key in json) {
        if (arrIgnore.indexOf(key) > -1) continue;
        var dom = $("#" + key);
        if (dom.length == 0 || dom.attr("type") == "file") continue;
        if (dom.is("select") == true) {
            //如是select无对应值则跳过
            var isExist = false;
            var count = dom.find('option').length;
            for (var i = 0; i < count; i++) {
                var v = dom.get(0).options[i].value;
                //0=="",所以
                if ((json[key] == 0 && v == "") || (json[key] == "" && v == 0)) {
                    isExist = (parseInt(v) === parseInt(json[key]));
                    if (isExist) break;
                    continue;
                }
                if (v == json[key]) {
                    isExist = true; break;
                }
            }
            //console.log(key + ",selectcount=" + count + ",exist=" + isExist + ",v=" + json[key])
            if (!isExist) continue;
        }
        if (dom.is("input") == false && dom.is("select") == false && dom.is("textarea") == false) {
            dom.html(json[key]);
            dom.val(json[key]);
        }
        else dom.val(json[key]);
    }
}
dbjs.setToday = function (elementID) {
    document.getElementById(elementID).value = new Date().Format();
}

//parseParams(obj); //"name=zhangsan&age=100"
dbjs.parseParams = function (data) {
    try {
        var tempArr = [];
        for (var i in data) {
            var key = encodeURIComponent(i);
            var value = encodeURIComponent(data[i]);
            tempArr.push(key + '=' + value);
        }
        var urlParamsStr = tempArr.join('&');
        return urlParamsStr;
    } catch (err) {
        return '';
    }
}

//getParams(urlStr); //{name: "zhangshan", age: "100"}
dbjs.getParams = function (url) {
    try {
        var index = url.indexOf('?');
        url = url.match(/\?([^#]+)/)[1];
        var obj = {}, arr = url.split('&');
        for (var i = 0; i < arr.length; i++) {
            var subArr = arr[i].split('=');
            var key = decodeURIComponent(subArr[0]);
            var value = decodeURIComponent(subArr[1]);
            obj[key] = value;
        }
        return obj;

    } catch (err) {
        return null;
    }
}

dbjs.changeExt = function (fileName, newExt) {
    var _tmp
    return fileName.substr(0, ~(_tmp = fileName.lastIndexOf('.')) ? _tmp : fileName.length) + '.' + newExt;
}

dbjs.arrIndexOf = function (arr, key, val) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i][key] == val) return i;
    }
    return -1;
}

dbjs.arrRemove = function (arr, key, val) {
    var index = dbjs.arrIndexOf(arr, key, val);
    if (index > -1) {
        arr.splice(index, 1);
    }
}
dbjs.arrQuery = function (arr, key, val) {
    var index = dbjs.arrIndexOf(arr, key, val);
    if (index == -1) return null;
    return arr[index];
}
dbjs.arrQuerySet = function (arr, key, val) {
    var res = [];
    for (var index in arr) {
        if (arr[index][key] != val) continue;
        res.push(arr[index]);
    }
    return res;
}
dbjs.arrUnique = function (array, key) {
    var result = [array[0]];
    for (var i = 1; i < array.length; i++) {
        var item = array[i];
        var repeat = false;
        for (let j = 0; j < result.length; j++) {
            if (item[key] == result[j][key]) {
                repeat = true;
                break;
            }
        }
        if (!repeat) {
            result.push(item);
        }
    }
    return result;
}

dbjs.deepCopy = function (obj) {
    var result = Array.isArray(obj) ? [] : {};
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            if (typeof obj[key] === 'object' && obj[key] !== null) {
                result[key] = dbjs.deepCopy(obj[key]);   //递归复制
            } else {
                result[key] = obj[key];
            }
        }
    }
    return result;
}