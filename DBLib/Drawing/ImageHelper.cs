﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace DBLib.Drawing
{
    public class ImageHelper
    {
        public enum WaterMarkPosition
        {
            LeftTop,
            LeftBottom,
            RightTop,
            RightBottom,
            TopMiddle,     //顶部居中
            BottomMiddle, //底部居中
            Center
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcPath"></param>
        /// <param name="savePath"></param>
        /// <param name="watertext"></param>
        /// <param name="color"></param>
        /// <param name="alpha">有效值为 0 到 255 之间。黑表示透明，白表示不透明，灰表示半透明</param>
        /// <param name="fontSize"></param>
        /// <param name="position"></param>
        public static void AddWaterText(string srcPath, string watertext, string color, string savePath = null, int alpha = 255, int fontSize = 16, WaterMarkPosition position = WaterMarkPosition.RightBottom)
        {
            var isSame = savePath.IsNullOrEmpty() || srcPath == savePath;
            var ext = Path.GetExtension(srcPath).ToLower();
            if (isSame) savePath = Guid.NewGuid().ToString() + ext;
            Image image = Image.FromFile(srcPath);
            Bitmap bitmap = new Bitmap(image.Width, image.Height);
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.Clear(Color.White);
            graphics.DrawImage(image, new Rectangle(0, 0, image.Width, image.Height), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
            Font font = new Font("arial", fontSize);
            SizeF ziSizeF = graphics.MeasureString(watertext, font);
            float x = 0f;
            float y = 0f;
            switch (position)
            {
                case WaterMarkPosition.LeftTop:
                    x = ziSizeF.Width / 2f;
                    y = 8f;
                    break;
                case WaterMarkPosition.LeftBottom:
                    x = ziSizeF.Width / 2f;
                    y = image.Height - ziSizeF.Height;
                    break;
                case WaterMarkPosition.RightTop:
                    x = image.Width * 1f - ziSizeF.Width / 2f;
                    y = 8f;
                    break;
                case WaterMarkPosition.RightBottom:
                    x = image.Width - ziSizeF.Width;
                    y = image.Height - ziSizeF.Height;
                    break;
                case WaterMarkPosition.TopMiddle:
                    x = image.Width / 2;
                    y = 8f;
                    break;
                case WaterMarkPosition.BottomMiddle:
                    x = image.Width / 2;
                    y = image.Height - ziSizeF.Height - 10;
                    break;
                case WaterMarkPosition.Center:
                    x = image.Width / 2;
                    y = image.Height / 2 - ziSizeF.Height / 2;
                    break;
            }
            try
            {
                StringFormat stringFormat = new StringFormat { Alignment = StringAlignment.Center };
                SolidBrush solidBrush = new SolidBrush(Color.FromArgb(alpha, 0, 0, 0));
                graphics.DrawString(watertext, font, solidBrush, x + 1f, y + 1f, stringFormat);
                SolidBrush brush = new SolidBrush(Color.FromArgb(alpha, ColorTranslator.FromHtml(color)));
                graphics.DrawString(watertext, font, brush, x, y, stringFormat);
                //graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
                //graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.Default;
                solidBrush.Dispose();
                brush.Dispose();
                if (ext.EndsWithX("jpg") || ext.EndsWithX("jpeg"))
                {
                    var myImageCodecInfo = GetEncoderInfo("image/jpeg");

                    // Create an Encoder object based on the GUID
                    // for the Quality parameter category.
                    var myEncoder = System.Drawing.Imaging.Encoder.Quality;

                    // Create an EncoderParameters object.

                    // An EncoderParameters object has an array of EncoderParameter

                    // objects. In this case, there is only one

                    // EncoderParameter object in the array.
                    var myEncoderParameters = new EncoderParameters(1);

                    // Save the bitmap as a JPEG file with quality level 25.
                    var myEncoderParameter = new EncoderParameter(myEncoder, 80L);
                    myEncoderParameters.Param[0] = myEncoderParameter;
                    bitmap.Save(savePath, myImageCodecInfo, myEncoderParameters);
                }
                else
                {
                    bitmap.Save(savePath);
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                bitmap.Dispose();
                image.Dispose();
            }
            if (isSame)
            {
                File.Delete(srcPath);
                File.Move(savePath, srcPath);
            }
        }

        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }


        public ImageFormat GetImageFormatByExt(string ext)
        {
            ImageFormat format = null;
            switch (ext.ToLower().Replace(".", ""))
            {
                case "bmp": format = ImageFormat.Bmp; break;
                case "emf": format = ImageFormat.Emf; break;
                case "wmf": format = ImageFormat.Wmf; break;
                case "gif": format = ImageFormat.Gif; break;
                case "jpg": format = ImageFormat.Jpeg; break;
                case "jpeg": format = ImageFormat.Jpeg; break;
                case "png": format = ImageFormat.Png; break;
                case "tiff": format = ImageFormat.Tiff; break;
                case "exif": format = ImageFormat.Exif; break;
                case "icon": format = ImageFormat.Icon; break;
                default: format = ImageFormat.Jpeg; break;
            }
            return format;
        }

        /// <summary>
        /// 将图片转成base64
        /// </summary>
        /// <param name="imageFilename">本地图片路径</param>
        /// <param name="imageFormat">[可选的]图片格式扩展,如jpg,png等(不带.)</param>
        /// <returns>base64</returns>
        public string ImageToBase64String(string imageFilename, string imageFormat = null)
        {
            try
            {
                Bitmap bmp = new Bitmap(imageFilename);
                imageFormat = imageFormat ?? DBLib.IO.Path.GetExtensionNoPoint(imageFilename);
                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, GetImageFormatByExt(imageFormat));
                byte[] arr = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(arr, 0, (int)ms.Length);
                ms.Close();
                return Convert.ToBase64String(arr);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 将图片转成带DataURIScheme的base64
        /// </summary>
        /// <param name="imageFilename">本地图片路径</param>
        /// <param name="imageFormat">[可选的]图片格式扩展,如jpg,png等(不带.)</param>
        /// <returns>带DataURIScheme的base64</returns>
        public string ImageToBase64WithDataScheme(string imageFilename, string imageFormat = null)
        {
            imageFormat = imageFormat ?? DBLib.IO.Path.GetExtensionNoPoint(imageFilename);
            var base64 = ImageToBase64String(imageFilename);
            return string.Format("data:image/{0};base64,{1}", imageFormat, base64);
        }

        //threeebase64编码的字符串转为图片  
        protected Bitmap Base64StringToImage(string strbase64)
        {
            try
            {
                byte[] arr = Convert.FromBase64String(strbase64);
                MemoryStream ms = new MemoryStream(arr);
                Bitmap bmp = new Bitmap(ms);

                //bmp.Save(@"d:\test.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                //bmp.Save(@"d:\"test.bmp", ImageFormat.Bmp);  
                //bmp.Save(@"d:\"test.gif", ImageFormat.Gif);  
                //bmp.Save(@"d:\"test.png", ImageFormat.Png);  
                ms.Close();
                return bmp;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public byte[] ImageToByte(string imageFilename)
        {
            try
            {
                FileStream fs = new FileStream(imageFilename, FileMode.Open, FileAccess.Read);
                Byte[] btye2 = new byte[fs.Length];
                fs.Read(btye2, 0, Convert.ToInt32(fs.Length));
                fs.Close();
                return btye2;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
