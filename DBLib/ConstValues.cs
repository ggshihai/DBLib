﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBLib
{
    /// <summary>
    /// 常用常量
    /// </summary>
    public class ConstValues
    {
        /// <summary>
        /// string值POST
        /// </summary>
        public const string POST = "POST";

        /// <summary>
        /// string值GET
        /// </summary>
        public const string GET = "GET";

        public const string LoginSession = "LoginSession";

        /// <summary>
        /// 常用图片扩展类型 .bmp .jpg .jpeg .png .gif
        /// </summary>
        public static readonly List<string> ImageExtensions = new List<string>() { ".bmp", ".jpg", ".jpeg", ".png", ".gif" };
    }
}
