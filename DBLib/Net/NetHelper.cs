﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;

namespace DBLib.Net
{
    public class NetHelper
    {
        #region C# IP内外网判断
        /// <summary>
        /// 判断IP地址是否为内网IP地址
        /// </summary>
        /// <param name="ipAddress">IP地址字符串</param>
        /// <returns></returns>
        public static bool IsInnerIP(string ipAddress)
        {
            bool isInnerIp = false;
            long ipNum = GetIpNum(ipAddress);
            /**
私有IP：A类 10.0.0.0-10.255.255.255
B类 172.16.0.0-172.31.255.255
C类 192.168.0.0-192.168.255.255
当然，还有127这个网段是环回地址
            **/
            long aBegin = GetIpNum("10.0.0.0");
            long aEnd = GetIpNum("10.255.255.255");
            long bBegin = GetIpNum("172.16.0.0");
            long bEnd = GetIpNum("172.31.255.255");
            long cBegin = GetIpNum("192.168.0.0");
            long cEnd = GetIpNum("192.168.255.255");
            isInnerIp = IsInner(ipNum, aBegin, aEnd) || IsInner(ipNum, bBegin, bEnd) || IsInner(ipNum, cBegin, cEnd) || ipAddress.Equals("127.0.0.1") || ipAddress.Equals("::1");
            return isInnerIp;
        }

        /// <summary>
        /// 把IP地址转换为Long型数字
        /// </summary>
        /// <param name="ipAddress">IP地址字符串</param>
        /// <returns></returns>
        private static long GetIpNum(string ipAddress)
        {
            string[] ip = ipAddress.Split('.');
            long a = int.Parse(ip[0]);
            long b = int.Parse(ip[1]);
            long c = int.Parse(ip[2]);
            long d = int.Parse(ip[3]);

            long ipNum = a * 256 * 256 * 256 + b * 256 * 256 + c * 256 + d;
            return ipNum;
        }

        /// <summary>
        /// 判断用户IP地址转换为Long型后是否在内网IP地址所在范围
        /// </summary>
        /// <param name="userIp"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        private static bool IsInner(long userIp, long begin, long end)
        {
            return (userIp >= begin) && (userIp <= end);
        }

        #endregion


        /// <summary>
        /// Model对象转换为uri网址参数形式
        /// </summary>
        /// <param name="obj">Model对象</param>
        /// <param name="url">前部分网址</param>
        /// <returns></returns>
        public static string ToUriParam(object obj, string url = "", bool isUrlEncode = false)
        {
            PropertyInfo[] propertis = obj.GetType().GetProperties();
            StringBuilder sb = new StringBuilder();
            sb.Append(url);
            sb.Append("?");
            foreach (var p in propertis)
            {
                var v = p.GetValue(obj, null);
                if (v == null)
                    continue;

                sb.Append(p.Name);
                sb.Append("=");
                if (isUrlEncode) sb.Append(System.Web.HttpUtility.UrlEncode(v.ToString()));
                else sb.Append(v.ToString());
                sb.Append("&");
            }
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }

        /// <summary>
        /// 根据url获取页面内容
        /// </summary>
        /// <param name="url">要获取的url</param>
        /// <param name="timeout">超时时间(毫秒),默认为1分钟=60,000毫秒</param>
        /// <returns></returns>
        public static string GetContentByUrl(string url, int timeout = 60000, bool isIgnoreError = true)
        {
            string strResult = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                //声明一个HttpWebRequest请求  
                request.Timeout = timeout;
                //设置连接超时时间  
                request.Headers.Set("Pragma", "no-cache");
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream streamReceive = response.GetResponseStream();
                Encoding encoding = Encoding.GetEncoding("UTF-8");
                StreamReader streamReader = new StreamReader(streamReceive, encoding);
                strResult = streamReader.ReadToEnd();
                streamReader.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                if (!isIgnoreError)
                    throw ex;
            }
            return strResult;
        }

        /// <summary>
        /// 根据url获取页面内容
        /// </summary>
        /// <param name="url">要获取的url</param>
        /// <param name="timeout">超时时间(毫秒),默认为1分钟=60,000毫秒</param>
        /// <returns></returns>
        public static string GetContentByUrl(string url, Encoding encoding, int timeout = 60000, bool isIgnoreError = true)
        {
            string strResult = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                //声明一个HttpWebRequest请求  
                request.Timeout = timeout;
                //设置连接超时时间  
                request.Headers.Set("Pragma", "no-cache");
                request.Headers.Set("ClientId", "7DE09D475B27EC5B");
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream streamReceive = response.GetResponseStream();
                //Encoding encoding = Encoding.GetEncoding("UTF-8");
                StreamReader streamReader = new StreamReader(streamReceive, encoding);
                strResult = streamReader.ReadToEnd();
                streamReader.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                if (!isIgnoreError)
                    throw ex;
            }
            return strResult;
        }


        /// <summary>
        /// 以表单形式,POST请求
        /// </summary>
        /// <param name="url">链接</param>
        /// <param name="data">要POST的数据</param>
        /// <param name="timeout">超时时间(毫秒),默认为1分钟=60,000毫秒</param>
        /// <returns></returns>
        public static string PostContentByUrl(string url, string data = null, int timeout = 60000)
        {
            string strResult = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                //声明一个HttpWebRequest请求  
                request.Timeout = timeout;
                //设置连接超时时间  
                //request.Headers.Set("Pragma", "no-cache");
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                if (!string.IsNullOrWhiteSpace(data))
                {
                    byte[] postBytes = Encoding.GetEncoding("utf-8").GetBytes(data);
                    request.ContentLength = postBytes.Length;
                    Stream newStream = request.GetRequestStream();
                    newStream.Write(postBytes, 0, postBytes.Length);
                    newStream.Close();
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream streamReceive = response.GetResponseStream();
                Encoding encoding = Encoding.GetEncoding("UTF-8");
                StreamReader streamReader = new StreamReader(streamReceive, encoding);
                strResult = streamReader.ReadToEnd();
                streamReader.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strResult;
        }
    }
}
