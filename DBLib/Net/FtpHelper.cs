﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace DBLib.Net
{
    public class FtpHelper
    {
        public string FtpHost { get; set; }
        public string FtpPort { get; set; }
        public string FtpUserName { get; set; }
        public string FtpPassWord { get; set; }
        private string FtpURI;

        private static NetworkCredential credential;
        public FtpHelper() { }

        public FtpHelper(string host, string port, string userName, string password)
        {
            FtpHost = host;
            FtpPort = port;
            FtpUserName = userName;
            FtpPassWord = password;
            credential = new NetworkCredential(FtpUserName, FtpPassWord);
            FtpURI = "{0}:{1}/".FormatWith(host, port);
        }

        /// <summary>
        /// 上传文件
        /// </summary> 
        /// <param name="uploadDir">目录,默认为/根目录</param>
        /// <param name="uploadFilePath"></param>
        /// <param name="localFileName"></param>
        /// <returns></returns>
        public string FtpUpload(string uploadDir, string uploadFilePath, string localFileName)
        {
            try
            {
                FtpCheckDirectoryExist(uploadDir);
                //构造一个web服务器的请求对象 
                //实例化一个文件对象 
                var f = new FileInfo(localFileName);
                var ftp = (FtpWebRequest)WebRequest.Create(new Uri(FtpHost + uploadDir + uploadFilePath));
                //创建用户名和密码 
                ftp.Credentials = credential;// new NetworkCredential(FtpUserName, FtpPassWord);
                ftp.KeepAlive = false;
                ftp.Method = WebRequestMethods.Ftp.UploadFile;
                ftp.UseBinary = true;
                ftp.ContentLength = f.Length;
                //获得请求对象的输入流 
                FileStream fs = f.OpenRead();
                byte[] buff = new byte[fs.Length];
                Stream sw = ftp.GetRequestStream();
                var contentLen = fs.Read(buff, 0, buff.Length);
                while (contentLen != 0)
                {
                    sw.Write(buff, 0, contentLen);
                    contentLen = fs.Read(buff, 0, buff.Length);
                }
                sw.Close();
                fs.Close();
                return "0";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //判断文件的目录是否存,不存则创建  
        public void FtpCheckDirectoryExist(string destFilePath)
        {
            string fullDir = FtpParseDirectory(destFilePath);
            string[] dirs = fullDir.Split('/');
            string curDir = "/";
            for (int i = 0; i < dirs.Length; i++)
            {
                string dir = dirs[i];
                //如果是以/开始的路径,第一个为空    
                if (dir != null && dir.Length > 0)
                {
                    try
                    {
                        curDir += dir + "/";
                        FtpMakeDir(curDir);
                    }
                    catch (Exception)
                    { }
                }
            }
        }

        public string FtpParseDirectory(string destFilePath)
        {
            return destFilePath.Substring(0, destFilePath.LastIndexOf("/"));
        }

        //创建目录  
        public Boolean FtpMakeDir(string localFile)
        {
            FtpWebRequest req = (FtpWebRequest)WebRequest.Create(FtpURI + localFile);
            req.Credentials = credential;// new NetworkCredential(FtpUserName, FtpPassWord);
            req.Method = WebRequestMethods.Ftp.MakeDirectory;
            try
            {
                FtpWebResponse response = (FtpWebResponse)req.GetResponse();
                response.Close();
            }
            catch (Exception)
            {
                req.Abort();
                return false;
            }
            req.Abort();
            return true;
        }

        /// <summary>
        /// FTP下载文件
        /// </summary>
        /// <param name="ftpPath">ftp相对路径</param>
        /// <param name="localSavePath">保存地址</param>
        /// <param name="userName">FTP登录帐号</param>
        /// <param name="passWord">FTP登录密码</param>
        public string FtpDownload(string ftpPath, string localSavePath)
        {
            //定义FTP请求对象
            FtpWebRequest ftpRequest;
            //定义FTP响应对象
            FtpWebResponse ftpResponse = null;
            //存储流
            FileStream saveStream = null;
            //FTP数据流
            Stream ftpStream = null;

            try
            {
                //生成下载文件
                saveStream = new FileStream(localSavePath, FileMode.OpenOrCreate);
                //生成FTP请求对象
                ftpRequest = (FtpWebRequest)WebRequest.Create(new Uri(FtpURI + ftpPath));

                //设置下载文件方法
                ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;

                //设置文件传输类型
                ftpRequest.UseBinary = true;

                //设置登录FTP帐号和密码
                ftpRequest.Credentials = credential;// new NetworkCredential(FtpUserName, FtpPassWord);

                //生成FTP响应对象
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();

                //获取FTP响应流对象
                ftpStream = ftpResponse.GetResponseStream();

                //响应数据长度
                long cl = ftpResponse.ContentLength;

                int bufferSize = 2048;

                int readCount;

                byte[] buffer = new byte[bufferSize];

                //接收FTP文件流
                readCount = ftpStream.Read(buffer, 0, bufferSize);

                while (readCount > 0)
                {
                    saveStream.Write(buffer, 0, readCount);

                    readCount = ftpStream.Read(buffer, 0, bufferSize);
                }

            }
            catch (Exception ex)
            {
                return "下载失败：" + ex.Message;
            }
            finally
            {
                if (ftpStream != null)
                {
                    ftpStream.Close();
                }

                if (saveStream != null)
                {
                    saveStream.Close();
                }
                if (ftpResponse != null)
                {
                    ftpResponse.Close();
                }
                FileInfo inf = new FileInfo(localSavePath);
                if (inf.Length == 0)
                {
                    File.Delete(localSavePath);
                }

            }
            return "0";
        }


        /// <summary>
        /// 获取当前目录下明细(包含文件和文件夹)
        /// </summary>
        /// <param name="folder">ftp服务器上的文件夹</param>
        /// <returns></returns>
        public string[] GetFilesDetailList(string folder = "")
        {
            //string[] downloadFiles;
            try
            {
                StringBuilder result = new StringBuilder();
                FtpWebRequest ftp;
                ftp = (FtpWebRequest)FtpWebRequest.Create(new Uri(FtpURI + folder));
                ftp.Credentials = credential;// new NetworkCredential(FtpUserName, FtpPassWord);
                ftp.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                ftp.UsePassive = false;
                WebResponse response = ftp.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.Default);

                string line = reader.ReadLine();

                while (line != null)
                {
                    result.Append(line);
                    result.Append("\n");
                    line = reader.ReadLine();
                }
                result.Remove(result.ToString().LastIndexOf("\n"), 1);
                reader.Close();
                response.Close();
                return result.ToString().Split('\n');
            }
            catch (Exception ex)
            {
                //downloadFiles = null;
                throw new Exception("FtpHelper  Error --> " + ex.Message);
            }
        }

        /// <summary>
        /// 获取当前目录下文件列表(仅文件)
        /// </summary>
        /// <param name="folder">ftp服务器上的文件夹</param>
        /// <param name="ext">指定的扩展名？</param>
        /// <returns></returns>
        public List<string> GetFileList(string folder = "", string ext = "")
        {
            List<string> list = new List<string>();
            //StringBuilder result = new StringBuilder();
            FtpWebRequest reqFTP;
            try
            {
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(FtpURI + folder));
                reqFTP.UseBinary = true;
                reqFTP.Credentials = credential;// new NetworkCredential(FtpUserName, FtpPassWord);
                reqFTP.Method = WebRequestMethods.Ftp.ListDirectory;
                reqFTP.UsePassive = false;
                WebResponse response = reqFTP.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.Default);

                string line = reader.ReadLine();
                while (line != null)
                {
                    if (ext.Trim() != string.Empty && ext.Trim() != "*.*")
                    {
                        //string mask_ = ext.Substring(0, ext.IndexOf("*"));
                        //if (line.Substring(0, mask_.Length) == mask_)
                        //{
                        //    result.Append(line);
                        //    result.Append("\n");
                        //}
                        if (line.EndsWith(ext)) list.Add(line);
                    }
                    else
                    {
                        list.Add(line);
                        //result.Append(line);
                        //result.Append("\n");
                    }
                    line = reader.ReadLine();
                }
                //result.Remove(result.ToString().LastIndexOf('\n'), 1);
                reader.Close();
                response.Close();
                //return result.ToString().Split('\n');
            }
            catch (Exception ex)
            {
                //downloadFiles = null;
                if (ex.Message.Trim() != "远程服务器返回错误: (550) 文件不可用(例如，未找到文件，无法访问文件)。")
                {
                    throw new Exception("FtpHelper GetFileList Error --> " + ex.Message.ToString());
                }
            }
            return list;
        }

        /// <summary>
        /// 获取当前目录下所有的文件夹列表(仅文件夹)
        /// </summary>
        /// <returns></returns>
        public string[] GetDirectoryList(string folder)
        {
            string[] drectory = GetFilesDetailList(folder);
            string m = string.Empty;
            foreach (string str in drectory)
            {
                int dirPos = str.IndexOf("<DIR>");
                if (dirPos > 0)
                {
                    /*判断 Windows 风格*/
                    m += str.Substring(dirPos + 5).Trim() + "\n";
                }
                else if (str.Trim().Substring(0, 1).ToUpper() == "D")
                {
                    /*判断 Unix 风格*/
                    string dir = str.Substring(54).Trim();
                    if (dir != "." && dir != "..")
                    {
                        m += dir + "\n";
                    }
                }
            }

            char[] n = new char[] { '\n' };
            return m.Split(n);
        }

    }
}