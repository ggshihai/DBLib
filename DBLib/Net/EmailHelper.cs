﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace DBLib.Net
{
    public class EmailHelper
    {
        string smtpServer; int port; string mailFrom; string userPassword;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="server">smtp服务器</param>
        /// <param name="_port">端口</param>
        /// <param name="userName">发件人邮箱</param>
        /// <param name="password">密码</param>
        public EmailHelper(string server, int _port, string userName, string password)
        {
            smtpServer = server;
            port = _port;
            mailFrom = userName;
            userPassword = password;
        }

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="mailTo">收件人</param>
        /// <param name="cc">抄送人</param>
        /// <param name="bcc">密送</param>
        /// <param name="subject">邮件主题</param>
        /// <param name="content">内容</param>
        /// <param name="attachments">附件</param>
        /// <returns>发送成功返回true否则false</returns>
        public bool SendEmail(string mailTo, string subject, string content, string cc = null, string bcc = null, string attachments = null, bool useHtml = true)
        {
            try
            {
                // 设置发送方的邮件信息
                // 邮件服务设置
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;//指定电子邮件发送方式
                smtpClient.Host = smtpServer; //指定SMTP服务器
                smtpClient.Port = port;//端口
                smtpClient.Credentials = new System.Net.NetworkCredential(mailFrom, userPassword);//验证用户名和密码
                smtpClient.EnableSsl = true; //使用SSL
                                             // 发送邮件设置       
                MailMessage mailMessage = new MailMessage(mailFrom, mailTo); // 发送人和收件人

                mailMessage.Subject = subject;//主题
                mailMessage.Body = content;//内容
                mailMessage.BodyEncoding = Encoding.UTF8;//正文编码
                mailMessage.IsBodyHtml = useHtml;//设置为HTML格式
                mailMessage.Priority = MailPriority.Normal;//优先级
                                                           //抄送人
                if (!string.IsNullOrEmpty(cc))
                    mailMessage.CC.Add(cc);
                //密送
                if (!string.IsNullOrEmpty(bcc))
                    mailMessage.Bcc.Add(bcc);
                //附件
                if (!string.IsNullOrEmpty(attachments))
                {
                    List<string> paths = new List<string>();
                    if (attachments.Contains(","))
                    {
                        paths = attachments.Split(',').ToList();
                    }
                    else
                    {
                        paths.Add(attachments);

                    }
                    foreach (var path in paths)
                    {
                        mailMessage.Attachments.Add(new Attachment(attachments));
                    }
                }
                smtpClient.Send(mailMessage); // 发送邮件
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
