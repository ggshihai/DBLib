﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace System
{
    public static class ObjectExtension
    {
        /// <summary>
        /// 将对象转为json字符串
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToJson(this object value)
        {
            return JsonConvert.SerializeObject(value);
        }

        /// <summary>
        /// 将对象转为json字符串
        /// </summary>
        public static string ToJsonDate(this object obj, string dateTimeFormat = "yyyy-MM-dd HH:mm:ss", Newtonsoft.Json.Formatting format = Newtonsoft.Json.Formatting.None)
        {
            var timeConverter = new IsoDateTimeConverter { DateTimeFormat = dateTimeFormat };
            return JsonConvert.SerializeObject(obj, format, timeConverter);
        }

        /// <summary>
        /// 通过newtonsoft.json克隆对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="myObject"></param>
        /// <returns></returns>
        public static T CloneByJson<T>(this T myObject)
        {
            return MapTo<T>(myObject);
        }

        /// <summary>
        /// 对象映射
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T MapTo<T>(this object value)
        {
            if (value.GetType() == typeof(string)) return value.ToString().ToObject<T>();
            return value.ToJson().ToObject<T>();
        }

        /// <summary>
        /// 将json字符串转换成对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="ignoreError"></param>
        /// <returns></returns>
        public static T ToObject<T>(this string value, bool ignoreError = true)
        {
            try
            {
                Newtonsoft.Json.JsonSerializerSettings settings = new Newtonsoft.Json.JsonSerializerSettings()
                {
                    NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
                    MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore
                };
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(value, settings);
            }
            catch (Exception ex)
            {
                if (ignoreError)
                    return default(T);
                throw ex;
            }
        }
         
        public static List<T> ToList<T>(this DataTable dt)
        {
            var list = new List<T>();
            if (dt == null || dt.Rows.Count == 0) return list;
            foreach (DataRow item in dt.Rows)
            {
                list.Add((T)item[0]);
            }
            return list;
        }
    }
}
