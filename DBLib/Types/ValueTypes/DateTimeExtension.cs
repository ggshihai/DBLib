﻿/*
 * 项目地址:http://git.oschina.net/ggshihai/DBLib
 * Author:DeepBlue
 * QQ群:257018781
 * Email:xshai@163.com
 * 说明:一些常用的操作类库.
 * 额外说明:东拼西凑的东西,没什么技术含量,爱用不用,用了你不吃亏,用了你不上当,不用你也取不了媳妇...
 * -------------------------------------------------- 
 * -----------我是长长的美丽的善良的分割线-----------
 * -------------------------------------------------- 
 * 我曾以为无惧时光荏苒 如今明白谁都逃不过似水流年
 * --------------------------------------------------  
 */
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace System
{
    /// <summary>
    /// DateTime 扩展方法
    /// </summary>
    public static class DateTimeExtension
    {
        /// <summary>
        /// 是否是DateTime类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsDateTime(this string value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value)) return false;
                Convert.ToDateTime(value);
                return true;
            }
            catch { return false; }
        }

        /// <summary>
        /// 转换成 System.DateTime
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(this DateTime? value)
        {
            return Convert.ToDateTime(value);
        }

        /// <summary>
        /// 转换成 System.DateTime
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime ToDateTimeOrDefault(this DateTime? value, DateTime? dtDefault = null)
        {
            if (value.HasValue)
                return Convert.ToDateTime(value);
            else if (dtDefault.HasValue) return dtDefault.ToDateTime();
            else return new DateTime(1970, 1, 1);
        }

        /// <summary>
        /// 转换成yyyy-MM的string类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToYyyyMM(this DateTime value)
        {
            return value.ToString("yyyy-MM");
        }

        /// <summary>
        /// 转换成yyyy-MM的string类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToYyyyMM(this DateTime? value)
        {
            if (!value.HasValue) return string.Empty;
            try
            {
                return Convert.ToDateTime(value).ToString("yyyy-MM");
            }
            catch { return string.Empty; }
        }

        /// <summary>
        /// 转换成yyyy-MM-dd的string类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToYyyyMMdd(this DateTime value)
        {
            return value.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 转换成yyyy-MM-dd的string类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToYyyyMMdd(this DateTime? value)
        {
            if (!value.HasValue) return string.Empty;
            try
            {
                return Convert.ToDateTime(value).ToString("yyyy-MM-dd");
            }
            catch { return string.Empty; }
        }

        /// <summary>
        /// 转换成yyyy-MM-dd的string类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToYyyyMMdd(this DateTime? value, string format)
        {
            if (!value.HasValue) return string.Empty;
            try
            {
                return Convert.ToDateTime(value).ToString(format);
            }
            catch { return string.Empty; }
        }

        /// <summary>
        /// 转换成yyyy-MM-dd的string类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToYyyyMMdd(this DateTime value, string format)
        {
            return value.ToString(format);
        }


        /// <summary>
        /// 转换成 yyyy-MM-dd HH:mm 的string类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToYyyyMMddHHmm(this DateTime? value)
        {
            return value.ToYyyyMMdd("yyyy-MM-dd HH:mm");
        }
        /// <summary>
        /// 转换成yyyy-MM-dd的string类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToYyyyMMdd(this string value, string format = "yyyy-MM-dd")
        {
            if (string.IsNullOrWhiteSpace(value)) return string.Empty;
            try
            {
                return Convert.ToDateTime(value).ToString(format);
            }
            catch { return string.Empty; }
        }

        /// <summary>
        /// 转换成yyyy/MM/dd的string类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToYyyyMMdd2(this DateTime value)
        {
            return value.ToString("yyyy\\/MM\\/dd");
        }

        /// <summary>
        /// 转换成yyyy/MM/dd的string类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToYyyyMMdd2(this DateTime? value)
        {
            if (value == null) return string.Empty;
            return value.ToYyyyMMdd();
        }

        /// <summary>
        /// 将字符串转成日期
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(this string value)
        {
            //if (string.IsNullOrEmpty(value)) return DateTime.MinValue;
            try
            {
                return Convert.ToDateTime(value);
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// 将字符串转成日期
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime? ToDateTimeOrNull(this string value)
        {
            try
            {
                return Convert.ToDateTime(value.Trim());
            }
            catch { return null; }
        }


        /// <summary>
        /// 使用指定的格式和区域性特定格式信息，将日期和时间的指定字符串表示形式转换为其等效的 System.DateTime。 字符串表示形式的格式必须与指定的格式完全匹配。
        /// </summary>
        /// <param name="value">包含要转换的日期和时间的字符串</param>
        /// <param name="format">用于定义所需的 s 格式的格式说明符。 有关详细信息，请参阅“备注”部分。</param>
        /// <param name="provider">一个对象，提供有关 s 的区域性特定格式信息。</param>
        /// <returns> 一个对象，它等效于 s 中包含的日期和时间，由 format 和 provider 指定。</returns>
        public static DateTime? ToDateTimeOrNull(this string value, string format, IFormatProvider provider = null)
        {
            try
            {
                if (provider == null) provider = System.Globalization.CultureInfo.CurrentCulture;
                return DateTime.ParseExact(value, format, provider);
            }
            catch { return null; }
        }

        /// <summary>
        /// 将字符串转成日期
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime? ToDateTimeOrNull(this string value, string[] formats, IFormatProvider provider, DateTimeStyles style)
        {
            try
            {
                return DateTime.ParseExact(value, formats, provider, style);
            }
            catch { return null; }
        }

        /// <summary>
        /// 将字符串转成日期
        /// </summary>
        /// <param name="value">要转换的字符串</param>
        /// <param name="defaultValue">转换失败后设置的默认值,如果不写则取DateTime.Now</param>
        /// <returns>转换失败后设置的默认值,如果不写则取DateTime.Now</returns>
        public static DateTime ToDateTimeOrDefault(this string value, DateTime? defaultValue = null)
        {
            try
            {
                return Convert.ToDateTime(value);
            }
            catch (Exception ex)
            {
                return defaultValue ?? DateTime.Now;
            }
        }

        /// <summary>  
        /// 得到本周第一天(以星期天为第一天)  
        /// </summary>  
        /// <param name="datetime"></param>  
        /// <returns></returns>  
        public static DateTime GetWeekFirstDaySun(this DateTime datetime)
        {
            //星期天为第一天  
            int weeknow = Convert.ToInt32(datetime.DayOfWeek);
            int daydiff = (-1) * weeknow;

            //本周第一天  
            DateTime FirstDay = datetime.AddDays(daydiff);
            return FirstDay;
        }

        /// <summary>  
        /// 得到本周第一天(以星期一为第一天)  
        /// </summary>  
        /// <param name="datetime"></param>  
        /// <returns></returns>  
        public static DateTime GetWeekFirstDayMon(this DateTime datetime)
        {
            //星期一为第一天  
            int weeknow = Convert.ToInt32(datetime.DayOfWeek);

            //因为是以星期一为第一天，所以要判断weeknow等于0时，要向前推6天。  
            weeknow = (weeknow == 0 ? (7 - 1) : (weeknow - 1));
            int daydiff = (-1) * weeknow;

            //本周第一天  
            DateTime FirstDay = datetime.AddDays(daydiff);
            return FirstDay;
        }

        /// <summary>  
        /// 得到本周最后一天(以星期六为最后一天)  
        /// </summary>  
        /// <param name="datetime"></param>  
        /// <returns></returns>  
        public static DateTime GetWeekLastDaySat(this DateTime datetime)
        {
            //星期六为最后一天  
            int weeknow = Convert.ToInt32(datetime.DayOfWeek);
            int daydiff = (7 - weeknow) - 1;

            //本周最后一天  
            DateTime LastDay = datetime.AddDays(daydiff);
            return LastDay;
        }

        /// <summary>  
        /// 获得当前时间的星期几
        /// <para>输出格式:SAT,SUN</para>
        /// </summary>  
        /// <param name="datetime"></param>  
        /// <returns></returns>  
        public static string GetDayOfWeek0(this DateTime datetime)
        {
            return datetime.DayOfWeek.ToString().Substring(0, 3).ToUpper();
        }

        /// <summary>  
        /// 获得当前时间的星期几
        /// <para>输出格式:一,二</para>
        /// </summary>  
        /// <param name="datetime"></param>  
        /// <returns></returns>  
        public static string GetDayOfWeek1(this DateTime datetime)
        {
            string day = null;
            switch (datetime.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    day = "日";
                    break;
                case DayOfWeek.Monday:
                    day = "一";
                    break;
                case DayOfWeek.Tuesday:
                    day = "二";
                    break;
                case DayOfWeek.Wednesday:
                    day = "三";
                    break;
                case DayOfWeek.Thursday:
                    day = "四";
                    break;
                case DayOfWeek.Friday:
                    day = "五";
                    break;
                case DayOfWeek.Saturday:
                    day = "六";
                    break;
                default: break;
            }
            return day;
        }

        /// <summary>
        /// 获得当前时间的星期几
        /// <para>输出格式:一,二 </para>
        /// </summary>
        /// <param name="strDatetime"></param>
        /// <returns></returns>
        public static string GetDayOfWeek1(this string strDatetime)
        {
            return GetDayOfWeek1(strDatetime.ToDateTime());
        }

        /// <summary>  
        /// 获得当前时间的星期几
        /// <para>输出格式:周一,周二</para>
        /// </summary>  
        /// <param name="datetime"></param>  
        /// <returns></returns>  
        public static string GetDayOfWeek2(this DateTime datetime)
        {
            string day = null;
            switch (datetime.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    day = "周日";
                    break;
                case DayOfWeek.Monday:
                    day = "周一";
                    break;
                case DayOfWeek.Tuesday:
                    day = "周二";
                    break;
                case DayOfWeek.Wednesday:
                    day = "周三";
                    break;
                case DayOfWeek.Thursday:
                    day = "周四";
                    break;
                case DayOfWeek.Friday:
                    day = "周五";
                    break;
                case DayOfWeek.Saturday:
                    day = "周六";
                    break;
                default: break;
            }
            return day;
        }

        /// <summary>  
        /// 获得当前时间的星期几
        /// <para>输出格式:周一,周二</para>
        /// </summary>  
        /// <param name="datetime"></param>  
        /// <returns></returns>  
        public static string GetDayOfWeek2(this string datetime)
        {
            return GetDayOfWeek2(datetime.ToDateTime());
        }

        /// <summary>  
        /// 获得当前时间的星期几
        ///<para>输出格式:星期一,星期二,</para> 
        /// </summary>  
        /// <param name="datetime"></param>  
        /// <returns></returns>  
        public static string GetDayOfWeek3(this DateTime datetime)
        {
            string day = null;
            switch (datetime.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    day = "星期日";
                    break;
                case DayOfWeek.Monday:
                    day = "星期一";
                    break;
                case DayOfWeek.Tuesday:
                    day = "星期二";
                    break;
                case DayOfWeek.Wednesday:
                    day = "星期三";
                    break;
                case DayOfWeek.Thursday:
                    day = "星期四";
                    break;
                case DayOfWeek.Friday:
                    day = "星期五";
                    break;
                case DayOfWeek.Saturday:
                    day = "星期六";
                    break;
                default: break;
            }
            return day;
        }

        /// <summary>  
        /// 获得当前时间的星期几
        /// <para>输出格式:星期一,星期二</para>
        /// </summary>  
        /// <param name="datetime"></param>  
        /// <returns></returns>  
        public static string GetDayOfWeek3(this string datetime)
        {
            return GetDayOfWeek3(datetime.ToDateTime());
        }

        /// <summary>  
        /// 得到本周最后一天(以星期天为最后一天)  
        /// </summary>  
        /// <param name="datetime"></param>  
        /// <returns></returns>  
        public static DateTime GetWeekLastDaySun(this DateTime datetime)
        {
            //星期天为最后一天  
            int weeknow = Convert.ToInt32(datetime.DayOfWeek);
            weeknow = (weeknow == 0 ? 7 : weeknow);
            int daydiff = (7 - weeknow);

            //本周最后一天  
            DateTime LastDay = datetime.AddDays(daydiff);
            return LastDay;
        }

        /// <summary>  
        /// 得到月份第一天
        /// </summary>  
        /// <param name="datetime"></param>  
        /// <returns></returns>  
        public static DateTime GetMonthFirstDay(this DateTime datetime)
        {
            return new DateTime(datetime.Year, datetime.Month, 1);
        }

        /// <summary>  
        /// 得到月份最后一天
        /// </summary>  
        /// <param name="datetime"></param>  
        /// <returns></returns>  
        public static DateTime GetMonthLastDay(this DateTime datetime)
        {
            return GetMonthFirstDay(datetime).AddMonths(1).AddDays(-1);
        }

        /// <summary>  
        /// 得到明天的日期
        /// </summary>  
        /// <param name="datetime"></param>  
        /// <returns></returns>  
        public static DateTime NextDay(this DateTime datetime)
        {
            return datetime.AddDays(1);
        }

        /// <summary>  
        /// 得到昨天的日期
        /// </summary>  
        /// <param name="datetime"></param>  
        /// <returns></returns>  
        public static DateTime Yesterday(this DateTime datetime)
        {
            return datetime.AddDays(-1);
        }

        /// <summary>
        /// 根据formatter比较两个时间是否相等
        /// </summary>
        /// <param name="dt1"></param>
        /// <param name="dt2"></param>
        /// <param name="formatter"></param>
        /// <returns></returns>
        public static bool IsEqualsByFormatter(this DateTime? dt1, DateTime? dt2, string formatter)
        {
            return Convert.ToDateTime(dt1).ToString(formatter) == Convert.ToDateTime(dt2).ToString(formatter);
        }

        /// <summary>
        /// 根据formatter比较两个时间是否相等
        /// </summary>
        /// <param name="dt1"></param>
        /// <param name="dt2"></param>
        /// <param name="formatter"></param>
        /// <returns></returns>
        public static bool IsEqualsByFormatter(this DateTime dt1, DateTime? dt2, string formatter)
        {
            return dt1.ToString(formatter) == Convert.ToDateTime(dt2).ToString(formatter);
        }

        /// <summary>
        /// 转换成时间戳,关于Unix时间戳,大概是这个意思,从1970年0时0分0秒开始到现在的秒数.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns>时间戳</returns>
        public static int ToTimeStamp(this DateTime dt)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (int)(dt - startTime).TotalSeconds;
        }

        /// <summary>
        /// 转换成时间戳,关于Unix时间戳,大概是这个意思,从1970年0时0分0秒开始到现在的秒数.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns>时间戳</returns>
        public static int ToTimeSpan(this DateTime? dt)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (int)(dt.ToDateTime() - startTime).TotalSeconds;
        }


        /// <summary>
        /// 转换成时间戳,关于Unix时间戳,大概是这个意思,从1970年0时0分0秒开始到现在的毫秒数.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns>时间戳</returns>
        public static long ToTimeStampLong(this DateTime dt)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (dt - startTime).Ticks / 10000;
        }

        /// <summary>
        /// 转换成时间戳,关于Unix时间戳,大概是这个意思,从1970年0时0分0秒开始到现在的毫秒数.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns>时间戳</returns>
        public static long ToTimeSpanLong(this DateTime? dt)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (dt.ToDateTime() - startTime).Ticks / 10000;
        }

        /// <summary>  
        /// 时间戳转为标准格式时间  
        /// </summary>  
        /// <param name="timeStamp">Unix时间戳格式(s)</param>  
        /// <returns>标准格式时间</returns>  
        public static DateTime ToDatetimeFromSeconds(this int seconds)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            //long lTime = long.Parse(timeStamp + "0000000");
            //TimeSpan toNow = new TimeSpan(lTime);
            //return dtStart.Add(toNow); 
            return dtStart.AddSeconds(Convert.ToDouble(seconds));
        }

        /// <summary>  
        /// 时间戳转为标准格式时间  
        /// </summary>  
        /// <param name="timeStamp">Unix时间戳格式(s)</param>  
        /// <returns>标准格式时间</returns>  
        public static DateTime ToDatetimeFromSeconds(this long seconds)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            //long lTime = long.Parse(timeStamp + "0000000");
            //TimeSpan toNow = new TimeSpan(lTime);
            //return dtStart.Add(toNow); 
            return dtStart.AddSeconds(Convert.ToDouble(seconds));
        }

        /// <summary>  
        /// 时间戳转为标准格式时间  
        /// </summary>  
        /// <param name="timeStamp">Unix时间戳格式(ms)</param>  
        /// <returns>标准格式时间</returns>  
        public static DateTime ToDatetimeFromMilliseconds(this long milliseconds)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            //long lTime = long.Parse(timeStamp + "0000000");
            //TimeSpan toNow = new TimeSpan(lTime);
            //return dtStart.Add(toNow); 
            return dtStart.AddMilliseconds(Convert.ToDouble(milliseconds));
        }


        /// <summary>
        /// 获取当前日期的月份英文前三字母,Jan,Feb
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string GetMonthOfYear1(this DateTime dt)
        {
            string ret = null;
            switch (dt.Month)
            {
                case 1: ret = "Jan"; break;
                case 2: ret = "Feb"; break;
                case 3: ret = "Mar"; break;
                case 4: ret = "Apr"; break;
                case 5: ret = "May"; break;
                case 6: ret = "Jun"; break;
                case 7: ret = "Jul"; break;
                case 8: ret = "Aug"; break;
                case 9: ret = "Sep"; break;
                case 10: ret = "Oct"; break;
                case 11: ret = "Nov"; break;
                case 12: ret = "Dec"; break;
                default: break;
            }
            return ret;
        }
    }
}
