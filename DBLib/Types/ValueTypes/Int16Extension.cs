﻿/*
 * 项目地址:http://git.oschina.net/ggshihai/DBLib
 * Author:DeepBlue
 * QQ群:257018781
 * Email:xshai@163.com
 * 说明:一些常用的操作类库.
 * 额外说明:东拼西凑的东西,没什么技术含量,爱用不用,用了你不吃亏,用了你不上当,不用你也取不了媳妇...
 * -------------------------------------------------- 
 * -----------我是长长的美丽的善良的分割线-----------
 * -------------------------------------------------- 
 * 我曾以为无惧时光荏苒 如今明白谁都逃不过似水流年
 * --------------------------------------------------  
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
    /// <summary>
    /// short 扩展方法
    /// </summary>
    public static class Int16Extension
    {
        public static bool ToBool(this short value)
        {
            return Convert.ToBoolean(value);
        }

        public static bool ToBool(this short? value)
        {
            try
            {
                return Convert.ToBoolean(value);
            }
            catch
            {
                return false;
            }
        }

        public static bool IsInt16(this string value)
        {
            try
            {
                short.Parse(value);
                return true;
            }
            catch { return false; }
        }

        /// <summary>
        /// 转换成short类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static short ToInt16(this string value)
        {
            try
            {
                return short.Parse(value.Trim());
            }
            catch (Exception ex) { throw ex; }
        }

        public static short ToInt16(this short? value)
        {
            try
            {
                return Convert.ToInt16(value);
            }
            catch (Exception ex) { throw ex; }
        }


        public static short ToInt16(this float value)
        {
            try
            {
                return Convert.ToInt16(value);
            }
            catch (Exception ex) { throw ex; }
        }

        public static short ToInt16(this float? value)
        {
            try
            {
                return Convert.ToInt16(value);
            }
            catch (Exception ex) { throw ex; }
        }

        public static short ToInt16(this double value)
        {
            try
            {
                return Convert.ToInt16(value);
            }
            catch (Exception ex) { throw ex; }
        }
        public static short ToInt16(this double? value)
        {
            try
            {
                return Convert.ToInt16(value);
            }
            catch (Exception ex) { throw ex; }
        }

        public static short ToShortOrZero(this short? value)
        {
            try
            {
                return Convert.ToInt16(value);
            }
            catch { return 0; }
        }

        public static short? ToShortOrNull(this string value)
        {
            try
            {
                if (value == null) return null;
                return Convert.ToInt16(value);
            }
            catch (Exception ex) { return null; }
        }

        /// <summary>
        /// 转换成short,如异常则返回 defaultValue
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static short ToShortOrDefault(this string value, short defaultValue = 0)
        {
            try
            {
                return short.Parse(value);
            }
            catch { return defaultValue; }
        }

        /// <summary>
        /// 转换成short,如异常则返回0
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static short ToShortOrZero(this string value)
        {
            try
            {
                return Convert.ToInt16(value);
            }
            catch { return 0; }
        }

        /// <summary>
        /// 除以
        /// </summary>
        /// <param name="value">被除数</param>
        /// <param name="num">除数</param>
        /// <returns></returns>
        public static decimal DividedBy(this short value, short num)
        {
            try
            {
                return value / (decimal)num;
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// 除以
        /// </summary>
        /// <param name="value">被除数</param>
        /// <param name="num">除数</param>
        /// <returns></returns>
        public static decimal DividedBy(this short value, float num)
        {
            try
            {
                return value / (decimal)num;
            }
            catch (Exception ex) { throw ex; }
        }


        /// <summary>
        /// 除以
        /// </summary>
        /// <param name="value">被除数</param>
        /// <param name="num">除数</param>
        /// <returns></returns>
        public static decimal DividedBy(this short value, double num)
        {
            try
            {
                return value / (decimal)num;
            }
            catch (Exception ex) { throw ex; }
        }


        /// <summary>
        /// 除以
        /// </summary>
        /// <param name="value">被除数</param>
        /// <param name="num">除数</param>
        /// <returns></returns>
        public static decimal DividedBy(this short value, decimal num)
        {
            try
            {
                return value / (decimal)num;
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// 除以
        /// </summary>
        /// <param name="value">被除数</param>
        /// <param name="num">除数</param>
        /// <returns></returns>
        public static decimal DividedBy(this short? value, short num)
        {
            try
            {
                return Convert.ToDecimal(value) / num;
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        ///除以(如果异常,则返回0)
        /// </summary>
        /// <param name="value">被除数</param>
        /// <param name="num">除数</param>
        /// <returns></returns>
        public static decimal DividedByOrDefault(this short? value, short num)
        {
            try
            {
                return Convert.ToDecimal(value) / num;
            }
            catch { return 0; }
        }

        /// <summary>
        /// 除以(如果异常,则返回0)
        /// </summary>
        /// <param name="value">被除数</param>
        /// <param name="num">除数</param>
        /// <returns></returns>
        public static decimal DividedByOrDefault(this short? value, decimal? num)
        {
            try
            {
                return Convert.ToDecimal(value) / num.ToDecimal();
            }
            catch { return 0; }
        }

        /// <summary>
        /// 除以(如果异常,则返回0)
        /// </summary>
        /// <param name="value">被除数</param>
        /// <param name="num">除数</param>
        /// <returns></returns>
        public static decimal DividedByOrDefault(this short value, decimal? num)
        {
            try
            {
                return value / num.ToDecimal();
            }
            catch { return 0; }
        }
    }
}
