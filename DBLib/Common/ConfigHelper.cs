﻿/*
 * 项目地址:http://git.oschina.net/ggshihai/DBLib
 * Author:DeepBlue
 * QQ群:257018781
 * Email:xshai@163.com
 * 说明:一些常用的操作类库.
 * 额外说明:东拼西凑的东西,没什么技术含量,爱用不用,用了你不吃亏,用了你不上当,不用你也取不了媳妇...
 * -------------------------------------------------- 
 * -----------我是长长的美丽的善良的分割线-----------
 * -------------------------------------------------- 
 * 我曾以为无惧时光荏苒 如今明白谁都逃不过似水流年
 * --------------------------------------------------  
 */
using System;
using System.Configuration;
using System.Web.Configuration;

namespace DBLib.Common
{
    /// <summary>
    /// web.config操作类 
    /// </summary>
    public sealed class ConfigHelper
    {
        /// <summary>
        /// 得到ConnectionStrings中的配置字符串信息
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConnectionString(string key)
        {
            string objModel = "";

            try
            {
                objModel = ConfigurationManager.ConnectionStrings[key].ConnectionString;
            }
            catch
            { }
            return objModel ?? string.Empty;
        }

        /// <summary>
        /// 得到AppSettings中的配置字符串信息
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConfigString(string key)
        {
            string objModel = "";

            try
            {
                objModel = ConfigurationManager.AppSettings[key];
            }
            catch
            { }
            return objModel ?? string.Empty;
        }

        /// <summary>
        /// 得到AppSettings中的配置Bool信息
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool GetConfigBool(string key)
        {
            bool result = false;
            string cfgVal = GetConfigString(key);
            if (null != cfgVal && string.Empty != cfgVal)
            {
                try
                {
                    result = bool.Parse(cfgVal);
                }
                catch (FormatException)
                {
                    // Ignore format exceptions.
                }
            }
            return result;
        }

        /// <summary>
        /// 得到AppSettings中的配置Decimal信息
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static decimal GetConfigDecimal(string key)
        {
            decimal result = 0;
            string cfgVal = GetConfigString(key);
            if (null != cfgVal && string.Empty != cfgVal)
            {
                try
                {
                    result = decimal.Parse(cfgVal);
                }
                catch (FormatException)
                {
                    // Ignore format exceptions.
                }
            }

            return result;
        }

        /// <summary>
        /// 得到AppSettings中的配置int信息
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int GetConfigInt(string key)
        {
            int result = 0;
            string cfgVal = GetConfigString(key);
            if (null != cfgVal && string.Empty != cfgVal)
            {
                try
                {
                    result = int.Parse(cfgVal);
                }
                catch (FormatException)
                {
                    // Ignore format exceptions.
                }
            }

            return result;
        }

        /// 配置文件加密解密
        /// 加密方式 两个方式不同点?
        /// 
        public enum EncryptType
        {
            DataProtectionConfigurationProvider,
            RSAProtectedConfigurationProvider
        }

        ///// 
        ///// 以DPAPI方式加密Config
        ///// 
        //public void EncryptWebConfigByDPAPI(string sectionName = "connectionStrings")
        //{
        //    Configuration configuration = null;
        //    ConfigurationSection section = null;
        //    //打开Request所在路径网站的Web.config文件
        //    //configuration = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
        //    configuration = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
        //    //取得Web.config中connectionStrings设置区块
        //    section = configuration.GetSection(sectionName);
        //    //未加密时
        //    if (!section.SectionInformation.IsProtected)
        //    {
        //        section.SectionInformation.ProtectSection(EncryptType.DataProtectionConfigurationProvider.ToString());
        //        configuration.Save();
        //    }
        //}
        ///// 
        ///// 解密DPAPI
        ///// 
        //public void DecryptWebConfigByDPAPI(string sectionName = "connectionStrings")
        //{
        //    Configuration configuration = null;
        //    ConfigurationSection section = null;
        //    //打开Request所在路径网站的Web.config文件
        //    //configuration = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
        //    configuration = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
        //    //取得Web.config中connectionStrings设置区块
        //    section = configuration.GetSection(sectionName);
        //    if (section != null && section.SectionInformation.IsProtected)
        //    {
        //        section.SectionInformation.UnprotectSection();
        //        configuration.Save();
        //    }
        //}

        /// 
        /// 以RSA方式加密Config
        /// 
        public void EncryptWebConfig(EncryptType type, string sectionName = "connectionStrings")
        {
            Configuration configuration = null;
            ConfigurationSection section = null;
            //打开Request所在路径网站的Web.config文件 
            //System.Web.HttpContext.Current.Request.ApplicationPath==/EQuality 虚拟路径
            //configuration = WebConfigurationManager.OpenWebConfiguration(System.Web.HttpContext.Current . Request.ApplicationPath);
            configuration = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            //取得Web.config中connectionStrings设置区块
            section = configuration.GetSection(sectionName);
            //未加密时
            if (section != null && !section.SectionInformation.IsProtected)
            {
                section.SectionInformation.ProtectSection(type.ToString());
                configuration.Save();
            }
        }

        /// 
        /// 解密Rsa或DPAPI是用同一种方法
        /// 
        public void DecryptWebConfig(string sectionName = "connectionStrings")
        {
            Configuration configuration = null;
            ConfigurationSection section = null;
            //打开Request所在路径网站的Web.config文件
            //configuration = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
            configuration = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);

            //取得Web.config中connectionStrings设置区块
            section = configuration.GetSection(sectionName);
            if (section != null && section.SectionInformation.IsProtected)
            {
                section.SectionInformation.UnprotectSection();
                configuration.Save();
            }
        }

        /// 
        /// 以RSA方式加密Config
        /// 
        public void EncryptAppConfig(string exePath, EncryptType type, string sectionName = "connectionStrings")
        {
            Configuration configuration = null;
            ConfigurationSection section = null;
            //exePath = System.Reflection.Assembly.GetExecutingAssembly().ManifestModule.FullyQualifiedName;
            configuration = ConfigurationManager.OpenExeConfiguration(exePath);
            //取得.config中connectionStrings设置区块
            section = configuration.GetSection(sectionName);
            //未加密时
            if (section != null && !section.SectionInformation.IsProtected)
            {
                section.SectionInformation.ProtectSection(type.ToString());
                configuration.Save();
            }
        }

        /// 
        /// 解密Rsa或DPAPI是用同一种方法
        /// 
        public void DecryptAppConfig(string exePath, string sectionName = "connectionStrings")
        {
            Configuration configuration = null;
            ConfigurationSection section = null;
            configuration = ConfigurationManager.OpenExeConfiguration(exePath);

            //config中connectionStrings设置区块
            section = configuration.GetSection(sectionName);
            if (section != null && section.SectionInformation.IsProtected)
            {
                section.SectionInformation.UnprotectSection();
                configuration.Save();
            }
        }
    }
}
