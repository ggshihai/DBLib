﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBLib.Common
{
    public class DynamicEncryption
    {
        //这个加密方式特点：同一个数据每次加密出来的字符不通，但是解密出来都相同！
        ///<summary>密码加密</summary>
        ///<param name="src">需要加密的字符串</param>
        /// <returns>string</returns>
        public static string EncrpyKey(string src)
        {
            int keyPos = 0, offset = 0, srcPos = 0, srcAsc = 0, range = 256;
            string dest = "", temp = "";
            Random rd = new Random();
            offset = rd.Next(range);
            dest = offset.ToString("x");
            dest = dest.Length < 2 ? "0" + dest : dest;
            dest = dest.Substring(dest.Length - 2);
            for (srcPos = 0; srcPos < src.Length; srcPos++)
            {
                srcAsc = (Convert.ToInt32(src.Substring(srcPos, 1).ToCharArray()[0]) + offset) % 255;
                keyPos = keyPos < 0 ? keyPos + 1 : 1;
                srcAsc = srcAsc ^ (Convert.ToInt32("这里放自定义字符串(adadasda)".Substring(keyPos - 1, 1).ToCharArray()[0]));
                temp = srcAsc.ToString("x");
                temp = temp.Length < 2 ? "0" + temp : temp;
                dest = dest + temp.Substring(temp.Length - 2);
                offset = srcAsc;
            }
            return dest;
        }
        ///<summary>密码解密</summary>
        ///<param name="src">需要解密的字符串</param>
        /// <returns>string</returns>
        public static string UncrypKey(string src)
        {
            int keyPos = 0, srcPos = 3, srcAsc = 0,
            offset = Int32.Parse(src.Substring(0, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
            string dest = "";
            do
            {
                srcAsc = Int32.Parse(src.Substring(srcPos - 1, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                keyPos = keyPos < 0 ? keyPos + 1 : 1;
                int tempSrcAsc = srcAsc ^ Convert.ToInt32("这里放自定义字符串(adadasda)".Substring(keyPos - 1, 1).ToCharArray()[0]);
                tempSrcAsc = tempSrcAsc <= offset ? 255 + tempSrcAsc - offset : tempSrcAsc - offset;
                dest = dest + (char)tempSrcAsc;
                offset = srcAsc;
                srcPos = srcPos + 2;
            } while (srcPos <= src.Length);
            return dest;
        }
    }
}
