﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.IO;

namespace DBLib.Windows
{
    /// <summary>
    /// 自定义控件:半透明控件
    /// </summary>
    /* 
     * [ToolboxBitmap(typeof(LoadingLayer))]
     * 用于指定当把你做好的自定义控件添加到工具栏时，工具栏显示的图标。
     * 正确写法应该是
     * [ToolboxBitmap(typeof(XXXXControl),"xxx.bmp")]
     * 其中XXXXControl是你的自定义控件，"xxx.bmp"是你要用的图标名称。
    */
    [ToolboxBitmap(typeof(LoadingLayer))]
    public class LoadingLayer : System.Windows.Forms.Control
    {
        private bool _transparentBG = true;//是否使用透明
        private int _alpha = 125;//设置透明度

        private System.ComponentModel.Container components = new System.ComponentModel.Container();

        public LoadingLayer()
            : this(125, true)
        {
        }

        private static Image _loading;

        public static Image GetLoadingImg()
        {
            if (_loading == null)
            {
                //var base64 = "R0lGODlhMAAwAOZnAL7L5Ka10+Xv+rHP8BdEeAssSjJrq3er5FaT1ouhu4WVpLvV8arJ6pi11cXQ3cva6tTk9N3q+MLK0bC+2qy617vI4qq51qm41a272LPB3LfE32WQwFFymZnA6oCu4Ehgd7rH4bXD3vr7/OHm8Oru9sXR56i31bnD17XC3a682LzJ4q692a+92au51rbD3vn6/LK/2rvH4bjF373J47nG4LnG4fr7/bTC3eLn8bC+2LLA27G/27zJ46i31LK/27jF4NPc7au41OHl7cHN5Pj5/Ky51MXQ5u7x+L/L483V5vf5/MLK2fv8/d3k8c7V5NPX3/b3+u3w9q261sfR6NXY4Ont9r7I3MLM4ejt9cTQ5tng7fz9/tPb7NPc7Pf4++3x9+Xp8cjS6Ky51efq8cvf9Zi/6////////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFCgBnACwAAAAAMAAwAAAH/4BmZkdAWQCHiImKi4yNh0ZdUYJmTSVVRJOZmpucnZpKWCVaZl9hNp6oqao2U2NcJKqxspokSUNMs7mxW0gzur+pMzzAxJw8KsUSCgrFZioVxQXSzMQV0LEJHA6Z0gXUgmRksxUx2ATn24Ld32RlZQOyMSCxDucEHJPrk+7usiA1sjjYS6fPzAB+4mLVoCGr3jl8ZvQJ4AdPFo0fnho0yCTwXIKI0wzyEzDrh4xOGwwY2PBgkj0CIL21c1fRjICDNTfJ0NBJpU+WZhI81DSzDEmR/Dxp4MnpgU+fGxOk03RTXNGknTS4QJXy6UZUE/nRPMrJRYhUD7oaiCV2ANlOIf9QxGpg4GuqmQlTobjRrFNeVTcybPKAoLBhBAxmCQjHONzbSRkEazp8OLGsq2I5ZdAxmHJhy7EwY9WkwwcnCAxSq4YwS7RRTj52xIrQIUIsAY9R7ZiQivaBAx1i4czNaQJvTwt+K7cNVuzfThNYdIqgXHkH3KhEE5/EYkWn6sBtH3wOrozNg209rUjxfbmgsGXILA5nhmL5zJ1SYJi+YEEm9EYVhdRr9+WkCQb7hWafgPAZmAoGFMiCn4ADbscJBRGqUlRCFMJnniwUtMDWaBSasaEsLVigIIEmjlafOxZqYoGKwJRok3yzWHABMWE5mMsFO/JIni4XmNBXXyb0cGRAMz1IIcKSwIgQxBU4QPkLDieAAcMLVs7yAgxPmOFEDiN40SUqUIyQwxKTCGGFGAHEKeecdNZp551xFnECFYIEAgAh+QQFCgBnACwHAAcAGgAaAAAHx4BngoOEhQ4JCYWKi4UEjomMig0bD4SOBJCCCgqMDQaflYKXmQoFBR+LD58GG4Ojg6amjBuroa9nH7GcqautZ68SsaiDDAyEtJ8Nv4+4sRKDHggIHhCDqwbLmKWmw4LS39RnnqyF2wXPgxDf38YNoYUSH7uF0evGkfgQ9Qj4/YIMCO75G0jwTIcDCBMeWFBQkEKFDBsefLiwoaAICzJqjGBxkYABAjoO+limzACRZ8iUXBmyoYCVK09ahGmy5UyWKM8IIEMmZyAAIfkEBQoAZwAsBwAHAB8AEwAAB8WAZ4KDhIUPDQ2FiouMggaPiY2SDB4QhI8GkYIJCZKDDAihlo6QgwkEBByeZxChCB6DmJqoqKtnHq6jsoIctJ2rraGwZ7sOtKqMCwuEuKEMxKW9qA6MHQcHHRGDrgjQmaeoyGcSHwUfgtfp2Wegr4XgBNRn5QX1ghHp6csMo4UOHJ0U1BtYgJC1fMskSSBY74OEQhEOHrBF0GGjBQcSehJYQIGtj2c8FhpQpqTJMmRALjp5MqXKkSxLunxJSACZmzgF0CQUCAAh+QQFCgBnACwJAAcAHwATAAAHxIBngoOEgxAMDIWKi4yCCI+JjZILHRGEjwiRgg0NkoMLB6GWjpCDDQYGG55nEaEHHYOYmqioq2cdrqOyghu0nautobBnuw+0qoxkZIS4oQvEpb2oD4wDZWUDAoOuB9CZp6jIZw4cBByC1+nZZ6CvheAG1GflBPWCAunpywujhQ8bnRLUG0iAkLV8yyQ5IFiPg4NCAg6WsUXQYSMyZRJ6Ekggga2PZzyC/ChBgcmTCiSMVKSggMuXLlcWagnzpUxCNGtKCAQAIfkEBQoAZwAsDgAHABoAGgAAB8eAZ4KDghELC4SJiotnB46IjIpkAwKEjgeQggwMjGRln5WCl5kMCAgeiwKfZQODo4OmpowDq6GvZx6xnKmrrY2PZxCxqJG0n2S/mLixEJGCq2XJC6WmxGcPGwYbiZ6sidQIzWfZBuWSoYkQHpwN5e4GzosP7+UbD/GL7/b4jO0GDfycAQzozEGCgwgTOCAoKAGBhxAfMjzjMCLEiRUtLiQoQcLERR8KfPD4UZCEAigLKCh5RkHKlCQnSgj58gNLly9ZCsJp80wgACH5BAUKAGcALBYABwASACAAAAe9gAJkZGeFhoeIZYqEiI2FimWMhQsLjmeQkgsHBx2OmIabm56LhR2hlY2fEaGdo5FnppsRlpiam61nEB4IHo22B7NnuwjEjhEdlQzEywiWuczEHhDOZ8zS1IXKCAzYh9zdhw8N4+QND84NBurr6ujs7O7vBueODg7ghhwEHPfdDgQACSToliBgwH6NFBQ440CfQQ6NCkj8UKigwUYfJBaQYMgiREQSNFIEl1Eix24hNeJTKFEBPo0nUSqIiSgQACH5BAUKAGcALBUACQATACAAAAfKgGeCg4NkZISIiWdkZWUDipCNjZCJA5KHlIMCko+Zg5aNAp6CjI2dZxEdBx2KpWWiZ6oHs5ACA4cLs7oHmRG7sx0RnrvBo2e5BwvGg8rLgxAM0dIMEJkMCNjZ2Nba2tzdCNWQDw/OghsGG+XGDwbuBg3GDe/v64oJBGcP6PQbghIKAp4hQJCDoHn0BCkowPAMB4IEHAxC6O/MwoYOIBqEdLGAoIcEJSrqKCgjRI4MPQrCRzDByJSEIIpERHKQgwQzEUlg+MEcQHOBAAAh+QQFCgBnACwOAA8AGgAaAAAHyoBngoOEhYUCA2UDhoyNiWWQjZKCZJCWZZOMApeQAwKZjJeeoI2VZWSkk6ipkxELr7ALEayCCwe3uLe0Z7a5uLu9vrOsEBC7jB4IHsbHghAI0AgMjRISkwzR0cyEHwUKjA0GZxDJ2R6EEgXqCg4J7mcG8RuC2Nnc6gUSCQT8ZxvxBh4MqndOkAJ8H87s6/cA4DxJ+AoIWkhA0L94AhkdXDeRX8UzDQE2ijiI4qBw8RpoVGetY79BADOCMjnoQQOZoBzw49CMUDtagQAAIfkEBQoAZwAsCQAWAB8AEwAAB8OAZ4KDhIWGZwJkiotkAoePh2Rlk5STkJeDkpWUmJiam46EEh8SnWcREZijBQUfph0HHamHCqy2pZcRB7sHC4UStrYfDg6XC7y8s2fBraUcBAmHDAinsMgdgsG4DgTdCQ8N4WcI5B6Cx8iCEgoKhM/dDg0G82ce5AgQg+jYhwndBBzOyKMH4Z45U2f+ERA00IAge+TydfLnjeE8h2cK3jOlcFDDQdPIMZgIz+NFQvckIjRJbxAEBipXCnowb4PMTuBkBgIAIfkEBQoAZwAsBwAWAB8AEwAAB8OAHwWDhAUKZ4iJiouMZ4WFh42SjIKPhpOYihIKnJ0SmaCMDhwOoQICmaMEBByhA2UDqI0Jq7WlmAJlumVkiw61tRwPD5hku7uyZ8CspRsGDY0LB2cCr8cDiMC3DwbdDRAM4WcH5B2IxseIDgkJis7dDwwI82cd5AcRiejYjQ3dBhvOyKMX4Z65UGf+GUA0EAEie+TygfLnjeE8h2cK3gulMFHDRNLILZgIz+NFRfckIjRJL1GEBSpXIoIwz4NMUOAQBgIAIfkEBQoAZwAsBwAOABoAGwAAB8iAZ4IKCoKGh4iJhh8FBR8SipGJjZSPkpcSlJSFl5KMmpydihKfBaKdCgWhp6yiHASwsQQJrYKysrS1r7eztYIOCcHCDr6KDxsPxYbHBgYbymcNzdPJtQ/T0xsQELXYzskeCAyKZGWJ2NUQCOsMEQvvZ2XyA4gPDQ2H4esQCwf+ZwPklRFwicE6BB7O9PsnQCA9SQcRCFp4QFBAeQQVGWQ30V/FMw0FRopoiKKhcvLIaNxX0uMhgRlFmTQkgExMURH8dYBmyJ2kQAAh+QQFCgBnACwHAAkAEgAgAAAHwoBngoOCCh8ShImJCgWNiIqKEo0FH5CQH5OPloOSjZWQCQmEmI0KihwEBBwOg5MFiqmxq2eMlIoOsbGiCpqnuQSim4IOqKnCiQnAx8uDGwbP0AYNm9HR05bO1dLCDw3e3w/MgxAeEMzkCAgeywzp7uaWEO7uHhERlvPq5h0HC5Dz8CIcGLhAAJmDgiAwYECI38AIZMpItLRg4IEOZyJOhGTxgCCNZSBVJPhRYkhFHQeBFPlQpUlxK5kJkDhA3BmDZwIBADs=";

                //var base64 = "R0lGODlhZABcAIe9APAAAADwAPB+MfB9fXzwfPC5k/C5uQCQ8LnwufDYxkCr8PCGQNjm8PBgAHzD8Nnw2fDwRETw8ADw8J3P8PBBQfDw3EHwQfDwf7jb8PCbYfCtgWC38PDwAMri8Ovu8CGe8PBvGYHw8J/w8PAiIiHwIfDayiTw8PDwIp3wnfDwZGPw8PBiYmHwYfDwoPCSUtzp8PDDpfCenurw6vDRvVKy8BKX8PDa2rzw8PCmdPDwzY7K8K3W8OHr8PBnDHO/8Mvw8PB3JfDj2fARERLwEvCOjo3wjcrwyvDwU1Tw8BPw8PBRUVHwUfDwj/DwFPAxMTHwMTTw8PDwM/CXWvDw7fDKyvC/nUqv8MHe8PChbPCzijOm8JTw8LDw8PDe0bDwsPDwdHTw8PBzc3Pwc/DwsPCOTKfU8PDq5fDOtfC0tBuc8OXw6vDk4wuV8PDwu4nI8Gy98PDt7PB1Ie3w8Fu28JfO8PBtFNXv7/ALCwrwCvCFPPCHh4fwh/C9mfDExMTwxPBmCubj5+Pw4/BMTErwSvAtLSvwK6fwp/Bra2nwafCoqPAaGhzwHPCWlpfwl9Tw1PBcXFzwXPA7Oz3wPfDT09Pl8PDWwzup8PDwS0zw8Avw8PDwiPDwC4fw8Kjw8Czw8PDwK/Dwa2vw8PDwp8Pw8PDw1fDwXFrw8B3w8PDwl/DwGzvw8PDwPfDwxPDJrbfc8LbwtvB7e3nwefCfafCxhiqi8Nrw8PCqebba8HnC8PB7LPC3kPDw4uTw8PDw8Jnw8Lbw8PDwe3nw8PDwtvDe3sXh8NTw8PAGBgXwBfCBNvCBgYHwgfC8lvC9vb3wvUWt8PCKRPBkB4LF8N7w3qDR8PBHR0XwRb3c8PCdZGS68Ceh8PBxHfAmJibwJvDczKLwovBkZGTwZPCUVPDGqfCiou7w7la08BWZ8PCodpHM8LLZ8OXs8PBpD3fC8PB5KPDl3fAVFRbwFvCRkZLwks3wzfBWVlXwVfA0NDXwNfCZXfDOzvDCoUyx8PCkbvC1jfDh1QAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQBFAD/ACwAAAAAZABcAAAI/wB7CRxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlxJ0APLlwq5wJxJs6ZGODZszsw3AgCFNTpZ0gNAVE/QlUqIAjB6NGUfRQCcDGuqEk4+OFSzat3KtavXr1XNYAX7sYuUOg1AZPBHluOZdQ3ixq1TqW1GMyDk6m1nxu7FfnoDV/FrMVxgvRkIVyRzWG5ixRNxNI47C/LEbtAa9whieaKGxro6UyyANq42faIrwjmj78zY1LBjy55Nu+SUMaAgrDqiiVTthayicBhOfBOwKb8PjtlEvPnwS8iTD8zRxLn1FNIHQrDOvQ1NM0B7mf/pO2UXchmByPXixWtiDu7cS82kcscYowLQ/pQoxeFELxIBQPJDJhL4IhET8FmXSnQsMUNUMp81MMN2m/SCRwDV3CCBBCFI9EWCzm2yC0wOAgBhXBNysMkUF2a4YYcRgQKicyO+1EdRs8RVySUqkjNEAIOMsiEnEmkyI3FNMLjSFH0wAwccMyTQyy7C+ObIK4H08sMNLkXEypHPfSWHRcIdKUp2vbTBHIiXoCkQgglGUYGbAolSnXVHzEmnQLtoskoqTXxSijB7DqReoQUFIs8gJAxBwiDyZLknOUX8GMClmC6CAp0yQILpp58icqh0YoBq6qUEZIfAMaeaeowf0lX/0+qp9ST3AKuzgoqHGr95k+upCGxkhgsl6LTHr6ZuqpEZz0hp07HIftrIb4ZE+6kXv91qbQB4SFqbBdsuEVstXHRyQ3u9eIHrr6+mJoIqG26YCSa/9AJOtLGIVgsm8fa7YShq1PMrC6MqxgsU/iZsigx7WArqEEUUrJgpCVe8RS8P7CHJEHjAU00R0sDUl0ZCVpxwEu2pR86hEqPURyR3KEJERiqYXLGBOq3xjlIAxMAQDxhMcwsPB3lic8JIBEUEzwBEopA6b7BxwNRsYEO0oQQe3S8UQenBtBMJvaDF1GRPnQ0DA/Gitb9c69SHMTyHkZAVZddtyZgCJbF2vJgcjKUH3D69VhAGdRdexkD87s1hUzaM04dCbxRe9xwDdaJ4JsX4RbfkZGthKMJrm0LY5pwfYAlBP+h9tAno2hV56QdsUNAvqlcMReaEXQH7ATsYZIcpWcebRDCtEzZH6VYkVIsvoagAhgjFK6aOApJrcfVCeKfmQTRpkG0OO10i2osHHWDQQfjip68+YQEBACH5BAEUAP8ALAsACQBUAFIAAAj/AHsJHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzpk2RzRbJu6kRRQBwPAmuoUjOj4ygSJMqXcq0qdOnUKNKnUq1qtWZU1qk+MJqylWCuyBwGMuBCUYzQ3uZMdNryi6vMgKR68WLF8kLZMnmsEjljjFGBaD9KVGKw4leJAJA+pFJgq+MTEDtNRgl71hNFpkBAJBMQ4MGM8Ru6oUnQLUbEiSEwChqbJSDnyxzwFxRM2fPoEVPKX069eqLLcYeNghKdhuLfTbrmfW50iUOm8gNCTBoVGpOGHelgDDmYIUTeVNc/5zShxkcODMS9NoljFQvR68C9fpxwwPJCl9WXUJ1Us7X/wAGKOCABBa4kBkFuLAAGbP4A6A+dXwm4R/neGWVLhJm+Bk+NvkRCySxzNNQAtBoqGEVNIlxTAAsHqMMQ1KYqGEucMgkD4s4soiCQnBEKGOG6sFEziI54viEQv78qKE+MTlSJI7HyIdQF0pmyCRMTj7JojQJwbFOlZ+VEJMMRD5ZyEIugAlEjTHtoeWOClVSopIFzEQOIjkeI0ZDzP3ogoVt9UEELMkkMsxKzSAyiBh+PFTAlxlCgwObcBAxwmaYCvGNDRwV04kIv9jFkTuzkCHAAjh0M9AwkWDq6maKoP+R0S+qNJZaJkj8QNIa9rzq6x2yWhRMasQSm4kIIx3i67KKpDVRCMVGK0EmXISUjzHLLjtAQTygY0U25mTjjA4vGMSYtNGeYt9HYWS77DYETZDGAfTWe4A5dMw1kAroSvvYR7266+skvciBi70I04uNfwKZ0G+0mHw0hSIC+yqrGwln7IBAvDwcrScfwfFOxa8m0gEbGWd8RS+12OqxBCaARAjJrvYxR8oZ70OOB0m8nFoEIK1A82ZCAFIDzgmzUS4mPqsGEhpDA7ACMUhnnE4vXPicSS0htUqyMfnsUHXCExSMxMu/gUSFECTr0YvYY9tbNl2qPKzCuiEZMLK7YQhk1EHc9royEC+huJxaElvoO9Ik1Cw7QgwDeWAO4AewwUNBtWyhAhKhiCDqSX2EQYETkayQCJsDYUN5OVFRcvTYbBAjFR1xu0GVGyjjvHFVO2ST8QdlfOVBGRtYQoslc0zzeUkBAQAh+QQBFAD/ACwGAAkAUgBSAAAI/wB7CRxIsKDBgwgP8grBK6HDhxAjSnRYLMmoiRgzSuygDqMcjSBDEtyR5gCbNyJTqnTIgM2Blwd0rJxJ0w3Ml1po6kxp82bOnUAzUnIJM1rQoxLllDH3EttHpFAfquvwIqrVq1izan0YaN6DrVgNSToW4Bi3IjLAHiXHIoDbtwEsBFILlADcu9Xo6nyA5+5dL3pn7vF7t17glfUIwy10WGVixW5JNE4ZC7JbC5NFNrMcoEhmkYMgk1DzOeQDboSHNCstMhALsm8tGGGt8gGKPY1m097Nu3fGXcJaiGI1xXdGVpc2cVjOIdWXXcYjalLOvPoJVtEdXqjOfXkT7NkNjv+h3r36J+jhB06JUr68JpA28ull1b78J435jAFg1MuMmV5T7FKcDIGQ0wsvDe2kSX3lVZARMwAAkEwB0PRQQikcnNALCQFA8kMmEviyEygMdgfeRBBKqEEDDcwAAQeb9NJXNTdIIEEII5bIXRsPRpjMii2+uMkUM9Z4407b6cicgxilmMwsLFZyCYzkDBHAIKPYyMlObSi53AnFYdRHhHrAMUMCvQBHSi+OvDLXDzd4sNMUJ3j5BUhoJALHQ08FhYqSTTCZ3kAYljjGoAVNkUJ9m4iCqEFTtFAnd5fk8ChCU7TBxBfAtLBmmJcmZAYfLsRRhzZkZOFOqAWJEweLsLL/uI4GoF5qCzSx5tqAC/9d2o+uwGZwaRc9AAusOI9eYyywyCAKBwjLAlvCoAlEC2wBg7Zira7nsOaIQ+Jsm6strM3jUCXixoptembUkS6L3SCawbsCPNrFH+nqc2kW4oYj0DCMrCCIEgP0ER0/1pJhBhywCBHhw8YIYoNxy0Craw+2wGEGNQ93HOE7Bvu2hi5kgNDDOsho4I9AK3jssiITG2cgHLUa4PLNK4SqxM0u37GGTnJeBccdPLucyEx2RJCJKUFDNUnRLidzEDkMWENJnw6pYKMEW1j1NNQdw2LQLVrARMsOEGGyNY5RwaEf2BHyR1AZN8FEx0O/gHiKHVdFeQJ3hPIN9EINdb/EBiUP1cJFglYx8jcFBelQOEwOlGaGE2DfwUxB2Ez+Eg2sUaEI1PEY1LnnoLM2id8uvxPDQXR4foBRtMGRCDXvGCOEPXrEbBAPTBXORlW9wUEzRBgQBRMbZbBqEDHOwKQABs4j5AElHVWv/fasBgQAIfkEARQA/wAsBgADAE0AVQAACP8AewkcSLCgwYMIC8q5ESJUiF8eEkqcSLGixRueJGjUaIKLxY8gQxbslGmjyUy+RKqsyGPamznYdFBC+KOkyZOjVuo0qM5HjQNAgzojZtDUzaMRdirtRYlW0KdA2dAhyCvJ0ZuZai1d+eID1K8Hygz8cfXoja0qaYD9au6FwFFlb3pEC5LYWrA+BNaKa/IHQiqHBMEaRnfgm7tf00TsBYWvhFMIE90BQFkRlcK9tCD+2kGgL8ecDpp5R7k0hcIe0myGukOgHCRxI8g5iKZ0aWNr6Howt/pp615yPBg9iokXQgO2KeMu7LQ30M4D5XBBcipTEkydZiM0IyR5JMzYnB//UGxQTnCLMYyVFnK58BXxb7b2WSEozKSdRvbUszBIDAJyvai1Wg0MYKbTPNUcE8CCDN7zCgOqbTaVgSoZggeDGC54jDLEeLUWG+hQqBICF2ZoYhEvYMPGV5ZYI2JIU6hBgok0HmNELy/QgQ0N5ThwxYsq7UHjkEsAuRU5hQxJIx6BGKnUAwoqaaIXADq5EgJS0liElRZVoU0rBGGZZYZ7VMmlROc00A9BjkQ55oIonEkRHJVMQZAMi7zJ4DxyqoSIngHcY2afH81T4piGEKqSMm/Wo6hK5PwppQVNPqqSPEPUiIgMluokjTL3DHEMHk+IcWOnSsUoDaeorrTLGEyg/8LKFHa2qhIppWzCwa4cpKJJrbZ+1EYqvBbLAQS7bFVCEFbm0ISxxl6y0hTJHDKMONCsw6yREEALLSoq9UGZHho00MAMrIAiioiseAvtJyoxQ1ky5Z7b7Sa9yCDNbB4Yt9QF7kJLikjyAkCvuTPc20ue9dQkwRZLlRKwscIQPG+9CXOA74XV3KBRCBFPXOwYFhuM8b3kcOyxBCAr9YXIvOZQ8sH2atzLhYPAJUFoSgkDc6/xXoywwvN4IU0vP9yw2E5TfALzBUGbjLAoEADzYhu6BhxFsiKZQQ8F+fDRADRdODlFC1lDe8LASsEBwxlnthGFsZuUwnWwIE3RxgWggI7CBNt4By744IQXbvjhSnWhAT44VII4Rbr8Ya65sgD7eEGtTK65BpcftIDmk9dhRucFQQP65I6TPpDppzeQuuq9INP6OqPD3kvmp9ti+0CzsG4uPnDsPlAC55BxDdzCJ6/88sw373ywVMCSiPOEUMYM83CQBsD0zMewzQrBPy/++CHxIL465Kev/vrshxQQACH5BAEUAP8ALAcAAQBTAFIAAAj/AHsJHEiwoMGDCAn+4KQCiQpOPxJKnEixosWCPzBlksCxY4Qfci6KHEmSIJckHVNyzCSipMuXCX9tVKkyUyeYOF/K4XWKps8ktXIKHRnMp1EVBifFO/ToEKNhQy92oBNNhzUPBeX0NEozCa+BVAQZA0CW7J1Da6JKvKLggFu3H3SEFPiDq9FRAhPdKcuX7Agqag/SYfO28IF9WHtxseuzJZqxffsqmhSY4A7DmDeEXMxYpa81IyKLpjClci8PHzBjTtfrRmeVXJKJno3G9DTVmK304jXzdaZi22aLfmR6A27DbDzIwfSaoyobwkWPMG3puGFKvX41l9CJWfTId8xU/65u/S32XqFemyLX53vf8JXLlXebXKAHJIwjfIXuvqwi0xPMd4AzA8nhARi9qRRKYr0E1x8A9JimThrz7ZDVDyqYsFEmp6iAF0EDPAhAbaZdZh0NCe1UCy9zFTSMIv1RAIdpArlBmGrOqCNRiwglAplw7+RD40DpaGGYOdHw6FIMe82mCDMDmQEDFvjgIM6MQ8lBjA4+OLCDjkNRQUFkxjxiw0AwxNHAmmvG0YpFgXgTizwPDJnQFFQkQ48SK8RjQ2kCZcHmoA1AwwdFhsATwKJ47GEnSTNAQ+igfyQgkR94LKppAPI8KtICkxIajkSDbKrpIjJ4WpE7kobK5h/iIf90jKmaGqEqRWe4SqilstK6aDO3TtSKroNWktATvuIRSLASdUHsmtC4k1AjviLC7ERAPIsMoAeRg4ipFix7bUIFPAsDRV4s8cQgjaQ67p1k6CoFt+8KBYcUoWKBZb1RTXFGBkCsA0QGxvJr8MEIJ6zwwgw3nFMgfrjrsEuspNBCL/cEIMbEL13CwSa9ZFqNHJyAERROrHyRAipT0GuaGWl1YcssZkDwccgBVOOaBCHghMomHAQdxS4GkQKKJi6XRMUdxjCiwZoz2AyyyDv3/FIFQAcdNCgG2cwBKjl5B0AyTzcQ9c1Uc2S1S6horfUJBkURNBNhk0U21FJPkTbPMLWU7TYHqRjUBgSlEI1TewDoUfbZU+dcNUw5/M3BEXZO0QczcMxAhhTuSI2zzmrj9IXbqZAyrtRyiMyLKZhEhFMLl6wCiunjjhHFBb2QEAAkHOc0TyPi9i788MQXb/zxyCev/PLMN+/8wWq4FKvwReCBQkn79r5HANc/jxA50ngv/vjkl2/++einr/767Lfv/vvwxz98QAAh+QQFFAD/ACwNAAEAUgBSAAAI/wB7CRxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzamToS9UpKFs2iqwIRoJJkyrkMFxjY83IkaNOyuSScE0yQsYA3IkUA87LjMFkngyFsM8IAEiTAog07OfFUEJNmjqYT4jSq4RcOqXoK6oETgcpXB0bZitFDyaEnuJl0MDYsXe0mu0lZ8eLhsWgnPT0Q2XBMG/HJpor0MGBbA1V3hBxw69BQYGv6iHcy8eBDxshR06ajLKHMpQ2At6MdBzll25J32l6WiQce6QPtX7Zx2rgrAO7FJCFDwufILMpGlD01skkgf7CQWvAnHkPKcCDR1wT5igAY06ImBHYqk7z78zrnP+RLhGOjXysBZ5ZDh78ukrkL67R1r5+Lp/xKWqoz79Afort8FefAP9J5A97An4HjVwFNnRGgvXBlxA5DRLUCoTtSVghQwlgCJ47GzYEh3ceNhBHiA5lUGIDOKDYUDcIJtiDPy42lIWH/tXI0BSyJAiNLVPo6FABJIKnDQxCQmRGAWTEUUccZCyzXZITTTHFGF8ccUkpTJBC5UOinMDBmGRukkIFXy70BZlsknlCDmkidEGbdHKQipfxjUKTRG3UWeclQUr3QyYShBTRKn7WOcZIZmhlxnZT7BKkH4Y4coNJIUSUQ6J1liISFXcYw0gB0PRQQikcnNALHgEMcqkEmUL/1AKndKq6ETNIJbNfAzNAwMEmqwZQzauxPjQnrWwCe2uuu/b6a7DDYhrRsciSKVIfSOkxC3OVXPIsq9HCGhEq1bo5Uh/MwAHHDAlEKoyXKMTSDLERsVLumJeQ94NJYEX0yb2ikCeHCJx4IJEo5UYRqHSORTQFqrRuwsrCcQo0xRGcNrFoxQVZecEmdUbBCscJVaAJBJ+cEEUKG5Ps8kUPGPHAywoFEgsJxwRwDAnKqEExzc0sEsDQRAfAjRE0DzTPEEU3DY8jSfdiQdNUD5J0M1RTfcw8NIuRNdV70LzE102DIzbZRbPQNdpEK0MzAmzr7EfS97BtddJ+MP3101H3ePIKPFkv4geFfT8gBuBDkyCGNH0XJMM8fjhCOMdyGNy4QbyokIQEp4Rg+eUeeCLUVJf3wolXN5QegVdgBIffSHpFRdTlUEUlQunFECqTJ5/r+AI6DDAkxw1pmaRKMV8qcIAWDnnwiy+pTy7kPgdYUrpB6pTBA3kBAQA7";
                var base64 = "R0lGODlhJwAnAOYAAP///9To+dHn+NPn+M/m+M3l+Mzj98ri98jj98bi98Tg98Hf9sPf9r/e9r3d9rra9bba9bHX9LTX9bPW9K/W9KzS86rR86jS86fR8qXP8qXP85/O8qHN8p7N8ZzK8ZjK8ZfJ8JrJ8ZXG8JPF8I/G8JHE8IvB7oW+7n++7n697Xy57Xq47Xm37HW47HC263O07G6163Kz62yx62qw62mv6maw6WGu6mOs6mCt6V2o6Fep6Fqn6VWo6Fmm6FKm51Cl51Ok6E6k502g5kqf50eh5kWg5kSb5UKa5UCZ5Tma5DiZ4z2X5DWY5DuW5DST4y6U4zKS4ymR4iiR4SuO4iCN4SSK4SKJ4R2J3B2G4B2F3hyE1RyC2RyD0xyB1xuC0ht/zBt90Bt8zht6yhl7xhp5wxl4whp4xxl1vBlzwBlyvhhwuxhwtBdxthhutxZrrRZqqxdprhVmpBVhov///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFCgBzACwAAAAAJwAnAAAH/4BzgoOEhYIQTF0khoyNjoU8aWpfBI+Wl3NEampkCpifjJqcDKClg6JkpKaEEUtMGI2oqoUKQ1orlkVhYVocoZuphgpXcMULjzu7YWC+hbK0xMVoDQGOCU5eXsvNp8Czc8PFcG0kCtXW2LvMhM+C4cXkDAKX19q83DybZqoJV2vwJOR9qhdGjBZYcyp4IVOD1IFo4wLOA0VQDJMBggZAYGBgjox/EQWaIogEAaFzKQCKXIUAB44IJg0NsNEExMpVcwIYOHDOkM4DGHEKffQAiJGjRpAgRXLiU4gqXaJK1dLliok5Qaho3cqVCkJLZeK8iUO2LFk4C3pUqWJl7Voqbv8zBH0U1qxdOBIa1DiChG9fvkECznWEYQqYw4gPV0lBKsABBZAjQ05QCdMABJIlAx3KmdEBFCs8MQrQwcRXnAiCMGFCoychDD165JAgNDUTJUp2xJxTYFCH2LJpm7KNG0mGBHMk9AjSQVCBGcBnl0qNWwkSDQwwqiAyJEfHOQagx5aOibh17BNZcNches4B8bInXKJeHP0gFUOGAPl2AEZ04Y60UJ1xN22n3zfuwVdDZY3gUFwGN82B3xA8IJggcA64VsgDOOxwQYQS5rcfIwigMMMHDGhYyAAKJDDYfdxV2EgAmPU2lIEydvYIjhbqSAgK+fXQo4+CMIADEB20R6QITAowwOAngQAAIfkEBQoAcwAsAAAAACYAJgAAB/+Ac4KDhIWDSFSGiouMhmJmXY2Sk4JmZmCUmYuPmJqeg5yfikaNYmSdooJAWFeJm5eSao01rK2vqKlzS1isroWhuYe8tr+wwcJXxKBmWseESLW+c1a4znPQyUjWjdhB240z3+LjwThB5+jnQiTfPVDv8PFTmnBzcHBubvf4gvH+75ryCRyobw6MdOl6fDNBrqHDRjRIHdtx5AijFXHiwAGRi+IRIjcW0ZCTcaMoj0S8DaqRg9AXkho5akI5pBAPHTUGMfiSMWYmlCoJ3SzUgGdJmY2AKrrZklBRmCYbfUy5iKmhoj3ZNJLxMaghHToWYc0oiUZIRlYVNXhSJuexoQ8QB8GNO2duXB083Mal0dRTIAAh+QQFCgBzACwAAAAAJgAmAAAH/4BzgoOEhYNASh6Gi4yNhC5UWFGOlJVzOJFVlpuLmFianKGCmFSgooQ9PY2kpoY7U5Q4TLMWnVSljFBiYmGOLUqzTLWFnlaLTLtiWpQ8s05Mhp6tglBhu2CWzcHDo5mFTNZi2Jvaz9wwkZOD4Nei2lDQ61Ut6+Hj7sBMPIwpXu2ngngAw8GIxD+AglbQcyQEocOHEAfJuHEDR0WLOAhyelKlo8eOUubMIEKypEkiESx5UcOypZo5atikiXGy5iYvbHLq3PlSRcafP2d8EHWlqNErTyIqXbqJhAmmjGbw0MGiERw4bRxK1aHjRSM3brACnMFVR45BG8rAoUEIrNhQZFC5nl0XJw6bQmHfWopr9ltdOIbyZq3Edy4hJn8XCaZkoqzhw4kVX0XjiIVcRojtWr1K6YVXzJGhQo4DWPTo0qYFZUadGkndu6kFQfjCZoWoQAAh+QQFCgBzACwAAAAAJgAlAAAH/4BzgoOEhYM4RIaKi4yFK0xQSI2TlHMwkEyVmoqXUEmboIOdn6GEDzQ3jKOMNUskjTFEsoudmYpGVFhYjSiyR4mGq4ZGulhVk4hEvxSFtcO5WFYhlMnLhM6EuFhX0prVwIKPntnQ3ZvfhERKKIPa0dOh1amL5fClyTOMVNz2pYLsjFr08EewoEFFLGAotJQPxowOm5ZAmUhxYiYWPDLq0JGxow9NWMSIHEkyzJyNKFNuBGmmpcuXYuacmEGz5gwYL0AhkcKzJ09XB4MKDXVkKKMvcuQcM1oIaZw4XRqxmUPGn9M4cF4tujJnaqmrcEwUcoMm6Jc4crCKHdvm4NmnWSkVuWlbEMlTrFoNzTVYBW5evQcxXLkCotFepnLpIma7eOzhxoLckA0VCAAh+QQFCgBzACwAAAAAJgAmAAAH/4BzgoOEhYMvO4aKi4yFJkREQI2TlHMqkEGVmooqQ0SZm6GCnZ+ihi8vD4ukoIotPB+NKjo6PauerYU9SkxKjSY9tLaGrIq7vEiTMMG1xLiGx0xJlcu1w4PFhNHTmtU6OYQnnpKD26LeOaqCOUEm5Uq83OfML4sk8NKmg8s9LIse8OTpm2PChDpFJ2AMXMiw4aAqWiJKlEhDk4UgQiBpNGJESIgqcUKKHBknRCUmVapYsUKFCsuWVkCSJAmikpOWOHO6hNAEjM+fPrO42ARkidGjRnM5XMq00Y8fjVIkaujGDSMTYsJsYVgVDqMXZrJu1de1DSEvZOoJkiImqxZTZVALsWHzhZCUsGHeboorlw2ZQmzdcqjkBg4cs4bm/gXcVoyWwY0KH16kWFFgMVgmGUasqLJlvA09KxJCpUVov00po06deDVrQnPrviZ0ZXGoQAAh+QQFCgBzACwAAAAAJgAmAAAH/4BzgoOEhYQ0homKi4Y6OjWMkZJzJ449k5iJlTo5mZ6Dm52fhBhXVxuKoYsoOJFVcXJwIJqOooY3RLmMSHGwsoaqhjS5REGRYL1xv4TBh8RDk8i9y4InPZyFw7nQmNLKs4O1ztuf3nCogjW2czRD5KPScl2KH+5E3KNz0lWL7/mCQ5BUWESCxb+DCBMqLISjh8OHDwW5mThxTkVPRJQw2ciRoxKLFENSxHSECRQnG1GeRLlQEI4gMGPCvNSyps1ENBnNSHhFEJNFWKhEwfCvpyB+iVBcwYKlCtFPRueQIXQFTIpBSpY2fYopaiEhZMxgIZSVqdOui8Ca0VKobNNJXjcNgRXDtq1WpJOmyg1bty1TLFHyKprb129QhGoLF+rhBAVivje/Qo48iDDlQTzMiL08SIriSYEAACH5BAUKAHMALAAAAAAmACYAAAf/gHOCg4SFg0xcJoaLjI2EPWtrYY6UlXNHkWiWm4uYa5qcoYKeoKKET0+NpI0jLBiOTWdnZYyriyo1uY4/srMbhraFuLkzlFW9Zb+EwYPDNTQUlceyycuZwrnP0ZbTvoM/kV+EztCh3bSDYWg0zdnlot1TjBru26bTTYwVue+mgj09HHUwEcGfwYMIDSGZwrBhwxWcWMSYSJGihSZjMmrcOGbTDYAgQwI8wrEkB0s3eKhcybJHAx5RYsqM2STFpgcraOjcqbNFhoRAg4aaUcORCBQOEDLp0iUIIw5HjgiZYHApUySMSCyJKqSgKKtdqJycE2GJExKDekSV6nUTWCodVwbVYLjk0VoibSm9jSuXoZJCaqMSsddob6G5U/4CvpvX0A6mYfkSQqx4cVSnjYQwhbuIMqPARygVKdLIMyMXQNAeNC3UEOvWk/3CNiSD7uxCDYooKSEqEAAh+QQJCgBzACwAAAAAJwAnAAAHmYBzgoOEhYNTYoaKi4yGP29vaI2TlIRvbJWZjZeanYWQmJ6GVVWTnItIXyuTUGtrpqGGWnFycpNErq+Mp4WztLGNubvAgrO1cCaZwoq8xbRyyJ3LhsDG0Mme04NsPYO+cdGic7lUizRxtOHigmtPiyvp2OtzQUKNPe7z+vv8/f7/AAMKHEiwoMGDCBMqXMiwocOHECNKnCgwEAA7";
                byte[] imageBytes = Convert.FromBase64String(base64);
                //读入MemoryStream对象
                MemoryStream memoryStream = new MemoryStream(imageBytes, 0, imageBytes.Length);
                memoryStream.Write(imageBytes, 0, imageBytes.Length);
                //转成图片
                _loading = Image.FromStream(memoryStream);
            }
            return _loading;
        }

        public LoadingLayer(int Alpha, bool IsShowLoadingImage)
        {
            SetStyle(System.Windows.Forms.ControlStyles.Opaque, true);
            base.CreateControl();

            this._alpha = Alpha;
            if (IsShowLoadingImage)
            {
                PictureBox pictureBox_Loading = new PictureBox();
                pictureBox_Loading.BackColor = System.Drawing.Color.White;
                pictureBox_Loading.Image = GetLoadingImg();
                pictureBox_Loading.Name = "pictureBox_Loading";
                pictureBox_Loading.Size = new System.Drawing.Size(48, 48);
                pictureBox_Loading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
                Point Location = new Point(this.Location.X + (this.Width - pictureBox_Loading.Width) / 2, this.Location.Y + (this.Height - pictureBox_Loading.Height) / 2);//居中
                pictureBox_Loading.Location = Location;
                pictureBox_Loading.Anchor = AnchorStyles.None;
                this.Controls.Add(pictureBox_Loading);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (!((components == null)))
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// 自定义绘制窗体
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            float vlblControlWidth;
            float vlblControlHeight;

            Pen labelBorderPen;
            SolidBrush labelBackColorBrush;

            if (_transparentBG)
            {
                Color drawColor = Color.FromArgb(this._alpha, this.BackColor);
                labelBorderPen = new Pen(drawColor, 0);
                labelBackColorBrush = new SolidBrush(drawColor);
            }
            else
            {
                labelBorderPen = new Pen(this.BackColor, 0);
                labelBackColorBrush = new SolidBrush(this.BackColor);
            }
            base.OnPaint(e);
            vlblControlWidth = this.Size.Width;
            vlblControlHeight = this.Size.Height;
            e.Graphics.DrawRectangle(labelBorderPen, 0, 0, vlblControlWidth, vlblControlHeight);
            e.Graphics.FillRectangle(labelBackColorBrush, 0, 0, vlblControlWidth, vlblControlHeight);
        }


        protected override CreateParams CreateParams//v1.10 
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x00000020; //0x20;  // 开启 WS_EX_TRANSPARENT,使控件支持透明
                return cp;
            }
        }

        /*
         * [Category("LoadingLayer"), Description("是否使用透明,默认为True")]
         * 一般用于说明你自定义控件的属性（Property）。
         * Category用于说明该属性属于哪个分类，Description自然就是该属性的含义解释。
         */
        [Category("LoadingLayer"), System.ComponentModel.Description("是否使用透明,默认为True")]
        public bool TransparentBG
        {
            get
            {
                return _transparentBG;
            }
            set
            {
                _transparentBG = value;
                this.Invalidate();
            }
        }

        [Category("LoadingLayer"), System.ComponentModel.Description("设置透明度")]
        public int Alpha
        {
            get
            {
                return _alpha;
            }
            set
            {
                _alpha = value;
                this.Invalidate();
            }
        }

    }

    /// <summary>
    /// DBLib.Windows.LoadingLayer 调用类
    /// </summary>
    public class LoadingLayerCMD
    {
        public Image Loading
        {
            get
            {
                return LoadingLayer.GetLoadingImg();
            }
        }

        private LoadingLayer m_OpaqueLayer = null;//半透明蒙板层

        /// <summary>
        /// 显示遮罩层
        /// </summary>
        /// <param name="control">控件</param>
        /// <param name="alpha">透明度</param>
        /// <param name="isShowLoadingImage">是否显示图标</param>
        public void ShowLoading(Control control, int alpha, bool isShowLoadingImage)
        {
            try
            {
                //if (this.m_OpaqueLayer == null)
                //{
                this.m_OpaqueLayer = new LoadingLayer(alpha, isShowLoadingImage);
                //}
                control.Controls.Add(this.m_OpaqueLayer);
                m_OpaqueLayer.BringToFront();
                this.m_OpaqueLayer.Dock = DockStyle.Fill;
                this.m_OpaqueLayer.BringToFront();
                this.m_OpaqueLayer.Enabled = true;
                this.m_OpaqueLayer.Visible = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 隐藏遮罩层
        /// </summary>
        public void HideLoading()
        {
            try
            {
                if (this.m_OpaqueLayer != null)
                {
                    this.m_OpaqueLayer.Visible = false;
                    this.m_OpaqueLayer.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
