﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Data;
using System.IO;

namespace DBLib.Office
{
    /// <summary>
    /// Aspose操作类
    /// </summary>
    public partial class AsposeHelper
    {
        /// <summary>
        /// Aspose.Cells常用操作封装
        /// </summary>
        public class Words
        {
            private static Words _instance;
            public static Words Instance
            {
                get
                {
                    if (_instance == null) _instance = new Words();
                    return _instance;
                }
            }

            public string SaveAsPdf(string wordFile, string savePdfPath = null)
            {
                Aspose.Words.Document doc = new Aspose.Words.Document(wordFile);
                if (savePdfPath == null) savePdfPath = System.IO.Path.ChangeExtension(wordFile, "pdf");
                doc.Save(savePdfPath, Aspose.Words.SaveFormat.Pdf);
                return savePdfPath;
            }
        }
    }
}
