﻿using Aspose.Cells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Data;
using System.IO;

namespace DBLib.Office
{
    /// <summary>
    /// Aspose操作类
    /// </summary>
    public partial class AsposeHelper
    {
        /// <summary>
        /// Aspose.Cells常用操作封装
        /// </summary>
        public class Excel
        {
            public static void RemoveWatermark(string filename)
            {
                try
                {
                    using (var epp = new DBLib.Office.EPPlus(filename))
                    {
                        epp.DeleteSheet("Evaluation Warning");
                        epp.Save();
                    }
                }
                catch (Exception ex) { }
            }
            public static void ExportExcelFromHtml()
            {
                //加载选项，html，否则会出错  
                //LoadOptions lo = new LoadOptions(LoadFormat.Html);
                //Workbook wb = new Workbook(stream, lo);
                ////输出到浏览器  
                //wb.Save(context.Response, "output.xls", ContentDisposition.Attachment, new XlsSaveOptions(SaveFormat.Excel97To2003));
            }

            /// <summary>
            /// 将Excel转成DataTable
            /// </summary>
            /// <param name="fileName"></param>
            /// <returns></returns>
            public static System.Data.DataTable ReadExcel(string fileName)
            {
                Workbook book = new Workbook(fileName);
                Worksheet sheet = book.Worksheets[0];
                Aspose.Cells.Cells cells = sheet.Cells;
                return cells.ExportDataTableAsString(0, 0, cells.MaxDataRow + 1, cells.MaxDataColumn + 1, true);
            }
            /// <summary>
            /// 将Excel转成DataTable
            /// </summary>
            /// <param name="fileName">文件名</param>
            /// <param name="sheetIndex">sheet索引</param>
            /// <param name="useFirstSheetIfNotExist">如果指定的不存在则用第一个sheet</param>
            /// <returns></returns>
            public static System.Data.DataTable ReadExcel(string fileName, int sheetIndex, bool useFirstSheetIfNotExist = true)
            {
                Workbook book = new Workbook(fileName);
                Worksheet sheet = null;
                try
                {
                    sheet = book.Worksheets[sheetIndex];
                }
                catch (Exception ex)
                {
                    if (useFirstSheetIfNotExist) sheet = book.Worksheets[0];
                    else throw ex;
                }
                Aspose.Cells.Cells cells = sheet.Cells;
                return cells.ExportDataTableAsString(0, 0, cells.MaxDataRow + 1, cells.MaxDataColumn + 1, true);
            }

            /// <summary>
            /// 将Excel转成DataTable
            /// </summary>
            /// <param name="fileName">文件名</param>
            /// <param name="sheetIndex">sheet名称</param>
            /// <param name="useFirstSheetIfNotExist">如果指定的不存在则用第一个sheet</param>
            public static System.Data.DataTable ReadExcel(string fileName, string sheetName, bool useFirstSheetIfNotExist = true)
            {
                Workbook book = new Workbook(fileName);
                Worksheet sheet = null;
                try
                {
                    sheet = book.Worksheets[sheetName];
                    if (sheet == null) throw new Exception("sheet名称不存在");
                }
                catch (Exception ex)
                {
                    if (useFirstSheetIfNotExist) sheet = book.Worksheets[0];
                    else throw ex;
                }
                Aspose.Cells.Cells cells = sheet.Cells;
                return cells.ExportDataTableAsString(0, 0, cells.MaxDataRow + 1, cells.MaxDataColumn + 1, true);
            }

            /// <summary>
            /// 初始化Workbook
            /// </summary>
            /// <param name="fileName"></param>
            /// <returns></returns>
            public static Workbook GetWorkbook(string fileName)
            {
                Workbook book = new Workbook(fileName);
                return book;
            }


            /// <summary>
            /// 将workbook从浏览器端下载
            /// </summary>
            /// <param name="workbook">Workbook</param>
            /// <param name="response">HttpResponse</param>
            /// <param name="filename">保存的文件名</param>
            public static void Download(Workbook workbook, System.Web.HttpResponse response, string filename = null)
            {
                if (string.IsNullOrEmpty(filename)) filename = DateTime.Now.ToString("yyyyMMdd_hhMMssfff") + ".xls";
                response.Clear();
                response.Buffer = true;
                response.Charset = "utf-8";
                response.AppendHeader("Content-Disposition", "attachment;filename=" + filename);
                response.ContentEncoding = System.Text.Encoding.UTF8;
                response.ContentType = "application/ms-excel";
                response.BinaryWrite(workbook.SaveToStream().ToArray());
                response.End();
            }

            /// <summary>
            /// 设置表页的列宽度自适应
            /// </summary>
            /// <param name="sheet">worksheet对象</param>
            static void setColumnWithAuto(Worksheet sheet)
            {
                Cells cells = sheet.Cells;
                int columnCount = cells.MaxColumn;  //获取表页的最大列数
                int rowCount = cells.MaxRow;        //获取表页的最大行数

                for (int col = 0; col < columnCount; col++)
                {
                    sheet.AutoFitColumn(col, 0, rowCount);
                }
                for (int col = 0; col < columnCount; col++)
                {
                    cells.SetColumnWidthPixel(col, cells.GetColumnWidthPixel(col) + 30);
                }
            }


            public static void Download<T>(IEnumerable<T> data, HttpResponse response, string filename = null,
                bool isAutoColumn = true, string[] columns = null, string sheetName = null)
            {
                Workbook workbook = new Workbook();
                Worksheet sheet = (Worksheet)workbook.Worksheets[0];
                if (!string.IsNullOrWhiteSpace(sheetName)) sheet.Name = sheetName;

                PropertyInfo[] ps = typeof(T).GetProperties();
                var colIndex = "A";
                foreach (var p in ps)
                {
                    if (isAutoColumn)
                        sheet.Cells[colIndex + 1].PutValue(p.Name);
                    int i = 2;
                    foreach (var d in data)
                    {
                        sheet.Cells[colIndex + i].PutValue(p.GetValue(d, null));
                        i++;
                    }

                    colIndex = ((char)(colIndex[0] + 1)).ToString();
                }

                if (columns != null)
                {
                    colIndex = "A";
                    foreach (var item in columns)
                    {
                        sheet.Cells[colIndex + 1].PutValue(item);
                        colIndex = ((char)(colIndex[0] + 1)).ToString();
                    }
                }

                if (string.IsNullOrEmpty(filename)) filename = DateTime.Now.ToString("yyyyMMdd_hhMMssfff") + ".xls";

                response.Clear();
                response.Buffer = true;
                response.Charset = "utf-8";
                response.AppendHeader("Content-Disposition", "attachment;filename=" + filename);
                response.ContentEncoding = System.Text.Encoding.UTF8;
                response.ContentType = "application/ms-excel";
                response.BinaryWrite(workbook.SaveToStream().ToArray());
                response.End();
            }

            public static bool DataTableToExcel(DataTable datatable, System.Web.HttpResponse response, out string error, List<string> listHeader = null, string sheetName = "Sheet1")
            {

                error = "";
                try
                {
                    string fileName = sheetName + ".xls";
                    if (datatable == null)
                    {
                        error = "DataTableToExcel:datatable 为空";
                        return false;
                    }

                    Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
                    Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];
                    sheet.Name = sheetName;
                    Aspose.Cells.Cells cells = sheet.Cells;

                    int nRow = 0;
                    if (listHeader == null)
                    {
                        for (int i = 0; i < datatable.Columns.Count; i++)
                        {
                            var name = datatable.Columns[i].ColumnName;
                            cells[nRow, i].PutValue(name);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < listHeader.Count; i++)
                        {
                            cells[nRow, i].PutValue(listHeader[i]);
                        }
                    }

                    foreach (DataRow row in datatable.Rows)
                    {
                        nRow++;
                        try
                        {
                            for (int i = 0; i < datatable.Columns.Count; i++)
                            {
                                if (row[i].GetType().ToString() == "System.Drawing.Bitmap")
                                {
                                    //------插入图片数据-------
                                    System.Drawing.Image image = (System.Drawing.Image)row[i];
                                    MemoryStream mstream = new MemoryStream();
                                    image.Save(mstream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    sheet.Pictures.Add(nRow, i, mstream);
                                }
                                else
                                {
                                    //var bl = RegexExt.IsMatch(row[i].ToString(), @"^[+-]?\d*[.]?\d*$");
                                    //if (bl)
                                    //{
                                    //    if (row[i].ToString() != "")
                                    //    {
                                    //        cells[nRow, i].PutValue(Convert.ToDouble(row[i].ToString()));
                                    //    }

                                    //}
                                    //else
                                    //{
                                    //    cells[nRow, i].PutValue(row[i].ToString());
                                    //} 
                                    cells[nRow, i].PutValue(row[i]);
                                }
                            }
                        }
                        catch (System.Exception e)
                        {
                            error = error + " DataTableToExcel: " + e.Message;
                        }
                    }

                    //自动列宽
                    int columnCount = cells.MaxColumn;  //获取表页的最大列数
                    int rowCount = cells.MaxRow;        //获取表页的最大行数

                    for (int col = 0; col < columnCount; col++)
                    {
                        sheet.AutoFitColumn(col, 0, rowCount);
                    }
                    for (int col = 0; col < columnCount; col++)
                    {
                        int pixel = cells.GetColumnWidthPixel(col) + 30;
                        if (pixel > 255)
                        {
                            cells.SetColumnWidthPixel(col, 255);
                        }
                        else
                        {
                            cells.SetColumnWidthPixel(col, pixel);
                        }
                    }

                    response.Clear();
                    response.Buffer = true;
                    response.Charset = "utf-8";
                    response.AppendHeader("Content-Disposition", "attachment;filename=" + fileName);
                    response.ContentEncoding = System.Text.Encoding.UTF8;
                    response.ContentType = "application/ms-excel";
                    response.BinaryWrite(workbook.SaveToStream().ToArray());
                    response.End();

                    //workbook.Save(filepath);
                    return true;
                }
                catch (System.Exception e)
                {
                    error = error + " DataTableToExcel: " + e.Message;
                    return false;
                }
            }
            public static bool DataTableToExcel(DataTable datatable, System.Web.HttpResponse response, bool isAutoHeader, out string error)
            {

                error = "";
                try
                {
                    string fileName = DateTime.Now.ToString("yyyyMMdd_hhMMssfff") + ".xls";
                    if (datatable == null)
                    {
                        error = "DataTableToExcel:datatable 为空";
                        return false;
                    }

                    Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
                    Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];
                    Aspose.Cells.Cells cells = sheet.Cells;

                    int nRow = 0;
                    for (int i = 0; i < datatable.Columns.Count; i++)
                    {
                        var name = datatable.Columns[i].ColumnName;
                        cells[nRow, i].PutValue(name);
                    }

                    foreach (DataRow row in datatable.Rows)
                    {
                        nRow++;
                        try
                        {
                            for (int i = 0; i < datatable.Columns.Count; i++)
                            {
                                if (row[i].GetType().ToString() == "System.Drawing.Bitmap")
                                {
                                    //------插入图片数据-------
                                    System.Drawing.Image image = (System.Drawing.Image)row[i];
                                    MemoryStream mstream = new MemoryStream();
                                    image.Save(mstream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    sheet.Pictures.Add(nRow, i, mstream);
                                }
                                else
                                {
                                    var bl = RegexExt.IsMatch(row[i].ToString(), @"^[+-]?\d*[.]?\d*$");
                                    if (bl)
                                    {
                                        if (row[i].ToString() != "")
                                        {
                                            cells[nRow, i].PutValue(Convert.ToDouble(row[i].ToString()));
                                        }

                                    }
                                    else
                                    {
                                        cells[nRow, i].PutValue(row[i].ToString());
                                    }
                                }
                            }
                        }
                        catch (System.Exception e)
                        {
                            error = error + " DataTableToExcel: " + e.Message;
                        }
                    }

                    //自动列宽
                    int columnCount = cells.MaxColumn;  //获取表页的最大列数
                    int rowCount = cells.MaxRow;        //获取表页的最大行数

                    for (int col = 0; col < columnCount; col++)
                    {
                        sheet.AutoFitColumn(col, 0, rowCount);
                    }
                    for (int col = 0; col < columnCount; col++)
                    {
                        cells.SetColumnWidthPixel(col, cells.GetColumnWidthPixel(col) + 30);
                    }

                    response.Clear();
                    response.Buffer = true;
                    response.Charset = "utf-8";
                    response.AppendHeader("Content-Disposition", "attachment;filename=" + fileName);
                    response.ContentEncoding = System.Text.Encoding.UTF8;
                    response.ContentType = "application/ms-excel";
                    response.BinaryWrite(workbook.SaveToStream().ToArray());
                    response.End();

                    //workbook.Save(filepath);
                    return true;
                }
                catch (System.Exception e)
                {
                    error = error + " DataTableToExcel: " + e.Message;
                    return false;
                }
            }

            public static bool DataTableToExcel(DataTable datatable, string fileSavePath, out string error)
            {

                error = "";
                try
                {
                    //string fileName = DateTime.Now.ToString("yyyyMMdd_hhMMssfff") + ".xls";
                    if (datatable == null)
                    {
                        error = "DataTableToExcel:datatable 为空";
                        return false;
                    }

                    Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
                    Aspose.Cells.Worksheet sheet = workbook.Worksheets[0];
                    Aspose.Cells.Cells cells = sheet.Cells;

                    int nRow = 0;
                    foreach (DataRow row in datatable.Rows)
                    {
                        try
                        {
                            for (int i = 0; i < datatable.Columns.Count; i++)
                            {
                                if (row[i].GetType().ToString() == "System.Drawing.Bitmap")
                                {
                                    //------插入图片数据-------
                                    System.Drawing.Image image = (System.Drawing.Image)row[i];
                                    MemoryStream mstream = new MemoryStream();
                                    image.Save(mstream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    sheet.Pictures.Add(nRow, i, mstream);
                                }
                                else
                                {
                                    var bl = RegexExt.IsMatch(row[i].ToString(), @"^[+-]?\d*[.]?\d*$");
                                    if (bl)
                                    {
                                        if (row[i].ToString() != "")
                                        {
                                            cells[nRow, i].PutValue(Convert.ToDouble(row[i].ToString()));
                                        }

                                    }
                                    else
                                    {
                                        cells[nRow, i].PutValue(row[i].ToString());
                                    }
                                }
                            }
                        }
                        catch (System.Exception e)
                        {
                            error = error + " DataTableToExcel: " + e.Message;
                        }
                        nRow++;
                    }

                    workbook.Save(fileSavePath);
                    return true;
                }
                catch (System.Exception e)
                {
                    error = error + " DataTableToExcel: " + e.Message;
                    return false;
                }
            }

            public static bool DataTableToExcels(List<DataTableSheeetNames> datatable, System.Web.HttpResponse response, out string error, List<string> listHeader = null, string sheetName = "Sheet1")
            {
                error = "";
                try
                {
                    string fileName = sheetName + ".xls";
                    if (datatable.Count() == 0)
                    {
                        error = "DataTableToExcel:datatable 为空";
                        return false;
                    }

                    Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
                    workbook.Worksheets.Clear();
                    for (int i = 0; i < datatable.Count(); i++)
                    {
                        workbook.Worksheets.Add(datatable[i].SheetName.ToString().Trim());
                    }
                    for (int k = 0; k < datatable.Count(); k++)
                    {
                        Aspose.Cells.Worksheet sheet = workbook.Worksheets[k];
                        Aspose.Cells.Cells cells = sheet.Cells;

                        int nRow = 0;
                        if (listHeader == null)
                        {
                            for (int i = 0; i < datatable[k].data.Columns.Count; i++)
                            {
                                var name = datatable[k].data.Columns[i].ColumnName;
                                cells[nRow, i].PutValue(name);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < listHeader.Count; i++)
                            {
                                cells[nRow, i].PutValue(listHeader[i]);
                            }
                        }

                        foreach (DataRow row in datatable[k].data.Rows)
                        {
                            nRow++;
                            try
                            {
                                for (int i = 0; i < datatable[k].data.Columns.Count; i++)
                                {
                                    if (row[i].GetType().ToString() == "System.Drawing.Bitmap")
                                    {
                                        //------插入图片数据-------
                                        System.Drawing.Image image = (System.Drawing.Image)row[i];
                                        MemoryStream mstream = new MemoryStream();
                                        image.Save(mstream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                        sheet.Pictures.Add(nRow, i, mstream);
                                    }
                                    else
                                    {
                                        var bl = RegexExt.IsMatch(row[i].ToString(), @"^[+-]?\d*[.]?\d*$");
                                        if (bl)
                                        {
                                            if (row[i].ToString() != "")
                                            {
                                                cells[nRow, i].PutValue(Convert.ToDouble(row[i].ToString()));
                                            }

                                        }
                                        else
                                        {
                                            cells[nRow, i].PutValue(row[i].ToString());
                                        }
                                    }
                                }
                            }
                            catch (System.Exception e)
                            {
                                error = error + " DataTableToExcel: " + e.Message;
                            }
                        }

                        //自动列宽
                        int columnCount = cells.MaxColumn;  //获取表页的最大列数
                        int rowCount = cells.MaxRow;        //获取表页的最大行数

                        for (int col = 0; col < columnCount; col++)
                        {
                            sheet.AutoFitColumn(col, 0, rowCount);
                        }
                        for (int col = 0; col < columnCount; col++)
                        {
                            cells.SetColumnWidthPixel(col, cells.GetColumnWidthPixel(col) + 30);
                        }
                    }
                    //response.Clear();
                    //response.Buffer = true;
                    //response.Charset = "utf-8";
                    //response.AppendHeader("Content-Disposition", "attachment;filename=" + fileName);
                    //response.ContentEncoding = System.Text.Encoding.UTF8;
                    //response.ContentType = "application/ms-excel";
                    //response.BinaryWrite(workbook.SaveToStream().ToArray());
                    //response.End();
                    workbook.Save(response, HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8), ContentDisposition.Attachment, new XlsSaveOptions(SaveFormat.Excel97To2003));
                    //workbook.Save("D:/2.xlsx");
                    return true;
                }
                catch (System.Exception e)
                {
                    error = error + " DataTableToExcel: " + e.Message;
                    return false;
                }
            }
        }


    }
    public class DataTableSheeetNames
    {
        public DataTable data { get; set; }
        public string SheetName { get; set; }
    }
}
