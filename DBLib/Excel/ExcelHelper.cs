﻿using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;
using NPOI.SS.Util;

namespace DBLib.Excel
{
    /// <summary>
    /// 创建人：胡田新
    /// 日 期：2020年7月28日 星期二
    /// 描 述：NPOI Excel DataTable操作类
    /// </summary>
    public class ExcelHelper
    {
        #region Excel导出方法 ExcelDownload
        /// <summary>
        /// Excel导出下载
        /// </summary>
        /// <param name="buffer">EXCEL文件流</param>
        /// <param name="excelConfig">导出设置包含文件名、标题、列设置</param>
        public static void ExcelDownload(byte[] buffer, ExcelConfig excelConfig)
        {
            HttpContext curContext = HttpContext.Current;
            // 设置编码和附件格式
            curContext.Response.ContentType = "application/ms-excel";
            curContext.Response.ContentEncoding = Encoding.UTF8;
            curContext.Response.Charset = "";
            curContext.Response.AppendHeader("Content-Disposition",
                "attachment;filename=" + HttpUtility.UrlEncode(excelConfig.FileName, Encoding.UTF8));
            //调用导出具体方法Export()
            curContext.Response.BinaryWrite(buffer);
            curContext.Response.End();
        }
        /// <summary>
        /// Excel导出下载
        /// </summary>
        /// <param name="dtSource">DataTable数据源</param>
        /// <param name="excelConfig">导出设置包含文件名、标题、列设置</param>
        public static void ExcelDownload(DataTable dtSource, ExcelConfig excelConfig)
        {
            HttpContext curContext = HttpContext.Current;
            // 设置编码和附件格式
            curContext.Response.ContentType = "application/ms-excel";
            curContext.Response.ContentEncoding = Encoding.UTF8;
            curContext.Response.Charset = "";
            curContext.Response.AppendHeader("Content-Disposition",
                "attachment;filename=" + HttpUtility.UrlEncode(excelConfig.FileName, Encoding.UTF8));
            //调用导出具体方法Export()
            curContext.Response.BinaryWrite(ExportMemoryStream(dtSource, excelConfig).GetBuffer());
            curContext.Response.End();
        }

        /// <summary>
        /// Excel导出下载
        /// </summary>
        /// <param name="dtSource">DataTable数据源</param>
        /// <param name="excelConfig">导出设置包含文件名、标题、列设置</param>
        public static void ExcelDownloadNew(DataTable dtSource, ExcelConfig excelConfig)
        {
            ExcelDownload(ExportMemoryStreamNew(dtSource, excelConfig).GetBuffer(), excelConfig);
        }

        /// <summary>
        /// Excel导出下载
        /// </summary>
        /// <param name="list">数据源</param>
        /// <param name="templdateName">模板文件名</param>
        /// <param name="newFileName">文件名</param>
        public static void ExcelDownload(List<TemplateDataModel> list, string templdateName, string newFileName)
        {
            HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.Charset = "UTF-8";
            response.ContentType = "application/vnd-excel";//"application/vnd.ms-excel";
            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=" + newFileName));
            System.Web.HttpContext.Current.Response.BinaryWrite(ExportListByTempale(list, templdateName).ToArray());
        }
        /// <summary>
        /// 动态增加行版本
        /// </summary>
        /// <param name="list">数据值</param>
        /// <param name="templdateName">模板路径</param>
        /// <param name="newFileName">下载文件名字</param>
        /// <param name="rowSourceindex">格式目标行</param>
        /// <param name="count">增加多少行</param>
        public static void ExcelDownload(List<TemplateDataModel> list, string templdateName, string newFileName, int rowSourceindex, int count)
        {
            HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.Charset = "UTF-8";
            response.ContentType = "application/vnd-excel";//"application/vnd.ms-excel";
            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=" + newFileName));
            System.Web.HttpContext.Current.Response.BinaryWrite(ExportListByTempale(list, templdateName, rowSourceindex, count).ToArray());
        }
        #endregion

        #region DataTable导出到Excel文件excelConfig中FileName设置为全路径
        /// <summary>
        /// DataTable导出到Excel文件 Export()
        /// </summary>
        /// <param name="dtSource">DataTable数据源</param>
        /// <param name="excelConfig">导出设置包含文件名、标题、列设置</param>
        public static void ExcelExport(DataTable dtSource, ExcelConfig excelConfig)
        {
            using (MemoryStream ms = ExportMemoryStream(dtSource, excelConfig))
            {
                using (FileStream fs = new FileStream(excelConfig.FileName, FileMode.Create, FileAccess.Write))
                {
                    byte[] data = ms.ToArray();
                    fs.Write(data, 0, data.Length);
                    fs.Flush();
                }
            }
        }
        #endregion

        #region DataTable导出到Excel的MemoryStream



        #region DataTable导出到Excel的MemoryStream
        /// <summary>
        /// DataTable导出到Excel的MemoryStream Export()
        /// </summary>
        /// <param name="dtSource">DataTable数据源</param>
        /// <param name="excelConfig">导出设置包含文件名、标题、列设置</param>
        public static MemoryStream ExportMemoryStream(DataTable dtSource, ExcelConfig excelConfig)
        {
            for (int i = 0; i < dtSource.Columns.Count;)
            {
                bool IsExists = false;
                DataColumn column = dtSource.Columns[i];
                for (int j = 0; j < excelConfig.ColumnEntity.Count; j++)
                {
                    if (excelConfig.ColumnEntity[j].Column == column.ColumnName)
                    {
                        IsExists = true;
                        break;
                    }
                }
                if (!IsExists)
                {
                    dtSource.Columns.Remove(column);
                }
                else
                {
                    i++;
                }
            }

            HSSFWorkbook workbook = new HSSFWorkbook();
            ISheet sheet = workbook.CreateSheet();
            #region 右击文件 属性信息
            {
                DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
                dsi.Company = "NPOI";
                workbook.DocumentSummaryInformation = dsi;

                SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
                si.Author = "刘晓雷"; //填加xls文件作者信息
                si.ApplicationName = "力软信息"; //填加xls文件创建程序信息
                si.LastAuthor = "刘晓雷"; //填加xls文件最后保存者信息
                si.Comments = "刘晓雷"; //填加xls文件作者信息
                si.Title = "标题信息"; //填加xls文件标题信息
                si.Subject = "主题信息";//填加文件主题信息
                si.CreateDateTime = System.DateTime.Now;
                workbook.SummaryInformation = si;
            }
            #endregion

            #region 设置标题样式
            ICellStyle headStyle = workbook.CreateCellStyle();
            int[] arrColWidth = new int[dtSource.Columns.Count];
            string[] arrColName = new string[dtSource.Columns.Count];//列名
            ICellStyle[] arryColumStyle = new ICellStyle[dtSource.Columns.Count];//样式表
            headStyle.Alignment = HorizontalAlignment.Center; // ------------------
            if (excelConfig.Background != new Color())
            {
                if (excelConfig.Background != new Color())
                {
                    headStyle.FillPattern = FillPattern.SolidForeground;
                    headStyle.FillForegroundColor = GetXLColour(workbook, excelConfig.Background);
                }
            }
            IFont font = workbook.CreateFont();
            font.FontHeightInPoints = excelConfig.TitlePoint;
            if (excelConfig.ForeColor != new Color())
            {
                font.Color = GetXLColour(workbook, excelConfig.ForeColor);
            }
            font.Boldweight = 700;
            headStyle.SetFont(font);
            #endregion

            #region 列头及样式
            ICellStyle cHeadStyle = workbook.CreateCellStyle();
            cHeadStyle.Alignment = HorizontalAlignment.Center; // ------------------
            IFont cfont = workbook.CreateFont();
            cfont.FontHeightInPoints = excelConfig.HeadPoint;
            cHeadStyle.SetFont(cfont);
            #endregion

            #region 设置内容单元格样式
            foreach (DataColumn item in dtSource.Columns)
            {
                ICellStyle columnStyle = workbook.CreateCellStyle();
                columnStyle.Alignment = HorizontalAlignment.Center;
                arrColWidth[item.Ordinal] = Encoding.GetEncoding(936).GetBytes(item.ColumnName.ToString()).Length;
                arrColName[item.Ordinal] = item.ColumnName.ToString();
                if (excelConfig.ColumnEntity != null)
                {
                    ColumnModel columnentity = excelConfig.ColumnEntity.Find(t => t.Column == item.ColumnName);
                    if (columnentity != null)
                    {
                        arrColName[item.Ordinal] = columnentity.ExcelColumn;
                        if (columnentity.Width != 0)
                        {
                            arrColWidth[item.Ordinal] = columnentity.Width;
                        }
                        if (columnentity.Background != new Color())
                        {
                            if (columnentity.Background != new Color())
                            {
                                columnStyle.FillPattern = FillPattern.SolidForeground;
                                columnStyle.FillForegroundColor = GetXLColour(workbook, columnentity.Background);
                            }
                        }
                        if (columnentity.Font != null || columnentity.Point != 0 || columnentity.ForeColor != new Color())
                        {
                            IFont columnFont = workbook.CreateFont();
                            columnFont.FontHeightInPoints = 10;
                            if (columnentity.Font != null)
                            {
                                columnFont.FontName = columnentity.Font;
                            }
                            if (columnentity.Point != 0)
                            {
                                columnFont.FontHeightInPoints = columnentity.Point;
                            }
                            if (columnentity.ForeColor != new Color())
                            {
                                columnFont.Color = GetXLColour(workbook, columnentity.ForeColor);
                            }
                            columnStyle.SetFont(font);
                        }
                        columnStyle.Alignment = getAlignment(columnentity.Alignment);
                    }
                }
                arryColumStyle[item.Ordinal] = columnStyle;
            }
            if (excelConfig.IsAllSizeColumn)
            {
                #region 根据列中最长列的长度取得列宽
                for (int i = 0; i < dtSource.Rows.Count; i++)
                {
                    for (int j = 0; j < dtSource.Columns.Count; j++)
                    {
                        if (arrColWidth[j] != 0)
                        {
                            int intTemp = Encoding.GetEncoding(936).GetBytes(dtSource.Rows[i][j].ToString()).Length;
                            if (intTemp > arrColWidth[j])
                            {
                                arrColWidth[j] = intTemp;
                            }
                        }

                    }
                }
                #endregion
            }
            #endregion

            #region 填充数据

            #endregion
            ICellStyle dateStyle = workbook.CreateCellStyle();
            IDataFormat format = workbook.CreateDataFormat();
            dateStyle.DataFormat = format.GetFormat("yyyy-MM-dd HH:mm:ss");
            int rowIndex = 0;
            foreach (DataRow row in dtSource.Rows)
            {
                #region 新建表，填充表头，填充列头，样式
                if (rowIndex == 65535 || rowIndex == 0)
                {
                    if (rowIndex != 0)
                    {
                        sheet = workbook.CreateSheet();
                    }

                    #region 表头及样式
                    {
                        if (excelConfig.Title != null)
                        {
                            IRow headerRow = sheet.CreateRow(0);
                            if (excelConfig.TitleHeight != 0)
                            {
                                headerRow.Height = (short)(excelConfig.TitleHeight * 20);
                            }
                            headerRow.HeightInPoints = 25;
                            headerRow.CreateCell(0).SetCellValue(excelConfig.Title);
                            headerRow.GetCell(0).CellStyle = headStyle;
                            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 0, dtSource.Columns.Count - 1)); // ------------------
                        }

                    }
                    #endregion
                    if (excelConfig.Title == null)
                    {
                        rowIndex = 0;
                    }
                    else {
                        rowIndex = 1;
                    }

                    #region 列头及样式
                    {
                        IRow headerRow = sheet.CreateRow(rowIndex);
                        #region 如果设置了列标题就按列标题定义列头，没定义直接按字段名输出
                        foreach (DataColumn column in dtSource.Columns)
                        {
                            headerRow.CreateCell(column.Ordinal).SetCellValue(arrColName[column.Ordinal]);
                            headerRow.GetCell(column.Ordinal).CellStyle = cHeadStyle;
                            //设置列宽
                            sheet.SetColumnWidth(column.Ordinal, (arrColWidth[column.Ordinal] + 1) * 256);
                        }
                        #endregion
                    }
                    #endregion
                    rowIndex++;
                }
                #endregion

                #region 填充内容
                IRow dataRow = sheet.CreateRow(rowIndex);
                foreach (DataColumn column in dtSource.Columns)
                {
                    ICell newCell = dataRow.CreateCell(column.Ordinal);
                    newCell.CellStyle = arryColumStyle[column.Ordinal];
                    string drValue = row[column].ToString();
                    SetCell(newCell, dateStyle, column.DataType, drValue);
                }
                #endregion
                rowIndex++;
            }
            using (MemoryStream ms = new MemoryStream())
            {
                workbook.Write(ms);
                ms.Flush();
                ms.Position = 0;
                return ms;
            }
        }
        #endregion

        /// <summary>
        /// DataTable导出到Excel的MemoryStream Export()
        /// </summary>
        /// <param name="dtSource">DataTable数据源</param>
        /// <param name="excelConfig">导出设置包含文件名、标题、列设置</param>
        public static MemoryStream ExportMemoryStreamNew(DataTable dtSource, ExcelConfig excelConfig)
        {
            if (excelConfig == null)
                excelConfig = new ExcelConfig();
            var model = excelConfig.ColumnEntity ?? new List<ColumnModel>();
            if (model.Count == 0)
            {
                foreach (DataColumn column in dtSource.Columns)
                {
                    model.Add(new ColumnModel { Column = column.ColumnName, ExcelColumn = column.ColumnName });
                }
            }
            HSSFWorkbook workbook = new HSSFWorkbook();
            ISheet sheet = workbook.CreateSheet();
            int rowIndex = 0;
            CreateTitle(workbook, sheet, excelConfig, excelConfig.ColumnEntity.Count - 1, ref rowIndex);
            CreateColHead(workbook, sheet, excelConfig, dtSource, ref rowIndex);
            FillData(workbook, sheet, excelConfig, dtSource, ref rowIndex);
            using (MemoryStream ms = new MemoryStream())
            {
                workbook.Write(ms);
                ms.Flush();
                ms.Position = 0;
                return ms;
            }
        }
        #endregion

        #region ListExcel导出(加载模板)
        /// <summary>
        /// List根据模板导出ExcelMemoryStream
        /// </summary>
        /// <param name="list"></param>
        /// <param name="templdateName"></param>
        public static MemoryStream ExportListByTempale(List<TemplateDataModel> list, string templdateName)
        {
            try
            {

                string templatePath = HttpContext.Current.Server.MapPath("/") + "/Resource/ExcelTemplate/";
                string templdateName1 = string.Format("{0}{1}", templatePath, templdateName);

                FileStream fileStream = new FileStream(templdateName1, FileMode.Open, FileAccess.Read);
                ISheet sheet = null;
                if (templdateName.IndexOf(".xlsx") == -1)//2003
                {
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(fileStream);
                    sheet = hssfworkbook.GetSheetAt(0);
                    SetPurchaseOrder(sheet, list);
                    sheet.ForceFormulaRecalculation = true;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        hssfworkbook.Write(ms);
                        ms.Flush();
                        return ms;
                    }
                }
                else//2007
                {
                    XSSFWorkbook xssfworkbook = new XSSFWorkbook(fileStream);
                    sheet = xssfworkbook.GetSheetAt(0);
                    SetPurchaseOrder(sheet, list);
                    sheet.ForceFormulaRecalculation = true;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        xssfworkbook.Write(ms);
                        ms.Flush();
                        return ms;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// 目标行
        /// </summary>
        /// <param name="list"></param>
        /// <param name="templdateName">模板路径加名字</param>
        /// <param name="rowSourceindex">目标行</param>
        /// <param name="count">增加多少行</param>
        /// <returns></returns>
        public static MemoryStream ExportListByTempale(List<TemplateDataModel> list, string templdateName, int rowSourceindex, int count)
        {
            try
            {

                string templatePath = HttpContext.Current.Server.MapPath("/");
                string templdateName1 = string.Format("{0}{1}", templatePath, templdateName);
                FileStream fileStream = new FileStream(templdateName1, FileMode.Open, FileAccess.Read);
                ISheet sheet = null;
                if (templdateName.IndexOf(".xlsx") == -1)//2003
                {
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(fileStream);
                    sheet = hssfworkbook.GetSheetAt(0);
                    var rowSource = sheet.GetRow(rowSourceindex);
                    MyInsertRowN(sheet, rowSourceindex + 1, count - 1, rowSource);
                    SetPurchaseOrder(sheet, list);
                    sheet.ForceFormulaRecalculation = true;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        hssfworkbook.Write(ms);
                        ms.Flush();
                        return ms;
                    }
                }
                else//2007
                {
                    XSSFWorkbook xssfworkbook = new XSSFWorkbook(fileStream);
                    sheet = xssfworkbook.GetSheetAt(0);
                    var rowSource = sheet.GetRow(rowSourceindex);
                    MyInsertRowN(sheet, rowSourceindex + 1, count - 1, rowSource);
                    SetPurchaseOrder(sheet, list);
                    sheet.ForceFormulaRecalculation = true;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        xssfworkbook.Write(ms);
                        ms.Flush();
                        return ms;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// 复制行
        /// </summary>
        /// <param name="sheet">当前表格页</param>
        /// <param name="aa">开始行</param>
        /// <param name="count">移动大小</param>
        /// <param name="bb">样式行</param>
        private static void MyInsertRowN(ISheet sheet, int aa, int count, IRow bb)
        {
            if (count == 0)
            {
                return;
            }
            #region 批量移动行
            sheet.ShiftRows(
                aa,                                    //--开始行
                sheet.LastRowNum,                      //--结束行
                count,                             //--移动大小(行数)--往下移动
                true,                                  //是否复制行高
                false//,                               //是否重置行高
            );
            #endregion
            List<CellRangeAddress> allRange = new List<CellRangeAddress>();
            for (int i = 0; i >= 0; i++)//获取目标行所有合并单元格对象 
            {
                try
                {
                    var region = bb.Sheet.GetMergedRegion(i);
                    if (region == null)
                    {
                        break;
                    }
                    if (region.FirstRow == aa - 1)//当前目标行的合并单元格
                    {
                        allRange.Add(region);
                    }
                }
                catch (Exception ex)
                {
                    break;
                }
            }
            for (int i = aa; i < aa + count; i++)
            {    /*对批量移动后空出的空行插，创建相应的行，
                 * 并以插入行的上一行为格式源(即：插入行-1的那一行)*/
                IRow targetRow = null;
                ICell sourceCell = null;
                ICell targetCell = null;
                targetRow = sheet.CreateRow(i);
                for (int m = bb.FirstCellNum; m < bb.LastCellNum; m++)
                {
                    sourceCell = bb.GetCell(m);
                    if (sourceCell == null)
                        continue;
                    targetCell = targetRow.CreateCell(m);
                    targetCell.CellStyle = sourceCell.CellStyle;
                    targetCell.SetCellType(sourceCell.CellType);
                    try
                    {
                        if (!string.IsNullOrEmpty(sourceCell.CellFormula))
                        {
                            targetCell.SetCellFormula(sourceCell.CellFormula);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                foreach (var item in allRange)//为每一行添加合并单元格
                {
                    var merged = new CellRangeAddress(i, i, item.FirstColumn, item.LastColumn);
                    sheet.AddMergedRegion(merged);
                }
                //CellRangeAddress四个参数为：起始行，结束行，起始列，结束列
                //sheet.AddMergedRegion(new CellRangeAddress(i, i, 3, 4));
            }
        }
        /// <summary>
        /// 赋值单元格
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="list"></param>
        private static void SetPurchaseOrder(ISheet sheet, List<TemplateDataModel> list)
        {
            try
            {
                foreach (var item in list)
                {
                    IRow row = null;
                    ICell cell = null;
                    row = sheet.GetRow(item.row);
                    if (row == null)
                    {
                        row = sheet.CreateRow(item.row);
                    }
                    cell = row.GetCell(item.cell);
                    if (cell == null)
                    {
                        cell = row.CreateCell(item.cell);
                    }
                    cell.SetCellValue(item.value);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region 设置表格内容
        private static void SetCell(ICell newCell, ICellStyle dateStyle, Type dataType, string drValue)
        {
            switch (dataType.ToString())
            {
                case "System.String"://字符串类型
                    newCell.SetCellValue(drValue);
                    break;
                case "System.DateTime"://日期类型
                    System.DateTime dateV;
                    if (System.DateTime.TryParse(drValue, out dateV))
                    {
                        newCell.SetCellValue(dateV);
                    }
                    else
                    {
                        newCell.SetCellValue("");
                    }
                    newCell.CellStyle = dateStyle;//格式化显示
                    break;
                case "System.TimeSpan"://时间类型
                    newCell.SetCellValue(drValue);
                    break;
                case "System.Boolean"://布尔型
                    bool boolV = false;
                    bool.TryParse(drValue, out boolV);
                    newCell.SetCellValue(boolV);
                    break;
                case "System.Int16"://整型
                    newCell.SetCellValue(drValue);
                    break;
                case "System.Int32":
                    newCell.SetCellValue(drValue);
                    break;
                case "System.Int64":
                    newCell.SetCellValue(drValue);
                    break;
                case "System.Byte":
                    int intV = 0;
                    int.TryParse(drValue, out intV);
                    newCell.SetCellValue(intV);
                    break;
                case "System.Decimal"://浮点型
                case "System.Double":
                    double doubV = 0;
                    double.TryParse(drValue, out doubV);
                    newCell.SetCellValue(doubV);
                    break;
                case "System.DBNull"://空值处理
                    newCell.SetCellValue("");
                    break;
                default:
                    newCell.SetCellValue("");
                    break;
            }
        }


        private static void SetCell(ICell newCell, Type dataType, string drValue)
        {
            switch (dataType.ToString())
            {
                case "System.DateTime"://日期类型
                    newCell.SetCellValue(DateTime.TryParse(drValue, out var dateV)
                        ? dateV.ToString("yyyy-MM-dd")
                        : "");
                    break;
                case "System.DBNull"://空值处理
                    newCell.SetCellValue("");
                    break;
                default:
                    if (double.TryParse(drValue, out double iValue))
                        newCell.SetCellValue(iValue);
                    else
                        newCell.SetCellValue(drValue);
                    break;
            }
        }
        #endregion

        #region 从Excel导入

        /// <summary>
        /// 数字转换时间格式 第一版
        /// </summary>
        /// <param name="timeStr">数字,如:42095.7069444444/0.650694444444444</param>
        /// <returns>日期/时间格式</returns>
        private string ToDateTimeValue1(string strNumber)
        {
            if (!string.IsNullOrWhiteSpace(strNumber))
            {
                System.Decimal tempValue;
                //先检查 是不是数字;
                if (System.Decimal.TryParse(strNumber, out tempValue))
                {
                    //天数,取整
                    int day = Convert.ToInt32(Math.Truncate(tempValue));
                    //这里也不知道为什么. 如果是小于32,则减1,否则减2
                    //日期从1900-01-01开始累加 
                    // day = day < 32 ? day - 1 : day - 2;
                    DateTime dt = new DateTime(1900, 1, 1).AddDays(day < 32 ? (day - 1) : (day - 2));

                    //小时:减掉天数,这个数字转换小时:(* 24) 
                    System.Decimal hourTemp = (tempValue - day) * 24;//获取小时数
                                                                     //取整.小时数
                    int hour = Convert.ToInt32(Math.Truncate(hourTemp));
                    //分钟:减掉小时,( * 60)
                    //这里舍入,否则取值会有1分钟误差.
                    System.Decimal minuteTemp = Math.Round((hourTemp - hour) * 60, 2);//获取分钟数
                    int minute = Convert.ToInt32(Math.Truncate(minuteTemp));
                    //秒:减掉分钟,( * 60)
                    //这里舍入,否则取值会有1秒误差.
                    System.Decimal secondTemp = Math.Round((minuteTemp - minute) * 60, 2);//获取秒数
                    int second = Convert.ToInt32(Math.Truncate(secondTemp));

                    //时间格式:00:00:00
                    string resultTimes = string.Format("{0}:{1}:{2}",
                            (hour < 10 ? ("0" + hour) : hour.ToString()),
                            (minute < 10 ? ("0" + minute) : minute.ToString()),
                            (second < 10 ? ("0" + second) : second.ToString()));

                    if (day > 0)
                        return string.Format("{0} {1}", dt.ToString("yyyy-MM-dd"), resultTimes);
                    else
                        return resultTimes;
                }
            }
            return string.Empty;
        }
        /// <summary>
        /// 数字转换时间格式
        /// </summary>
        /// <param name="timeStr">数字,如:42095.7069444444/0.650694444444444</param>
        /// <returns>日期/时间格式</returns>
        private static string ToDateTimeValue(string strNumber)
        {
            if (!string.IsNullOrWhiteSpace(strNumber))
            {
                System.Decimal tempValue;
                //先检查 是不是数字;
                if (System.Decimal.TryParse(strNumber, out tempValue))
                {
                    int day = 0;
                    int hour = 0;
                    int minute = 0;
                    int second = 0;
                    int totalSecond = 0;

                    //天数,取整，整数部分即为天数
                    day = Convert.ToInt32(Math.Truncate(tempValue));

                    //获取除去天数后剩下的秒数，四舍五入后取整
                    totalSecond = Convert.ToInt32(Math.Truncate(Math.Round((tempValue - day) * 24 * 60 * 60)));

                    //计算小时数,如果秒数大于一小时，说明存在小时数
                    if (totalSecond > 3600)
                        hour = totalSecond / 3600;
                    else
                        hour = 0;

                    //计算分钟数，减去得到的小时数之后，如果剩下的秒数大于一分钟，说明存在分钟数
                    if ((totalSecond - hour * 3600) > 59)
                        minute = (totalSecond - hour * 3600) / 60;
                    else
                        minute = 0;

                    //减去小时数、分钟数用到的秒数之后，余下的就是最终的秒数
                    second = totalSecond - hour * 3600 - minute * 60;

                    //将天数累计到小时数上
                    hour = hour + day * 24;

                    //时间格式:00:00:00
                    string resultTimes = string.Format("{0}:{1}:{2}",
                            (hour < 10 ? ("0" + hour) : hour.ToString()),
                            (minute < 10 ? ("0" + minute) : minute.ToString()),
                            (second < 10 ? ("0" + second) : second.ToString()));


                    return resultTimes;
                }
            }
            return string.Empty;
        }
        /// <summary>
        /// 获取单元格类型
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        private static object GetValueType(ICell cell)
        {
            if (cell == null)
                return null;
            switch (cell.CellType)
            {
                case CellType.Blank: //BLANK:  
                    return null;
                case CellType.Boolean: //BOOLEAN:  
                    return cell.BooleanCellValue;
                case CellType.Numeric: //NUMERIC:  
                    short format = cell.CellStyle.DataFormat;
                    if (format != 0)
                    {
                        //天数,取整，整数部分即为天数
                        var day = Convert.ToInt32(Math.Truncate(cell.NumericCellValue));
                        if (day > 0)
                        {
                            return cell.DateCellValue;
                        }
                        //return ToDateTimeValue(cell.NumericCellValue.ToString());
                        return cell.DateCellValue.ToString("HH:mm:ss");
                    }
                    else { return cell.NumericCellValue; }
                case CellType.String: //STRING: 
                    return cell.StringCellValue;
                case CellType.Error: //ERROR:  
                    return cell.ErrorCellValue;
                case CellType.Formula: //FORMULA:  
                default:
                    return "=" + cell.CellFormula;
            }
        }

        /// <summary>
        /// 返回读取execl_Sheet
        /// </summary>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        public static ISheet GetSheet(string strFileName)
        {
            ISheet sheet = null;
            using (FileStream file = new FileStream(strFileName, FileMode.Open, FileAccess.Read))
            {
                if (strFileName.IndexOf(".xlsx") == -1)//2003
                {
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(file);
                    sheet = hssfworkbook.GetSheetAt(0);
                }
                else//2007
                {
                    XSSFWorkbook xssfworkbook = new XSSFWorkbook(file);
                    sheet = xssfworkbook.GetSheetAt(0);
                }
            }
            return sheet;
        }
        /// <summary>
        /// 根据ISheet返回DataTable
        /// </summary>
        /// <param name="sheet">表格数据</param>
        /// <param name="dataRowindex">数据开始行 默认第1行是数据行</param>
        /// <param name="istitle">是否用表格标题 默认F开头的有序列</param>
        /// <param name="titleRowsIndex">标题行 默认1行是标题</param>
        /// <returns></returns>
        public static DataTable SheetToTable(ISheet sheet, int dataRowindex = 1, bool istitle = false, int titleRowsIndex = 0)
        {
            DataTable dt = new DataTable();
            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
            IRow headerRow = sheet.GetRow(titleRowsIndex);//读取标题行
            int cellCount = headerRow.LastCellNum;
            var index = dataRowindex;
            for (int j = 0; j < cellCount; j++)
            {
                ICell cell = headerRow.GetCell(j);
                if (istitle)
                    dt.Columns.Add(cell.ToString());
                else
                    dt.Columns.Add("F" + (j + 1));
            }
            try
            {
                for (int i = (sheet.FirstRowNum + index); i <= sheet.LastRowNum; i++)
                {
                    IRow row = sheet.GetRow(i);
                    DataRow dataRow = dt.NewRow();
                    for (int j = row.FirstCellNum; j < cellCount; j++)
                    {
                        if (row.GetCell(j) != null)
                        {
                            object obj = GetValueType(row.GetCell(j));
                            dataRow[j] = obj.TobjIsNull() ? string.Empty : obj.ToString();
                        }
                    }
                    dt.Rows.Add(dataRow);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        /// <summary>
        /// 读取excel ,默认第一行为标头
        /// </summary>
        /// <param name="strFileName">excel文档路径</param>
        /// <param name="istitleRows">第一行是否是标题行</param>
        /// <returns></returns>
        public static DataTable ExcelImport(string strFileName, bool istitleRows = false)
        {
            var sheet = GetSheet(strFileName);
            int dataRowindex = 1;
            if (istitleRows)
            {
                dataRowindex = 0;
            }
            DataTable dt = SheetToTable(sheet, dataRowindex);
            return dt;
        }
        /// <summary>
        /// 读取excel ,默认第一行为标头
        /// </summary>
        /// <param name="strFileName">excel文档路径</param>
        /// <param name="dataRowindex">数据开始行 默认第1行是数据行</param>
        /// <param name="istitle">是否用表格标题 默认F开头的有序列</param>
        /// <param name="titleRowsIndex">标题行 默认1行是标题</param>
        /// <returns></returns>
        public static DataTable ExcelImport(string strFileName, int dataRowindex = 1, bool istitle = false, int titleRowsIndex = 0)
        {
            var sheet = GetSheet(strFileName);
            DataTable dt = SheetToTable(sheet, dataRowindex, istitle, titleRowsIndex);
            return dt;
        }
        /// <summary>
        /// 读取excel ,默认第一行为标头 
        /// </summary>
        /// <param name="fileStream">文件数据流</param>
        /// <returns></returns>
        public static DataTable ExcelImport(Stream fileStream, string flieType)
        {
            DataTable dt = new DataTable();
            ISheet sheet = null;
            if (flieType == ".xls")
            {
                HSSFWorkbook hssfworkbook = new HSSFWorkbook(fileStream);
                sheet = hssfworkbook.GetSheetAt(0);
            }
            else
            {
                XSSFWorkbook xssfworkbook = new XSSFWorkbook(fileStream);
                sheet = xssfworkbook.GetSheetAt(0);
            }
            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
            IRow headerRow = sheet.GetRow(0);
            int cellCount = headerRow.LastCellNum;
            for (int j = 0; j < cellCount; j++)
            {
                ICell cell = headerRow.GetCell(j);
                dt.Columns.Add(cell.ToString());
            }
            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
            {
                IRow row = sheet.GetRow(i);
                DataRow dataRow = dt.NewRow();

                for (int j = row.FirstCellNum; j < cellCount; j++)
                {
                    if (row.GetCell(j) != null)
                        dataRow[j] = row.GetCell(j).ToString();
                }

                dt.Rows.Add(dataRow);
            }
            return dt;
        }
        #endregion

        #region RGB颜色转NPOI颜色
        private static short GetXLColour(HSSFWorkbook workbook, Color SystemColour)
        {
            short s = 0;
            HSSFPalette XlPalette = workbook.GetCustomPalette();
            NPOI.HSSF.Util.HSSFColor XlColour = XlPalette.FindColor(SystemColour.R, SystemColour.G, SystemColour.B);
            if (XlColour == null)
            {
                if (NPOI.HSSF.Record.PaletteRecord.STANDARD_PALETTE_SIZE < 255)
                {
                    XlColour = XlPalette.FindSimilarColor(SystemColour.R, SystemColour.G, SystemColour.B);
                    //2021.11.15注释,XlColour.Indexed未验证是否可行
                    //s = XlColour.GetIndex(); 
                    s = XlColour.Indexed;
                }

            }
            else
                //2021.11.15注释,XlColour.Indexed未验证是否可行
                //s = XlColour.GetIndex();
                s = XlColour.Indexed;
            return s;
        }
        #endregion

        #region 设置列的对齐方式
        /// <summary>
        /// 设置对齐方式
        /// </summary>
        /// <param name="style"></param>
        /// <returns></returns>
        private static HorizontalAlignment getAlignment(string style)
        {
            switch (style)
            {
                case "center":
                    return HorizontalAlignment.Center;
                case "left":
                    return HorizontalAlignment.Left;
                case "right":
                    return HorizontalAlignment.Right;
                case "fill":
                    return HorizontalAlignment.Fill;
                case "justify":
                    return HorizontalAlignment.Justify;
                case "centerselection":
                    return HorizontalAlignment.CenterSelection;
                case "distributed":
                    return HorizontalAlignment.Distributed;
            }
            return NPOI.SS.UserModel.HorizontalAlignment.General;


        }

        #endregion

        private static void CreateTitle(HSSFWorkbook workbook, ISheet sheet, ExcelConfig config, int mergerCol, ref int rowIndex)
        {
            if (config.Title == null) return;
            IRow row = sheet.CreateRow(rowIndex);
            rowIndex++;
            if (config.TitleHeight != 0)
                row.Height = (short)(config.TitleHeight * 20);
            row.HeightInPoints = 25;
            ICell cell = row.CreateCell(0);
            cell.SetCellValue(config.Title);
            ICellStyle style = workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;
            if (config.Background != new Color())
            {
                style.FillPattern = FillPattern.SolidForeground;
                style.FillForegroundColor = GetXLColour(workbook, config.Background);
            }
            IFont font = workbook.CreateFont();
            font.FontHeightInPoints = config.TitlePoint;
            if (config.ForeColor != new Color())
                font.Color = GetXLColour(workbook, config.ForeColor);
            font.Boldweight = 700;
            style.SetFont(font);
            cell.CellStyle = style;
            sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, mergerCol));
        }

        private static void CreateColHead(HSSFWorkbook workbook, ISheet sheet, ExcelConfig config, DataTable dt, ref int rowIndex)
        {
            int startRow = rowIndex;
            IRow[] iRows = new IRow[config.HeadMergerRow + 1];
            ICellStyle style = workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;
            IFont font = workbook.CreateFont();
            font.FontHeightInPoints = config.HeadPoint;
            style.SetFont(font);
            for (int i = 0; i <= config.HeadMergerRow; i++)
            {
                iRows[i] = sheet.CreateRow(rowIndex);
                rowIndex++;
            }
            for (int ordinal = 0; ordinal < config.ColumnEntity.Count; ordinal++)
            {
                var model = config.ColumnEntity[ordinal];
                for (int i = 0; i <= config.HeadMergerRow; i++)
                {
                    ICell cell = iRows[i].GetCell(ordinal) ?? iRows[i].CreateCell(ordinal);
                    if (cell.IsMergedCell) continue;
                    cell.CellStyle = style;
                    if (model.MergerCol > 0)
                    {
                        if (i == 0)
                        {
                            cell.SetCellValue(model.ParentColumn ?? model.ExcelColumn);
                            sheet.AddMergedRegion(new CellRangeAddress(startRow, startRow, ordinal,
                                ordinal + model.MergerCol));
                            continue;
                        }
                        cell.SetCellValue(model.ExcelColumn);
                        if (config.HeadMergerRow > 1)
                            sheet.AddMergedRegion(new CellRangeAddress(startRow + i, startRow + config.HeadMergerRow, ordinal, ordinal));
                    }
                    else
                    {
                        cell.SetCellValue(model.ExcelColumn);
                        sheet.AddMergedRegion(new CellRangeAddress(startRow, startRow + config.HeadMergerRow, ordinal, ordinal));
                    }
                    sheet.SetColumnWidth(ordinal, GetColumnWidth(model, config.IsAllSizeColumn, dt));
                }
            }
        }

        private static int GetColumnWidth(ColumnModel model, bool isAllSizeColumn = false, DataTable dt = null)
        {
            int width = model.Width;
            if (width == 0)
                width = Encoding.GetEncoding(936).GetBytes(model.ExcelColumn).Length;
            if (!isAllSizeColumn) return (width + 1) * 256;
            if (dt?.Rows == null) return (width + 1) * 256;
            foreach (DataRow row in dt.Rows)
            {
                int tempWidth = Encoding.GetEncoding(936).GetBytes(row[model.Column].ToString()).Length;
                if (tempWidth > width)
                    width = tempWidth;
            }
            return (width + 1) * 256;
        }

        private static void FillData(HSSFWorkbook workbook, ISheet sheet, ExcelConfig config, DataTable dt, ref int rowIndex)
        {
            ICellStyle style = workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;
            foreach (DataRow row in dt.Rows)
            {
                if (rowIndex == 65535)
                {
                    sheet = workbook.CreateSheet();
                    rowIndex = 0;
                    CreateTitle(workbook, sheet, config, config.ColumnEntity.Count - 1, ref rowIndex);
                    CreateColHead(workbook, sheet, config, dt, ref rowIndex);
                }
                IRow iRow = sheet.CreateRow(rowIndex);
                rowIndex++;
                int i = 0;
                foreach (ColumnModel model in config.ColumnEntity)
                {
                    ICell iCell = iRow.CreateCell(i);
                    if (model.Background != new Color())
                    {
                        style.FillPattern = FillPattern.SolidForeground;
                        style.FillForegroundColor = GetXLColour(workbook, model.Background);
                    }
                    IFont font = workbook.CreateFont();
                    font.FontHeightInPoints = 10;
                    if (model.Font != null)
                        font.FontName = model.Font;
                    if (model.Point != 0) font.FontHeightInPoints = model.Point;
                    if (model.ForeColor != new Color()) font.Color = GetXLColour(workbook, model.ForeColor);
                    style.SetFont(font);
                    if (!string.IsNullOrEmpty(model.Alignment)) style.Alignment = getAlignment(model.Alignment);
                    iCell.CellStyle = style;
                    DataColumn column = dt.Columns[model.Column];
                    SetCell(iCell, column.DataType, row[model.Column].ToString());
                    i++;
                }
            }
        }
    }
}
