﻿
namespace DBLib.Excel
{
    /// <summary>
    /// 创建人：胡田新
    /// 日 期：2020年7月28日 星期二
    /// 描 述：模板数据模型
    /// </summary>
    public class TemplateDataModel
    {
        /// <summary>
        /// 行号
        /// </summary>
        public int row { get; set; }
        /// <summary>
        /// 列号
        /// </summary>
        public int cell { get; set; }
        /// <summary>
        /// 数据值
        /// </summary>
        public string value { get; set; }
    }
}
