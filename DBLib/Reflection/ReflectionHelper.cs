﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DBLib.Reflection
{
    /// <summary>
    /// 反射帮助类
    /// </summary>
    public class ReflectionHelper
    {
        public static object GetValue(object obj, string field)
        {
            try
            {
                var objType = obj.GetType();
                PropertyInfo[] pros = objType.GetProperties();
                //var obj = Activator.CreateInstance(objType);
                foreach (PropertyInfo p in pros)
                {
                    if (p.Name != field) continue;
                    var objValue = p.GetValue(obj, null);
                    return objValue;
                }
            }
            catch (Exception ex)
            {
            }
            return string.Empty;
        }

        /// <summary>
        /// 判断两个对象是否相同
        /// </summary>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <param name="ignoreField"></param>
        /// <returns></returns>
        public static bool IsEquals(object obj1, object obj2, string ignoreField = "", bool isIgnoreNullOrEmpty = true)
        {
            try
            {
                if (isIgnoreNullOrEmpty)
                {
                    obj1 = TrimEmptyString(obj1);
                    obj2 = TrimEmptyString(obj2);
                }
                var objType1 = obj1.GetType();
                var objType2 = obj2.GetType();
                PropertyInfo[] pros1 = objType1.GetProperties();
                PropertyInfo[] pros2 = objType2.GetProperties();
                var arr = (ignoreField ?? string.Empty).ToLower().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                foreach (PropertyInfo p1 in pros1)
                {
                    if (arr.Contains(p1.Name.ToLower())) continue;
                    var v1 = p1.GetValue(obj1, null);
                    var p2 = pros2.FirstOrDefault(n => n.Name == p1.Name);
                    var v2 = p2.GetValue(obj2, null);
                    if (object.Equals(v1, v2) == false)
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 根据属性名称赋值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="propertyName">属性名称</param>
        /// <param name="newValue">值</param>
        /// <returns></returns>
        public static T SetValueByPropertyName<T>(T t, string propertyName, object newValue)
        {
            if (t == null) return default(T);
            var objType = t.GetType(); ;
            PropertyInfo[] pros = objType.GetProperties();
            foreach (PropertyInfo p in pros)
            {
                if (p.Name.EqualsX(propertyName) == false) continue;
                p.SetValue(t, newValue, null);
            }
            return t;
        }

        public static string GetLayuiColumn(Type t)
        {
            PropertyInfo[] pros = t.GetProperties();
            var list = new List<string>();
            foreach (PropertyInfo p in pros)
            {
                list.Add($"{{ field: '{p.Name}', title: '{p.Name}', minWidth: 100, align: 'center' }}");
            }
            return string.Join("," + Environment.NewLine, list);
        }

        public static string GetTableField<T>(string asTable, T t, Type model2 = null)
        {
            if (string.IsNullOrWhiteSpace(asTable) == false) asTable = asTable + ".";
            PropertyInfo[] pros = t.GetType().GetProperties();
            PropertyInfo[] pros2 = null;
            if (model2 != null) pros2 = model2.GetProperties();
            var list = new List<string>();
            foreach (PropertyInfo p in pros)
            {
                if (pros2 == null && pros2.Any(n => n.Name.Equals(p.Name, StringComparison.OrdinalIgnoreCase)))
                    list.Add($"{asTable}{p.Name} {p.Name}_2 ");
                else
                {
                    list.Add($"{asTable}{p.Name}");
                }
            }
            return string.Join(",", list);
        }

        /// <summary>
        /// 批量替换对象指定数据类型的值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t">对象</param>
        /// <param name="type">数据类型,如TypeOf(string)</param>
        /// <param name="oldValue">旧值</param>
        /// <param name="newValue">新值</param>
        /// <returns></returns>
        public static T ReplacePropertyValue<T>(T t, Type type, object oldValue, object newValue)
        {
            if (t == null) return default(T);
            var objType = t.GetType(); ;
            PropertyInfo[] pros = objType.GetProperties();
            //var obj = Activator.CreateInstance(objType);
            foreach (PropertyInfo p in pros)
            {
                if (p.PropertyType != type) continue;
                var objValue = p.GetValue(t, null);
                if (object.Equals(oldValue, objValue)) p.SetValue(t, newValue, null);
            }
            return t;
        }

        /// <summary>
        /// 将TypeOf(string)类型Trim()或将null设置为空串
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public static T TrimEmptyString<T>(T t)
        {
            if (t == null) return t;
            var objType = t.GetType(); ;
            PropertyInfo[] pros = objType.GetProperties();
            var type = typeof(string);
            foreach (PropertyInfo p in pros)
            {
                if (p.PropertyType != type) continue;
                var objValue = p.GetValue(t, null);
                if (object.Equals(null, objValue)) p.SetValue(t, string.Empty, null);
                else p.SetValue(t, objValue.ToString().Trim(), null);
            }
            return t;
        }

        /// <summary>
        /// 创建对象实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public static T CreateInstance<T>(T t)
        {
            Type o = t.GetType();//加载类型
            object obj = Activator.CreateInstance(o, true);//根据类型创建实例
            return (T)obj;//类型转换并返回
        }

        /// <summary>
        /// 创建对象实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fullName">命名空间.类型名</param>
        /// <param name="assemblyName">程序集</param>
        /// <returns></returns>
        public static T CreateInstance<T>(string fullName, string assemblyName)
        {
            string path = fullName + "," + assemblyName;//命名空间.类型名,程序集
            Type o = Type.GetType(path);//加载类型
            object obj = Activator.CreateInstance(o, true);//根据类型创建实例
            return (T)obj;//类型转换并返回
        }

        /// <summary>
        /// 创建对象实例
        /// </summary>
        /// <typeparam name="T">要创建对象的类型</typeparam>
        /// <param name="assemblyName">类型所在程序集名称</param>
        /// <param name="nameSpace">类型所在命名空间</param>
        /// <param name="className">类型名</param>
        /// <returns></returns>
        public static T CreateInstance<T>(string assemblyName, string nameSpace, string className)
        {
            try
            {
                string fullName = nameSpace + "." + className;//命名空间.类型名
                                                              //此为第一种写法
                object ect = Assembly.Load(assemblyName).CreateInstance(fullName);//加载程序集，创建程序集里面的 命名空间.类型名 实例
                return (T)ect;
                //类型转换并返回
                //下面是第二种写法
                //string path = fullName + "," + assemblyName;//命名空间.类型名,程序集
                //Type o = Type.GetType(path);//加载类型
                //object obj = Activator.CreateInstance(o, true);//根据类型创建实例
                //return (T)obj;//类型转换并返回
            }
            catch
            {
                //发生异常，返回类型的默认值
                return default(T);
            }
        }
    }
}