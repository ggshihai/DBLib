﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace DBLib.Web
{ 
    public class WebConfigHelper
    {
        /// 配置文件加密解密
        /// 加密方式 两个方式不同点?
        /// 
        public enum EncryptType
        {
            DataProtectionConfigurationProvider,
            RSAProtectedConfigurationProvider
        }

        ///// 
        ///// 以DPAPI方式加密Config
        ///// 
        //public void EncryptWebConfigByDPAPI(string sectionName = "connectionStrings")
        //{
        //    Configuration configuration = null;
        //    ConfigurationSection section = null;
        //    //打开Request所在路径网站的Web.config文件
        //    //configuration = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
        //    configuration = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
        //    //取得Web.config中connectionStrings设置区块
        //    section = configuration.GetSection(sectionName);
        //    //未加密时
        //    if (!section.SectionInformation.IsProtected)
        //    {
        //        section.SectionInformation.ProtectSection(EncryptType.DataProtectionConfigurationProvider.ToString());
        //        configuration.Save();
        //    }
        //}
        ///// 
        ///// 解密DPAPI
        ///// 
        //public void DecryptWebConfigByDPAPI(string sectionName = "connectionStrings")
        //{
        //    Configuration configuration = null;
        //    ConfigurationSection section = null;
        //    //打开Request所在路径网站的Web.config文件
        //    //configuration = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
        //    configuration = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
        //    //取得Web.config中connectionStrings设置区块
        //    section = configuration.GetSection(sectionName);
        //    if (section != null && section.SectionInformation.IsProtected)
        //    {
        //        section.SectionInformation.UnprotectSection();
        //        configuration.Save();
        //    }
        //}

        /// 
        /// 以RSA方式加密Config
        /// 
        public void EncryptWebConfig(EncryptType type, string sectionName = "connectionStrings")
        {
            Configuration configuration = null;
            ConfigurationSection section = null;
            //打开Request所在路径网站的Web.config文件 
            //System.Web.HttpContext.Current.Request.ApplicationPath==/EQuality 虚拟路径
            //configuration = WebConfigurationManager.OpenWebConfiguration(System.Web.HttpContext.Current . Request.ApplicationPath);
            configuration = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            //取得Web.config中connectionStrings设置区块
            section = configuration.GetSection(sectionName);
            //未加密时
            if (section != null && !section.SectionInformation.IsProtected)
            {
                section.SectionInformation.ProtectSection(type.ToString());
                configuration.Save();
            }
        }

        /// 
        /// 解密Rsa或DPAPI是用同一种方法
        /// 
        public void DecryptWebConfig(string sectionName = "connectionStrings")
        {
            Configuration configuration = null;
            ConfigurationSection section = null;
            //打开Request所在路径网站的Web.config文件
            //configuration = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
            configuration = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            
            //取得Web.config中connectionStrings设置区块
            section = configuration.GetSection(sectionName);
            if (section != null && section.SectionInformation.IsProtected)
            {
                section.SectionInformation.UnprotectSection();
                configuration.Save();
            }
        }

    }
}