﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace DBLib.Web
{
    public class CacheHelper
    {
        /// <summary>
        /// 读取缓存数据
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static object Get(string Key)
        {
            return HttpRuntime.Cache.Get(Key);
        }

        /// <summary>
        /// 读取缓存数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(string key)
        {
            return (T)HttpRuntime.Cache.Get(key);
        }

        public static bool IsSet(string key)
        {
            return HttpRuntime.Cache.Get(key) != null;
        }

        /// <summary>
        /// 移除指定缓存
        /// </summary>
        /// <param name="Key"></param>
        public static void Remove(string Key)
        {
            if (HttpRuntime.Cache.Get(Key) != null)
            {
                HttpRuntime.Cache.Remove(Key);
            }
        }

        /// <summary>
        /// 设置缓存数据
        /// </summary>
        /// <param name="cacheKey">键</param>
        /// <param name="content">值</param>
        public static void Set(string cacheKey, object content)
        {
            HttpRuntime.Cache.Insert(cacheKey, content);
        }

        /// <summary>
        /// 设置缓存数据并且设置默认过期时间
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="content"></param>
        /// <param name="timeOut"></param>
        public static void Set(string cacheKey, object content, int timeOut = 3600)
        {
            try
            {
                if (content == null)
                {
                    return;
                }
                var objCache = HttpRuntime.Cache;
                //设置绝对过期时间
                //绝对时间过期。DateTime.Now.AddSeconds(10)表示缓存在3600秒后过期，TimeSpan.Zero表示不使用平滑过期策略。
                objCache.Insert(cacheKey, content, null, DateTime.Now.AddSeconds(timeOut), TimeSpan.Zero, CacheItemPriority.High, null);
                //相对过期
                //DateTime.MaxValue表示不使用绝对时间过期策略，TimeSpan.FromSeconds(10)表示缓存连续10秒没有访问就过期。
                //objCache.Insert(cacheKey, objObject, null, DateTime.MaxValue, timeout, CacheItemPriority.NotRemovable, null); 
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// 删除全部缓存
        /// </summary>
        public static void RemoveAllCache()
        {
            var objCache = HttpRuntime.Cache;
            var cacheEnum = objCache.GetEnumerator();
            while (cacheEnum.MoveNext())
            {
                objCache.Remove(cacheEnum.Key.ToString());
            }
        }
    }
}
